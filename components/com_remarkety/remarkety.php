<?php
/**
* @copyright    Copyright (C) 2013 Interamind Ltd. All rights reserved.
* @license      GNU/GPL, see LICENSE.txt
**/

defined( '_JEXEC' ) or die( 'Restricted access' );
/**
* @copyright    Copyright (C) 2013 Interamind Ltd. All rights reserved.
* @license        GNU/GPL, see LICENSE.txt
**/

// Require the base controller
require_once (JPATH_COMPONENT.DS.'controller.php');

// Create the controller
$controller = new remarketyController();

// Perform the Request task
$controller->execute(JRequest::getVar('task', null, 'default', 'cmd'));
$controller->redirect();
?>
