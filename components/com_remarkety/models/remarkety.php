<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
/**
* @copyright    Copyright (C) 2013 Interamind Ltd. All rights reserved.
* @license      GNU/GPL, see LICENSE.txt
**/
 
jimport( 'joomla.application.component.model' );


class remarketyModelRemarkety extends JModel {
	public function getApiKey(){
		$db = & JFactory::getDBO();
		$q = "SELECT param_value FROM #__remarkety WHERE param_name='REMARKETY_API_KEY'";
		$db->setQuery($q);
        $api_key = $db->loadResult();
        return $api_key;
	}
	
}
