<?php
/**
* @copyright    Copyright (C) 2013 Interamind Ltd. All rights reserved.
* @license      GNU/GPL, see LICENSE.txt
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

JHTML::_('stylesheet','remarkety.css','administrator/components/com_remarkety/views/remarkety/tmpl/css/');


//JRequest::setVar( 'hidemainmenu', 1 );

?>
<div style="font-size: 14px;">
	<div><?php echo JText::_("REMARKETY_LOGO")?></div>
	<h3><?php echo JText::_("API_CONNECTION_CODE")?></h3>
	<p><?php echo JText::_("YOUR_CONNECTION_API_KEY")?></p>
	<p><?php echo $this->getApiKey() ?></p>
	<h3><?php echo JText::_("HOW_TO_USE_API")?></h3>
	<p><?php echo JText::_("USE_THIS_KEY_FOR_DESCRIPTION")?></p>
	<h3><?php echo JText::_("SUPPORT_TITLE")?></h3>
	<p><?php echo JText::_("SUPPORT_DESCRIPTION")?></p>
</div>