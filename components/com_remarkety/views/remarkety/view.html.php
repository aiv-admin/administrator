<?php
/**
* @copyright    Copyright (C) 2013 Interamind Ltd. All rights reserved.
* @license      GNU/GPL, see LICENSE.txt
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

class remarketyViewRemarkety extends Jview
{
	private $dispatcher = null;
	
	function __construct($config = array()){
		parent::__construct();
		JPluginHelper::importPlugin('vmee');
		$this->dispatcher =& JDispatcher::getInstance();
	}
	
	function display($tpl = null){
		JToolBarHelper::title( JText::_( 'REMARKETY_TITLE' ), 'interamind_logo' );
//		JToolBarHelper::apply('apply');
		JToolBarHelper::preferences('com_remarkety');
//		JToolBarHelper::custom("help", "help", "help", "Help and Support", false);

		$model =& $this->getModel();
		
		
		$error = "";
		
		parent::display($tpl);
	}
	
	public function getApiKey(){
		return $this->getModel()->getApiKey();
	}
	
}
?>

