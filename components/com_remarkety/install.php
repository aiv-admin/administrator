<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
/**
* @copyright    Copyright (C) 2014 Interamind Ltd. All rights reserved.
* @license      GNU/GPL, see LICENSE.txt
**/

define ('MENU_TITLE', '');
jimport('joomla.installer.installer');
jimport('joomla.application.component.helper');

function com_install()
{
	$installer = & JInstaller::getInstance();
	$src = $installer->getPath('source');
	$com_version = '';
	$apiKey = '';
	try{
		$jlang =& JFactory::getLanguage();
		$jlang->load('com_remarkety', JPATH_ADMINISTRATOR, 'en-GB', true);
		$jlang->load('com_remarkety', JPATH_ADMINISTRATOR, null, true);
		
		$manifest = new SimpleXMLElement(file_get_contents($src.DS.'manifest.xml'));
		$com_version = $manifest->version;
		
		$phpVer = phpversion();
		if(version_compare($phpVer, '5.2.0') < 0){
			throw new Exception(JText::_('PHP_VERSION_TOO_LOW'));
		}
		
		require_once (JPATH_ADMINISTRATOR.DS."components".DS."com_virtuemart".DS."version.php");
		$vmver = new vmVersion();
		$matches = array();
		preg_match("/\s(.*?)\s/", $shortversion,$matches);
		$vmReleaseNum = $matches[1];
		/*if(version_compare($vmReleaseNum, '2.0.2', 'lt')){
			throw new Exception(JText::_('VM_VERSION_TOO_LOW'));
		}*/
		
		$db = & JFactory::getDBO();
		$q = "SELECT param_value FROM #__remarkety WHERE param_name = 'REMARKETY_API_KEY'";
		$db->setQuery($q);
		$apiKey = $db->loadResult();
		
		if (empty($apiKey)) {
			// Try installing from the oneclick tables
			$q = "SELECT param_value FROM #__oneclickcampaign WHERE param_name = 'ONE_CLICK_CAMPAIGN_API_KEY'";
			$db->setQuery($q);
			$apiKey = $db->loadResult();
			if (empty($apiKey)) {
				// Generate a new one
				$apiKey = preg_replace("/\./", "", substr(uniqid(null,true) . uniqid(null,true),0,32));
			}

			$q = "INSERT INTO #__remarkety (param_name,param_value) VALUES('REMARKETY_API_KEY','".$apiKey."')";
			$db->setQuery($q);
			$db->query();
		}
	}
	catch (Exception $e){
		$Errormsg = JText::_("Error ocured during installation");
		echo '<h2 style="color:red">' . JText::_("INSTALL_FAILED") . '</h2>';
		echo '<p style="color:red">' . $Errormsg . $e->getMessage() . '</p>';
		return raiseError($Errormsg . $e->getMessage());
	}
	
	echo '<div>' . JText::_("REMARKETY_LOGO") . '</div>';
	echo '<h2>Installed version: ' .$com_version.'</h2>';
	echo '<p style="font-size:16px">' . JText::_("INSTALL_SUCCESS") . '</p>';
	echo '<p style="font-size:16px">' . JText::_("YOUR_CONNECTION_API_KEY").' '.$apiKey. '</p>';
	echo '<p style="font-size:16px">' . JText::_("USE_THIS_KEY_FOR_DESCRIPTION") . '</p>';
	echo '<p style="font-size:16px">' . JText::_("THANK_YOU_FOR_INSTALLING") . '</p>';
	echo '<p style="font-size:16px"><a target="_blank" href="http://www.remarkety.com">www.remarkety.com</a></p>';
	return true;
}


function raiseError($msg) {
	JError::raiseWarning(1, JText::_('Component').' '.JText::_('Install').': '.$msg);
	return false;
}

function interadebug($msg) {
	//global $mainframe;
	//$mainframe->enqueueMessage($msg);
}

?>
