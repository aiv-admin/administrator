<?php
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 * Googlebasexml Googlebasexml Controller
 *
 */
class GooglebasexmlControllerGooglebasexml extends JControllerLegacy
{
	/**
	 * constructor (registers additional tasks to methods)
	 * @return void
	 */
	function __construct()
	{
		parent::__construct();
		$this->registerTask( 'add'  , 'edit' );
		$this->registerTask('apply', 'save');
		$this->registerTask('save2copy', 'save');
		
	}
	
	function display($cachable = false, $urlparams = Array())
	{
        $jinput = JFactory::getApplication()->input;			
		$jinput->set( 'view', 'googlebasexmls');
		parent::display($cachable, $urlparams);
	}
	function edit()
	{
		$canDo = GooglebasexmlHelper::getActions();
		if (!$canDo->get('core.edit')) {	jexit('Invalid Action'); }	
		
		$model = $this->getModel('googlebasexml');		
        //$user = JFactory::getUser();
        $jinput = JFactory::getApplication()->input;	
	    $jinput->set('view', 'googlebasexml');
	    $jinput->set( 'layout', 'default'  );
	    $jinput->set('hidemainmenu', 1);
	    parent::display();
	}
	

    function save()
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$canDo = GooglebasexmlHelper::getActions();
		if (!$canDo->get('core.create')) {	jexit('Invalid Action'); }	
		
		
        $jinput = JFactory::getApplication()->input;	
		// Initialise variables.
		$app	= JFactory::getApplication();
		$model	= $this->getModel('googlebasexml');
		$form	= $model->getForm();
		$formdata	= $jinput->get('jform', array(),'array');
		$tabledata =  $jinput->get('tform', array(),'array');
				
		
		$task = $this->getTask();
		if($task == 'save2copy')
		{
		   $id = 0;	
		}
		else
		{
		   $id = $jinput->get('id',0,'int');
		}
		$option	= 'googlebasexml';

		// Check if the user is authorized to do this.
	/*	if (!JFactory::getUser()->authorise('core.admin', $option))
		{
			JFactory::getApplication()->redirect('index.php', JText::_('JERROR_ALERTNOAUTHOR'));
			return;
		}*/

		// Validate the posted data.
		$return = $model->validate($form, $formdata);

		// Check for validation errors.
		if ($return === false) {
			// Get the validation messages.
			$errors	= $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++) {
				if ($errors[$i] instanceof Exception) {
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				} else {
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}


			// Redirect back to the edit screen.
			$this->setRedirect(JRoute::_('index.php?option=com_googlebasexml&view=googlebasexmls', false));
			return false;
		}

		// Attempt to save the configuration.
		$data	= array(
					'formdata'	=> $return,
					'id'		=> $id,
					'tabledata'	=> $tabledata
					);
		$return = $model->save($data, $task);

		// Check the return value.
		if ($return === false)
		{
			// Save failed, go back to the screen and display a notice.
			$message = JText::sprintf('JERROR_SAVE_FAILED', $model->getError());
			$this->setRedirect('index.php?option=com_googlebasexml&view=googlebasexmls', $message, 'error');
			return false;
		}

		// Set the redirect based on the task.
		switch ($task)
		{
			case 'apply':
				$message = JText::_('COM_GOOGLEBASEXML_SAVE_SUCCESS');
				$this->setRedirect('index.php?option=com_googlebasexml&view=googlebasexml&task=googlebasexml.edit&id='.$return, $message);
				break;

			case 'save':
			default:
				$message = JText::_('COM_GOOGLEBASEXML_SAVE_SUCCESS');			
				$this->setRedirect('index.php?option=com_googlebasexml&view=googlebasexmls',$message);
				break;
		}

		return true;
     }
	
	

	function remove()
	{
		$canDo = GooglebasexmlHelper::getActions();
		if (!$canDo->get('core.delete')) {	jexit('Invalid Action'); }	
		
		
		$model = $this->getModel('googlebasexml');		
        if(!$model->delete()) {
			$msg = JText::_( 'GXML_ERROR_DELETING_FEED' );
		} else {
			$msg = JText::_( 'GXML_FEEDS_DELETED' );
		}

		$this->setRedirect( 'index.php?option=com_googlebasexml&view=googlebasexmls', $msg );
	}

	/**
	 * cancel editing a record
	 * @return void
	 */
	function cancel()
	{
		$msg = JText::_( 'GXML_OPERATION_CANCELLED' );
		//$model = $this->getModel('googlebasexml');		
        //$model->checkIn();			

		$this->setRedirect( 'index.php?option=com_googlebasexml&view=googlebasexmls', $msg );
	}
	

	function publish()
	{
		$canDo = GooglebasexmlHelper::getActions();
		if (!$canDo->get('core.edit.state')) {	jexit('Invalid Action'); }	
		
		
		$model = $this->getModel('googlebasexml');		
        if( !$model->publishMenu(1) )
		{
			$msg = JText::_( 'Error: Publishing State Could not be Edited ' );
		} else {
			$msg = JText::_( 'Publishing State Edited' );
		}
		
		$this->setRedirect( 'index.php?option=com_googlebasexml&view=googlebasexmls', $msg );	
	
	}
	
		
		
	function unpublish()
	{
		$canDo = GooglebasexmlHelper::getActions();
		if (!$canDo->get('core.edit.state')) {	jexit('Invalid Action'); }	
		
		
		$model = $this->getModel('googlebasexml');		
        if( !$model->publishMenu(0) )
		{
			$msg = JText::_( 'Error: Publishing State Could not be Edited ' );
		} else {
			$msg = JText::_( 'Feed Unpublished' );
		}
		
		$this->setRedirect( 'index.php?option=com_googlebasexml&view=googlebasexmls', $msg );	
	
	}
		
		

}