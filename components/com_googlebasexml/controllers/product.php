<?php
/**
* @version		$Id:default.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Controllers
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controlleradmin');
jimport('joomla.application.component.controllerform');

/**
 * GooglebasexmlProduct Controller
 *
 * @package    Googlebasexml
 * @subpackage Controllers
 */
class GooglebasexmlControllerProduct extends JControllerForm
{
	public function __construct($config = array())
	{
	
		$this->view_item = 'product';
		$this->view_list = 'products';
		parent::__construct($config);
	}	
	
	/**
	 * Proxy for getModel.
	 *
	 * @param   string	$name	The name of the model.
	 * @param   string	$prefix	The prefix for the PHP class name.
	 *
	 * @return  JModel
	 * @since   1.6
	 */
	public function getModel($name = 'Product', $prefix = 'GooglebasexmlModel', $config = array('ignore_request' => false))
	{
		$model = parent::getModel($name, $prefix, $config);
	
		return $model;
	}	
}// class
?>