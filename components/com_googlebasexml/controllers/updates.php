<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Googlebasexml
 * @author     Fiona Coulter <forwarder@spiralscripts.co.uk>
 * @copyright  2016 Fiona Coulter
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Updates controller class.
 *
 * @since  1.6
 */
 
 
class GooglebasexmlControllerUpdates extends JControllerLegacy
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	 
 	protected $context = "updates";
	protected $option = "com_googlebasexml";
	protected $text_prefix = "GOOGLEBASEXML";
	protected $view_item = "updates";
	protected $view_list = "googlebasexmls";

	 
	 
	public function __construct()
	{
		parent::__construct();
		// Apply, Save & New, and Save As copy should be standard on forms.
		$this->registerTask('apply', 'save');
	}
	
	
	protected function allowEdit()
	{
		return JFactory::getUser()->authorise('core.admin', $this->option);
	}


	protected function allowSave()
	{
		$user = JFactory::getUser();

		return ($user->authorise('core.admin', $this->option));
	}
	
	
	public function cancel($key = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app = JFactory::getApplication();
		

		$this->setRedirect(
			JRoute::_(
				'index.php?option=' . $this->option . '&view='.$this->view_list , false
			)
		);

		return true;
	}
	
	public function save($key = null, $urlVar = null)
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$model = $this->getModel('updates');
		$input = $app->input;
		$data  = $input->post->get('jform', array(), 'array');
		
		$task = $this->getTask();



		// Access check.
		if (!$this->allowSave())
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_list, false
				)
			);

			return false;
		}

		// Validate the posted data.
		// Sometimes the form needs some posted data, such as for plugins and modules.
		$form = $model->getForm($data, false);
		
		

		if (!$form)
		{
			$app->enqueueMessage($model->getError(), 'error');

			return false;
		}

		// Test whether the data is valid.
		$validData = $model->validate($form, $data);
		

		// Check for validation errors.
		if ($validData === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}


			// Redirect back to the edit screen.
			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item, false
				)
			);

			return false;
		}


		// Attempt to save the data.
		if (!$model->save($validData))
		{

			// Redirect back to the edit screen.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(
				JRoute::_(
					'index.php?option=' . $this->option . '&view=' . $this->view_item
					, false
				)
			);

			return false;
		}


		$this->setMessage(
			JText::_('JLIB_APPLICATION_SAVE_SUCCESS')
		);

		// Redirect the user and adjust session state based on the chosen task.
		switch ($task)
		{
			case 'apply':

				// Redirect back to the edit screen.
				$this->setRedirect(
					JRoute::_(
						'index.php?option=' . $this->option . '&view=' . $this->view_list
						, false
					)
				);
				break;


			default:

				// Redirect to the list screen.
				$this->setRedirect(
					JRoute::_(
						'index.php?option=' . $this->option . '&view=' . $this->view_list, false
					)
				);
				break;
		}


		return true;
	}
	
	
	
}

	
	
