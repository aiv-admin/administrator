<?php
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');
require_once (JPATH_ADMINISTRATOR.'/components/com_googlebasexml/helpers/googlebasexml.php' );

/**
 * googlebasexml  Component Controller
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class GooglebasexmlController extends JControllerLegacy
{
	
	protected $default_view = 'googlebasexmls';	
	/**
	 * Method to display the view
	 *
	 * @access	public
	 */
	 
	 
	public function display($cachable = false, $urlparams = false)
	{
        $jinput = JFactory::getApplication()->input;	
		
	    if($jinput->get('view','','cmd') == '') {
			$jinput->set('view', 'googlebasexmls' );
        }
		//$layout = $jinput->get('layout', 'default');
		//$id     = $jinput->get('id');
		
	
		parent::display($cachable, $urlparams);
		
		return $this;
		
	}
}