CREATE TABLE IF NOT EXISTS `#__googlebasexml`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(7) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` varchar(5120) NOT NULL,
   PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET=utf8;    
