CREATE TABLE IF NOT EXISTS `#__googlebasexml`(
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordering` int(7) NOT NULL DEFAULT '1',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` varchar(10240) NOT NULL,
   PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET=utf8;    

CREATE TABLE IF NOT EXISTS `#__googlebasexml_cats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `virtuemart_category_id` int(11) NOT NULL DEFAULT '0',
  `google_category` varchar(1000) NOT NULL DEFAULT '',
  `language` char(7) NOT NULL COMMENT 'The language code.',
  PRIMARY KEY (`id`),
  KEY `idx_language` (`language`)
) DEFAULT CHARACTER SET=utf8;

CREATE TABLE IF NOT EXISTS `#__googlebasexml_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `virtuemart_product_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(1000) NOT NULL DEFAULT '' COMMENT 'alternative title',
  `description` text NOT NULL COMMENT 'alternative description',
  `g_item_group_id` varchar(100) NOT NULL DEFAULT '',
  `g_google_category` varchar(1000) NOT NULL DEFAULT '',
  `g_condition` varchar(50) NOT NULL DEFAULT '',
  `g_brand` varchar(100) NOT NULL DEFAULT '',
  `g_gtin` varchar(20) NOT NULL DEFAULT '',
  `g_mpn` varchar(20) NOT NULL DEFAULT '',
  `g_shipping_label` varchar(50) NOT NULL DEFAULT '',
  `g_multipack` smallint(5) unsigned NOT NULL DEFAULT '0',
  `g_is_bundle` varchar(20) NOT NULL DEFAULT '',
  `g_adult` varchar(20) NOT NULL DEFAULT '',
  `g_adwords_redirect` varchar(1000) NOT NULL DEFAULT '',
  `g_gender` varchar(20) NOT NULL DEFAULT '',
  `g_age_group` varchar(20) NOT NULL DEFAULT '',
  `g_color` varchar(20) NOT NULL DEFAULT '',
  `g_size` varchar(20) NOT NULL DEFAULT '',
  `g_size_type` varchar(50) NOT NULL DEFAULT '',
  `g_size_system` varchar(20) NOT NULL DEFAULT '',
  `g_material` varchar(100) NOT NULL DEFAULT '',
  `g_pattern` varchar(100) NOT NULL DEFAULT '',
  `g_custom_label_0` varchar(100) NOT NULL DEFAULT '',
  `g_custom_label_1` varchar(100) NOT NULL DEFAULT '',
  `g_custom_label_2` varchar(100) NOT NULL DEFAULT '',
  `g_custom_label_3` varchar(100) NOT NULL DEFAULT '',
  `g_custom_label_4` varchar(100) NOT NULL DEFAULT '',
  `g_excluded_destination` varchar(100) NOT NULL DEFAULT '',
  `g_energy_efficiency_class` varchar(20) NOT NULL DEFAULT '',
  `g_promotion_id` varchar(1000) NOT NULL DEFAULT '',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL COMMENT 'The language code',
  PRIMARY KEY (`id`),
  KEY `idx_language` (`language`)
) DEFAULT CHARACTER SET=utf8;

