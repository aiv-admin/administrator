<?php

defined('JPATH_BASE') or die;

/**
 * Supports a modal cat picker.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_googlebasexml
 */
class JFormFieldModal_Cat extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'Modal_Cat';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 *
	 * @since   1.6
	 */
	protected function getInput()
	{
		
		if(version_compare(JVERSION,'3','<')){
		   return $this->getInput_25();	
		}
		
		
		$allowEdit		= ((string) $this->element['edit'] == 'true') ? true : false;
		$allowClear		= ((string) $this->element['clear'] != 'false') ? true : false;

		// Load language
		JFactory::getLanguage()->load('com_googlebasexml', JPATH_ADMINISTRATOR);
		$vmlang = GooglebasexmlHelper::getVMLang();

		// Load the javascript
		JHtml::_('behavior.framework');
		JHtml::_('behavior.modal', 'a.modal');
		if(!version_compare(JVERSION,'3','<')){
			JHtml::_('bootstrap.tooltip');
		}
		

		// Build the script.
		$script = array();

		// Select button script
		$script[] = '	function jSelectCat_' . $this->id . '(id, name, object) {';
		$script[] = '		document.id("' . $this->id . '_id").value = id;';
		$script[] = '		document.id("' . $this->id . '_name").value = name;';

		if ($allowEdit)
		{
			$script[] = '		jQuery("#' . $this->id . '_edit").removeClass("hidden");';
		}

		if ($allowClear)
		{
			$script[] = '		jQuery("#' . $this->id . '_clear").removeClass("hidden");';
		}

		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';

		// Clear button script
		static $scriptClear;

		if ($allowClear && !$scriptClear)
		{
			$scriptClear = true;

			$script[] = '	function jClearCat(id) {';
			$script[] = '		document.getElementById(id + "_id").value = "";';
			$script[] = '		document.getElementById(id + "_name").value = "'.htmlspecialchars(JText::_('JSELECT', true), ENT_COMPAT, 'UTF-8').'";';
			$script[] = '		jQuery("#"+id + "_clear").addClass("hidden");';
			$script[] = '		if (document.getElementById(id + "_edit")) {';
			$script[] = '			jQuery("#"+id + "_edit").addClass("hidden");';
			$script[] = '		}';
			$script[] = '		return false;';
			$script[] = '	}';
		}

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));

		// Setup variables for display.
		$html	= array();
		$link	= 'index.php?option=com_googlebasexml&amp;view=vmcats&amp;layout=modal&amp;tmpl=component&amp;function=jSelectCat_' . $this->id;

		if (isset($this->element['language']))
		{
			$link .= '&amp;forcedLanguage=' . $this->element['language'];
		}

		// Get the title of the linked chart
		if ((int) $this->value > 0)
		{
			$db = JFactory::getDbo();
			$query = $db->getQuery(true)
				->select($db->quoteName('category_name'))
				->from($db->quoteName('#__virtuemart_categories_'.$vmlang))
				->where('virtuemart_category_id = ' . (int) $this->value);
			$db->setQuery($query);

			try
			{
				$title = $db->loadResult();
			}
			catch (RuntimeException $e)
			{
				JError::raiseWarning(500, $e->getMessage);
			}
		}

		if (empty($title))
		{
			$title = JText::_('JSELECT');
		}

		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

		// The active cat id field.
		if (0 == (int) $this->value)
		{
			$value = '';
		}
		else
		{
			$value = (int) $this->value;
		}

		// The current cat display field.
		$html[] = '<span class="input-append">';
		$html[] = '<input type="text" class="input-large" id="' . $this->id . '_name" value="' . $title . '" disabled="disabled" size="60" />';
		$html[] = '<a class="modal btn hasTooltip" title="' . JHtml::tooltipText('JSELECT') . '"  href="' . $link . '&amp;' . JSession::getFormToken() . '=1" rel="{handler: \'iframe\', size: {x: 800, y: 450}}"><i class="icon-file"></i> ' . JText::_('JSELECT') . '</a>';

		// Edit article button
		if ($allowEdit)
		{
			$html[] = '<a class="btn hasTooltip' . ($value ? '' : ' hidden') . '" href="index.php?option=com_googlebasexml&layout=modal&tmpl=component&task=cat.edit&id=' . $value . '" target="_blank" title="' . JHtml::tooltipText('com_googlebasexml_EDIT_ITEM') . '" ><span class="icon-edit"></span> ' . JText::_('JACTION_EDIT') . '</a>';
		}

		// Clear cat button
		if ($allowClear)
		{
			$html[] = '<button id="' . $this->id . '_clear" class="btn' . ($value ? '' : ' hidden') . '" onclick="return jClearCat(\'' . $this->id . '\')"><span class="icon-remove"></span> ' . JText::_('JCLEAR') . '</button>';
		}

		$html[] = '</span>';

		// class='required' for client side validation
		$class = '';

		if ($this->required)
		{
			$class = ' class="required modal-value"';
		}

		$html[] = '<input type="hidden" id="' . $this->id . '_id"' . $class . ' name="' . $this->name . '" value="' . $value . '" />';

		return implode("\n", $html);
	}
	
	protected function getInput_25()
	{
		// Load the modal behavior script.
		JHtml::_('behavior.modal', 'a.modal');
		
		// Load language
		JFactory::getLanguage()->load('com_googlebasexml', JPATH_ADMINISTRATOR);
		$vmlang = GooglebasexmlHelper::getVMLang();
		

		// Build the script.
		$script = array();
		$script[] = '	function jSelectCat_'.$this->id.'(id, title, catid, object) {';
		$script[] = '		document.id("'.$this->id.'_id").value = id;';
		$script[] = '		document.id("'.$this->id.'_name").value = title;';
		$script[] = '		SqueezeBox.close();';
		$script[] = '	}';

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration(implode("\n", $script));


		// Setup variables for display.
		$html	= array();
		$link	= 'index.php?option=com_googlebasexml&amp;view=vmcats&amp;layout=modal&amp;tmpl=component&amp;function=jSelectCat_' . $this->id;

		$db = JFactory::getDbo();
		$query = $db->getQuery(true)
			->select($db->quoteName('category_name'))
			->from($db->quoteName('#__virtuemart_categories_'.$vmlang))
			->where('virtuemart_category_id = ' . (int) $this->value);
		$db->setQuery($query);
		$title = $db->loadResult();

		if ($error = $db->getErrorMsg()) {
			JError::raiseWarning(500, $error);
		}

		if (empty($title)) {
			$title = JText::_('JSELECT');
		}
		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

		// The current user display field.
		$html[] = '<div class="fltlft">';
		$html[] = '  <input type="text" id="'.$this->id.'_name" value="'.$title.'" disabled="disabled" size="35" />';
		$html[] = '</div>';

		// The user select button.
		$html[] = '<div class="button2-left">';
		$html[] = '  <div class="blank">';
		$html[] = '	<a class="modal" title="'.JText::_('COM_GOOGLEBASEXML_EDIT_ITEM').'"  href="'.$link.'&amp;'.JSession::getFormToken().'=1" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_GOOGLEBASEXML_EDIT_ITEM').'</a>';
		$html[] = '  </div>';
		$html[] = '</div>';

		// The active article id field.
		if (0 == (int)$this->value) {
			$value = '';
		} else {
			$value = (int)$this->value;
		}

		// class='required' for client side validation
		$class = '';
		if ($this->required) {
			$class = ' class="required modal-value"';
		}

		$html[] = '<input type="hidden" id="'.$this->id.'_id"'.$class.' name="'.$this->name.'" value="'.$value.'" />';

		return implode("\n", $html);
	}	


}
