<?php
/*
copyright 2011 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport('joomla.application.component.modelform');
jimport( 'joomla.application.component.helper' );
//require_once(JPATH_SITE . '/components/com_googlebasexml/helpers/googlebasexml.php');


class GooglebasexmlModelGooglebasexml extends JModelForm
{
	var $params = null;
	var $_id = 0;
	var $_data = null;
	
    function __construct()
	{
		parent::__construct();
        $jinput = JFactory::getApplication()->input;	
		
		if ($id = $jinput->get('id',null,'int'))
		{
		   $this->setId((int) $id);		
		}
		else if($array = $jinput->get('cid',null,'array'))
		{
		     $this->setId((int)$array[0]);	
		}
		
		$this->populateState();
		
  
	}
	
	public function setId($id)
	{
		// Set id 
		$this->_id		= (int)$id;
		$this->_data	= null;
	}
	
	public function getId()
	{
		// get id
		return($this->_id);
	}
	
	
	
	protected function populateState()
	{
		// Set the component (option) we are dealing with.
		$this->setState('component.option', 'com_googlebasexml');
	}
	
	public function getData()
	{
		$id = $this->_id;
		$this->_data = new stdClass();
		if($id > 0)
		{
			$table = $this->getTable();
			if (!$table->load($id))
			{
				$this->setError($table->getError());
				return false;
			}
			foreach($table as $key=>$value)
			{
				$this->_data->$key = $value;
			}
			
		}
		if ($id == 0 || !$this->_data) {
			//$this->_data = new stdClass();
			$this->_data->id = 0;
			$this->_data->title = '';
			$this->_data->alias = '';
		    $this->_data->ordering = 0;
		    $this->_data->date = null;	
		    $this->_data->published = 0;			
		    $this->_data->publish_up = null;			
		    $this->_data->publish_down = null;
			$this->_data->params = '';
	    }	
        return $this->_data;
		
	}
	
	public function getForm($data = array(), $loadData = true)
	{
		if ($path = $this->getState('component.path')) {
			// Add the search path for the admin component config.xml file.
			JForm::addFormPath($path);
		}
		else {
			// Add the search path for the admin component config.xml file.
			JForm::addFormPath(JPATH_ADMINISTRATOR.'/components/'.$this->getState('component.option'));
		}

		// Get the form.
		$form = $this->loadForm(
				'com_googlebasexml.component',
				'config',
				array('control' => 'jform', 'load_data' => $loadData),
				false,
				'/config'
			);

		if (empty($form)) {
			return false;
		}
		
		//print_r($form);

		return $form;
	}
	
	
	protected function loadFormData()
	{
		if(isset($this->_data->params) && !empty($this->_data->params))
		{
		   $this->params = json_decode($this->_data->params, true);
		   return $this->params;
		}
		else
		{
		   $component = $this->getComponent();
		   $this->params = get_object_vars($component->params);
		   return $this->params;	//return global values
		}
	}
	
	
	public function getParams()
	{
		if(isset($this->params))
		{
			return $this->params;
		}
		else
		{
		   return $this->loadFormData();	
		}
		
	}
	

	/**
	 * Get the component information.
	 *
	 * @return	object
	 * @since	1.6
	 */
	function getComponent()
	{
		if (isset($this->_component)){ return $this->_component; }
		// Initialise variables.
		$option = $this->getState('component.option');

		// Load common and local language files.
		$lang = JFactory::getLanguage();
			$lang->load($option, JPATH_BASE, null, false, false)
		||	$lang->load($option, JPATH_BASE . "/components/$option", null, false, false)
		||	$lang->load($option, JPATH_BASE, $lang->getDefault(), false, false)
		||	$lang->load($option, JPATH_BASE . "/components/$option", $lang->getDefault(), false, false);

		$result = JComponentHelper::getComponent($option);
		$this->_component = $result;

		return $result;
	}

	/**
	 * Method to save the configuration data.
	 *
	 * @since	1.6
	 */
	public function save($data, $task)
	{
		JSession::checkToken() or jexit( 'Invalid Token' );	
		$table = $this->getTable();
		
		$id = (int)$data['id'];
		
		
		$tabledata = $data['tabledata'];
		if(empty($tabledata['title']))
		{
			$this->setError(JText::_('COM_GOOGLEBASEXML_TITLE_REQUIRED'));
			return false;
		}
		
		// Load the previous Data
		if($id > 0)
		{
		  if (!$table->load($id)) {
			  $this->setError($table->getError());
			  return false;
		  }
		}
		
		$tabledata['params'] = json_encode($data['formdata']);
		if ( (int) $tabledata['ordering'] < 1 )
		{
		   $tabledata['ordering'] = $table->getNextOrder();		
		}
		else
		{
			$tabledata['ordering'] = (int)$tabledata['ordering'];
		}
		
		
		if($id == 0)
		{
		  $date = JFactory::getDate();
		  $db = JFactory::getDBO();
		  $format = $db->getDateFormat();
		  $tabledata['date'] = $date->format($format);
		}
		
		$tabledata['alias'] = trim($tabledata['alias']);
		//alias
		if(!isset($tabledata['alias']) || empty($tabledata['alias']))
		{
			$tabledata['alias'] = JStringNormalise::toDashSeparated(JString::strtolower($tabledata['title']));
			
		}
		
		if($task == 'save2copy')
		{
			list($title, $alias) = $this->incrementTitle($tabledata['alias'], $tabledata['title']);
			$tabledata['title']	= $title;
			$tabledata['alias']	= $alias;
			
		}
		else
		{
			if(empty($table->alias))
			{
				$tabledata['alias']	= $this->uniqueAlias($tabledata['alias']);
			}
		}
		
		
		// Bind the data.
		if (!$table->bind($tabledata)) {
			$this->setError($table->getError());
			return false;
		}
        
		// Check the data.
		if (!$table->check()) {
			$this->setError($table->getError());
			return false;
		}

		// Store the data.
		if (!$table->store()) {
			$this->setError($table->getError());
			return false;
		}
		
		return $table->id;
		
	}

	function checkIn($pk = NULL)
	{}
	
	function delete()
	{
	
		// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );	
        $jinput = JFactory::getApplication()->input;			
		$cids = $jinput->get('cid',array(),'array');

		$row = $this->getTable();

		if (count( $cids )) {
			foreach($cids as $cid) {
				if (!$row->delete( $cid )) {
					$this->setError( $row->getError() );
					return false;
				}
			}
		}
		return true;
	}

	function publishMenu( $publish )
	{
			// Check for request forgeries
		JSession::checkToken() or jexit( 'Invalid Token' );
        $jinput = JFactory::getApplication()->input;	
		$cids = $jinput->get('cid',array(),'array');

		$row = $this->getTable();
		
		$user = JFactory::getUser();
		if (!$row->publish($cids, $publish, $user->id))
		{
		  $this->setError($row->getError());
		  return false;
		}
		return true;
	}
	
	protected function uniqueAlias($alias)
	{
		$table = $this->getTable();
		while ($table->load(array('alias' => $alias)))
		{
			$alias = JString::increment($table->alias, 'dash');
		}

		return $alias;
	}


	protected function incrementTitle($alias, $title)
	{
		// Alter the title & alias
		$table = $this->getTable();
		while ($table->load(array('alias' => $alias)))
		{
			$title = JString::increment($table->title);
			$alias = JString::increment($table->alias, 'dash');
		}

		return array($title, $alias);
	}
	
}