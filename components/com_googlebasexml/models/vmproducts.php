 <?php
/**
* @version		$Id:default.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Models
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');


JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_googlebasexml/tables');

class GooglebasexmlModelVmproducts extends JModelList
{
	public $vmlang = "";
	
	public function __construct($config = array())
	{
		
		// Load language
		JFactory::getLanguage()->load('com_googlebasexml', JPATH_ADMINISTRATOR);
		$this->vmlang = GooglebasexmlHelper::getVMLang();
		
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                            'product_name', 'b.product_name',
                            'virtuemart_category_id', 'c.virtuemart_category_id',
                            'published', 'a.published',
                            'virtuemart_product_id', 'a.virtuemart_product_id',
							'category_name', 'd.category_name'
                        );
        }

		parent::__construct($config);		
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
			parent::populateState();
			$app = JFactory::getApplication();
            $config = JFactory::getConfig();
			$id = $app->input->getInt('virtuemart_category_id', null);
			$this->setState('productlist.id', $id);			
			
			// Load the filter state.
			$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search','','string');
			$this->setState('filter.search', $search);

			$app = JFactory::getApplication();
			$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $config->get('list_limit'),'int');
			$limit = $value;
			$this->setState('list.limit', $limit);
			
			$value = $app->getUserStateFromRequest($this->context.'.limitstart', 'limitstart', 0, 'int');
			$limitstart = ($limit != 0 ? (floor($value / $limit) * $limit) : 0);
			$this->setState('list.start', $limitstart);
			
			$value = $app->getUserStateFromRequest($this->context.'.ordercol', 'filter_order', $ordering, 'cmd');
			$this->setState('list.ordering', $value);			
			$value = $app->getUserStateFromRequest($this->context.'.orderdirn', 'filter_order_Dir', $direction, 'cmd');
			$this->setState('list.direction', $value);

			$state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'cmd');
			$this->setState('filter.state', $state);
			
			$language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '', 'cmd');
			$this->setState('filter.language', $language);
			
					
	}
    		
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('productlist.id');
						$id .= ':' . $this->getState('filter.state');
				return parent::getStoreId($id);
	}	
	
	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 */
	protected function getListQuery()
	{
		
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);		
		$query->select('a.virtuemart_product_id, a.published, b.product_name, c.virtuemart_category_id, d.category_name');
		$query->from('#__virtuemart_products as a');
		$query->leftJoin('#__virtuemart_products_'.$db->escape($this->vmlang).' AS b ON a.virtuemart_product_id=b.virtuemart_product_id');
		$query->leftJoin('#__virtuemart_product_categories AS c ON a.virtuemart_product_id=c.virtuemart_product_id');
		$query->leftJoin('#__virtuemart_categories_'.$db->escape($this->vmlang).' AS d ON c.virtuemart_category_id=d.virtuemart_category_id');
		
		 				// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.virtuemart_product_id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(b.product_name LIKE ' . $search . '  OR d.category_name LIKE ' . $search .  ' )');
			}
		}
				
		$published = $this->getState('filter.state');
		
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}
		
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'virtuemart_product_id');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if(empty($orderCol)) $orderCol = 'virtuemart_product_id';
		if(empty($orderDirn)) $orderDirn = 'ASC'; 		
		$query->order($db->escape($orderCol . ' ' . $orderDirn));
							
		return $query;
	}	
}