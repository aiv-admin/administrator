<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Googlebasexml
 * @author     Fiona Coulter <forwarder@spiralscripts.co.uk>
 * @copyright  2016 Fiona Coulter
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Googlebasexml model.
 *
 * @since  1.6
 */
class GooglebasexmlModelUpdates extends JModelLegacy
{
	protected $updateName = "GBaseXML";
	protected $updatePath = "https://www.spiralscripts.co.uk/index.php?option=com_virtuemart&view=plugin&name=spiral_download";
	protected $dummy = "&file=dummy.zip";
	protected $option = "com_googlebasexml";
	protected $item = null;
	
	public function getForm ($data = array(), $loadData = true)
	{
		
		// Get the form.
		JForm::addFormPath(JPATH_COMPONENT . '/models/forms');
		JForm::addFieldPath(JPATH_COMPONENT . '/models/fields');
		JForm::addFormPath(JPATH_COMPONENT . '/model/form');
		JForm::addFieldPath(JPATH_COMPONENT . '/model/field');
		
		
		
		
		// Get the form.
		$form = JForm::getInstance(
			'com_googlebasexml.updates', 'updates',
			array('control' => 'jform'),
			false
		);

		if (empty($form))
		{
			return false;
		}
		
		if($loadData)
		{
			$data = $this->loadFormData();
			$form->bind($data);
		}

		return $form;		
	
	}
	
	protected function loadFormData()
	{
		if(isset($this->item))
		{
			return get_object_vars($this->item);
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('*')->from($db->quoteName('#__update_sites'))->where($db->quoteName('name').' = '.$db->quote($this->updateName));
		$db->setQuery($query);
		
		$item = new stdClass();
		$item->id = 1;
		if($result = $db->loadObject())
		{
			if(!empty($result->extra_query))
			{
				$item->download_url = $this->updatePath . $result->extra_query;
				$item->download_url = str_replace($this->dummy, "", $item->download_url);
			}
			else
			{
				$item->download_url = "";
			}
		}
		
		$this->item = $item;
		$array = get_object_vars($item);
		return $item;
		
		
	}
	
	
	public function getItem()
	{
		return $this->loadFormData();
	}
	
	
	public function save($data)
	{
		JSession::checkToken() or jexit( 'Invalid Token' );	
        if (!JFactory::getUser()->authorise('core.admin', $this->option))
		{
			JFactory::getApplication()->redirect('index.php', JText::_('JERROR_ALERTNOAUTHOR'));
			return;
		}		
		
		
		try{
		  $id = (int)$data['id'];
		  $download_url = trim($data['download_url']);
		  $extra_query = "";
		  
		  if(!empty($download_url))
		  {
			  $extra_query = str_replace($this->updatePath, "", $download_url);
			  $extra_query = $extra_query . $this->dummy;
		  }
		  
		  $db = JFactory::getDbo();
		  $query = $db->getQuery(true);
		  
		  $fields = array($db->quoteName('extra_query'). '='. $db->quote($extra_query));
		  $conditions = array($db->quoteName('name'). '='. $db->quote($this->updateName));
		  
		  $query->update($db->quoteName('#__update_sites'))->set($fields)->where($conditions);
		  $db->setQuery($query);
		  $result = $db->execute();	
		  return $result;
		
		} catch(Exception $e){
			$this->setError('Something went wrong with saving the data');
			return false;
		}


	}
	
	protected function populateState()
	{
		// Set the component (option) we are dealing with.
		$this->setState('component.option', 'com_googlebasexml');
	}
	
	
	public function validate($form, $data, $group = null)
	{
		// Filter and validate the form data.



		return $data;
	}
	
	
	
}
