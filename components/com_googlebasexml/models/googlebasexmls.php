<?php
/*
copyright 2011 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.model' );
jimport( 'joomla.application.component.helper' );
//require_once(JPATH_SITE . '/components/com_googlebasexml/helpers/googlebasexml.php');

/**
 * Googlebasexml Model
 *
 */
class GooglebasexmlModelGooglebasexmls extends JModelLegacy
{
	
	var $_data;
	
	function __construct()
	{
		$app = JFactory::getApplication();
		$option = 'com_googlebasexml';
		parent::__construct();
		
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
		$limitstart = $app->getUserStateFromRequest($option.'.limitstart', 'limitstart', 0);
		
		$this->setState('limit', $limit);
		$this->setState('limitstart', $limitstart);				
		
    }
	
	
	function _buildQuery()
	{
	   $db = $this->_db;
	   $query = $db->getQuery(true);
	   $query->select("*");
	   $query->from("#__googlebasexml");
	   $where = $this->_buildQueryWhere();
	   if(!empty($where))
	   {
		   $query->where($where);
	   }
	   $query->order($this->_buildQueryOrderBy());
	   //$query = 'SELECT * FROM `#__googlebasexml` '. $this->_buildQueryWhere(). $this->_buildQueryOrderBy();		

		return $query;
	}
	
	function _buildQueryWhere()
	{
	  // global $mainframe, $option;
	   $app = JFactory::getApplication();
	   $option = 'com_googlebasexml';
	   
	   $filter_state = $app->getUserStateFromRequest( $option.'.filter_state','filter_state');
	   $filter_search = $app->getUserStateFromRequest( $option.'.filter_search','filter_search',"","string");	      	     
	   
	   $where = array();
	   
	   if ($filter_state == 'P')
	   {
	      $where[] = 'published = 1';
	   }
	   else if ($filter_state == 'U')
	   {
	      $where[] = 'published = 0';	   
	   }
	   
	   $filter_search = trim($filter_search);
	   
	   if (!empty($filter_search))
	   {
	     $filter_search = strtolower($filter_search);
		 $db = $this->_db;
		 $filter_search = $db->escape($filter_search, true);
		 $where[] = ' (LOWER(title) LIKE '.$db->quote('%'.$filter_search.'%")');
	   
	   }
	   
	   return (count($where)) ? implode(' AND ', $where): '';	
	   
    }
	
	function _buildQueryOrderBy()
	{
	    //global $mainframe, $option;
		$app = JFactory::getApplication('site');
		$option = 'com_googlebasexml';
	   
	    //Array of allowable order fields
	    $orders = array('id', 'title', 'published', 'ordering');
	   
		$filter_order = $app->getUserStateFromRequest($option.'.filter_order', 'filter_order', 'id','cmd');
		$filter_order_Dir = $app->getUserStateFromRequest($option.'.filter_order_Dir', 'filter_order_dir', 'ASC','cmd');
		
		if ($filter_order_Dir != 'DESC')
		{
		  $filter_order_Dir = 'ASC';
		}
		
		if (!in_array($filter_order, $orders))
		{
		  $filter_order = 'id';
		}
	   
	    return $filter_order .' '.$filter_order_Dir;
	
	}

	/**
	 * Retrieves the product data
	 * @return array Array of objects containing the data from the database
	 */
	function getData()
	{
		// Lets load the data if it doesn't already exist
		if (empty( $this->_data ))
		{
			$query = $this->_buildQuery();
			$limitstart = $this->getState('limitstart');
			$limit = $this->getState('limit');
		    $this->_data = $this->_getList($query, $limitstart, $limit);
			
			//$this->_data = $this->_getList( $query );
		}

		return $this->_data;
	}
	
		/**
	* get a pagination object
	*
	* @access public
	* return JPagination
	*/	
	function getPagination()
	{
	  if (empty($this->_pagination))
	  {
	    //import the pagination library
		jimport('joomla.html.pagination');
		
		$total = $this->getTotal();
		$limitstart = $this->getState('limitstart');
		$limit = $this->getState('limit');
		
		$this->_pagination = new JPagination($total, $limitstart, $limit);
	  }
	  
	  return $this->_pagination;
	
	}
	
	/**
	* get number of items
	*
	* @access public
	* return integer
	*/
	function getTotal()
	{
	  if (empty($this->_total))
	  {
	    $query = $this->_buildQuery();
		$this->_total = $this->_getListCount($query);
	  }
	  return $this->_total;
	
	}
	
	function reorder( $catid )
	{
	   $row = $this->getTable();
	   $db = $row->getDBO();
	   $row->reorder();		
	}
		
	
}