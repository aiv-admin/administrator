 <?php
/**
* @version		$Id:default.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Models
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/


// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');

JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_googlebasexml/tables');

class GooglebasexmlModelVmcats extends JModelList
{
	public $lang = "en_gb";
	
	public function __construct($config = array())
	{
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
							'virtuemart_category_id','a.virtuemart_category_id',
							'category_parent_id','c.category_parent_id',							
							'category_name', 'a.category_name'
                        );
        }
		
		
		$this->lang = GooglebasexmlHelper::getVMLang();
	   
		
		

		parent::__construct($config);		
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
			parent::populateState();
			$app = JFactory::getApplication();
            $config = JFactory::getConfig();
			$id = $app->input->getInt('virtuemart_category_id', null);
			$this->setState('catlist.id', $id);			
			
			// Load the filter state.
			$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search',"","string");
			$this->setState('filter.search', $search);

			$app = JFactory::getApplication();
			$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $config->get('list_limit'),"int");
			$limit = $value;
			$this->setState('list.limit', $limit);
			
			$value = $app->getUserStateFromRequest($this->context.'.limitstart', 'limitstart', 0, "int");
			$limitstart = ($limit != 0 ? (floor($value / $limit) * $limit) : 0);
			$this->setState('list.start', $limitstart);
			
			$value = $app->getUserStateFromRequest($this->context.'.ordercol', 'filter_order', $ordering, "cmd");
			$this->setState('list.ordering', $value);			
			$value = $app->getUserStateFromRequest($this->context.'.orderdirn', 'filter_order_Dir', $direction, "cmd");
			$this->setState('list.direction', $value);
			
			$language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '', 'cmd');
			$this->setState('filter.language', $language);
			
			
			

					
	}
    		
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('catlist.id');
						return parent::getStoreId($id);
	}	
	
	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 */
	protected function getListQuery()
	{
		
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);		
		$query->select('a.virtuemart_category_id,a.category_name,c.category_parent_id');
		$query->from('#__virtuemart_categories_'.$db->escape($this->lang).' AS a');
		$query->leftJoin('#__virtuemart_category_categories as c ON a.virtuemart_category_id = c.category_child_id');
	
		 				// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.virtuemart_category_id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.category_name LIKE ' . $search .' )');
			}
		}
				
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'category_parent_id');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if(empty($orderCol)) $orderCol = 'category_parent_id';
		if(empty($orderDirn)) $orderDirn = 'ASC'; 		
		$query->order($db->escape($orderCol . ' ' . $orderDirn));
							
		return $query;
	}	
	
	//gets the complete category paths for the cats
	function addCategoryPaths(&$items)
	{
		foreach($items as &$item)
		{
			
			$item->category_path = $this->get_category_names($item->virtuemart_category_id);
		}
		
	}
	
	/**
	 * Returns the category names
	 *
	 * @param int $cid
	 * @return string The category names
	 */
	function get_category_names($cid) {
		$cat_list = $this->get_navigation_list($cid);
		$catnames = "";
		if(isset($cat_list[0]['category_name']))
		{
		  $catnames = $cat_list[0]['category_name'];
		}
		
		if(count($cat_list) > 1)
		{
		  for($i = 1; $i<count($cat_list); $i++)
		  {
			 $catnames =  $cat_list[$i]['category_name'].' > '. $catnames;
		  }
			
		}
		return $catnames;
	}
	
	function get_navigation_list($category_id = 0) {
		
		$db	= $this->getDbo();
		$i=0;
		$category_list = array();
		if($category_id == 0){ return $category_list;}
		
		$query = $db->getQuery(true);
		$query->select(array('c.virtuemart_category_id', 'l.category_name', 'cc.category_parent_id', 'cc.category_child_id'));
		$query->from('#__virtuemart_categories AS c');
		$query->leftJoin("#__virtuemart_categories_".$db->escape($this->lang)." AS l ON c.virtuemart_category_id = l.virtuemart_category_id");
		$query->leftJoin("#__virtuemart_category_categories AS cc ON c.virtuemart_category_id = cc.category_child_id");
		$query->where("c.virtuemart_category_id=".(int)$category_id);
		
		$db->setQuery($query);   
		if(! $row = $db->loadObject()){ /*echo $db->getErrorMsg(); */ return $category_list; }
		
		$category_list[$i]['category_id'] = $row->virtuemart_category_id;
		$category_list[$i]['category_name'] = $row->category_name;
		if (isset($row->category_parent_id) && ! empty($row->category_parent_id)) {
			$i++;
			$category_list = array_merge( $category_list, $this->get_navigation_list($row->category_parent_id) );
		}
		
		return $category_list;
	}		
}