 <?php
/**
* @version		$Id:default.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Models
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');
jimport('joomla.application.component.helper');


JTable::addIncludePath(JPATH_ROOT.'/administrator/components/com_googlebasexml/tables');

class GooglebasexmlModelproducts extends JModelList
{
	public $vmlang = "";
	
	public function __construct($config = array())
	{
		$this->vmlang = GooglebasexmlHelper::getVMLang();
		
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                            'title', 'a.title',
							'g_item_group_id', 'a.g_item_group_id',
                            'g_google_category', 'a.g_google_category',
                            'g_condition', 'a.g_condition',
                            'g_brand', 'a.g_brand',
                            'g_gtin', 'a.g_gtin',
                            'g_mpn', 'a.g_mpn',
                            'g_shipping_label', 'a.g_shipping_label',
                            'g_is_bundle', 'a.g_is_bundle',
                            'g_adult', 'a.g_adult',
                            'g_adwords_redirect', 'a.g_adwords_redirect',
                            'g_gender', 'a.g_gender',
                            'g_age_group', 'a.g_age_group',
                            'g_color', 'a.g_color',
                            'g_size', 'a.g_size',
                            'g_size_type', 'a.g_size_type',
                            'g_size_system', 'a.g_size_system',
                            'g_material', 'a.g_material',
                            'g_pattern', 'a.g_pattern',
                            'g_custom_label_0', 'a.g_custom_label_0',
                            'g_custom_label_1', 'a.g_custom_label_1',
                            'g_custom_label_2', 'a.g_custom_label_2',
                            'g_custom_label_3', 'a.g_custom_label_3',
                            'g_custom_label_4', 'a.g_custom_label_4',
                            'g_excluded_destination', 'a.g_excluded_destination',
                            'g_energy_efficiency_class', 'a.g_energy_efficiency_class',
                            'g_promotion_id', 'a.g_promotion_id',
                            'published', 'a.published',
                            'id', 'a.id',
							'product_name','b.product_name',
							'language','a.language'
                        );
        }

		parent::__construct($config);		
	}
	
	protected function populateState($ordering = null, $direction = null)
	{
			parent::populateState();
			$app = JFactory::getApplication();
            $config = JFactory::getConfig();
			$id = $app->input->getInt('id', null);
			$this->setState('productlist.id', $id);			
			
			// Load the filter state.
			$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search',"","string");
			$this->setState('filter.search', $search);

			$app = JFactory::getApplication();
			$value = $app->getUserStateFromRequest('global.list.limit', 'limit', $config->get('list_limit'),"int");
			$limit = $value;
			$this->setState('list.limit', $limit);
			
			$value = $app->getUserStateFromRequest($this->context.'.limitstart', 'limitstart', 0, "int");
			$limitstart = ($limit != 0 ? (floor($value / $limit) * $limit) : 0);
			$this->setState('list.start', $limitstart);
			
			$value = $app->getUserStateFromRequest($this->context.'.ordercol', 'filter_order', $ordering, 'cmd');
			$this->setState('list.ordering', $value);			
			$value = $app->getUserStateFromRequest($this->context.'.orderdirn', 'filter_order_Dir', $direction, 'cmd');
			$this->setState('list.direction', $value);

			$state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', '', 'cmd');
			$this->setState('filter.state', $state);
			
			$language = $this->getUserStateFromRequest($this->context . '.filter.language', 'filter_language', '', 'cmd');
			$this->setState('filter.language', $language);
			
			
			$value = $this->getUserStateFromRequest($this->context . '.list.columns', 'columns',array(),'array');
			if(in_array('all',$value))
			{
			   $value = array();	
			}
			$this->setState('list.columns', $value);
					
	}
    		
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id	.= ':'.$this->getState('productlist.id');
		$id .= ':' . $this->getState('filter.state');
		$id .= ':' . $this->getState('filter.language');
		return parent::getStoreId($id);
	}	
	
	/**
	 * Method to get a JDatabaseQuery object for retrieving the data set from a database.
	 *
	 * @return	object	A JDatabaseQuery object to retrieve the data set.
	 */
	protected function getListQuery()
	{
		$language = $this->getState('filter.language');
		if(!empty($language) && $language != "*")
		{
			$active_langs = GooglebasexmlHelper::getActiveLangs();
			if(in_array($language,$active_langs))
			{
				$this->vmlang = $language;
			}
		}
		
		
		$db		= $this->getDbo();
		$query	= $db->getQuery(true);	
		$columns = $this->getState('list.columns');
		if(empty($columns))
		{
			$query->select('a.*,b.product_name');
		}
		else
		{
			foreach($columns as $column)
			{
				$column = $db->escape($column);
			}
			$fields = implode(",",$columns);
			$query->select($fields.',a.title,a.virtuemart_product_id,a.id,a.published,b.product_name');
		}
		$query->from('#__googlebasexml_products as a');
		$query->leftJoin('#__virtuemart_products_'.$db->escape($this->vmlang).' AS b ON a.virtuemart_product_id=b.virtuemart_product_id');
		
	
		 				// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.title LIKE ' . $search . '  OR a.g_item_group_id LIKE ' . $search .'  OR a.g_google_category LIKE ' . $search . '  OR a.g_condition LIKE ' . $search . '  OR a.g_brand LIKE ' . $search . '  OR a.g_gtin LIKE ' . $search . '  OR a.g_mpn LIKE ' . $search . '  OR a.g_shipping_label LIKE ' . $search . '  OR a.g_is_bundle LIKE ' . $search . '  OR a.g_adult LIKE ' . $search . '  OR a.g_adwords_redirect LIKE ' . $search . '  OR a.g_gender LIKE ' . $search . '  OR a.g_age_group LIKE ' . $search . '  OR a.g_color LIKE ' . $search . '  OR a.g_size LIKE ' . $search . '  OR a.g_size_type LIKE ' . $search . '  OR a.g_size_system LIKE ' . $search . '  OR a.g_material LIKE ' . $search . '  OR a.g_pattern LIKE ' . $search . '  OR a.g_custom_label_0 LIKE ' . $search . '  OR a.g_custom_label_1 LIKE ' . $search . '  OR a.g_custom_label_2 LIKE ' . $search . '  OR a.g_custom_label_3 LIKE ' . $search . '  OR a.g_custom_label_4 LIKE ' . $search . '  OR a.g_excluded_destination LIKE ' . $search . '  OR a.g_energy_efficiency_class LIKE ' . $search . '  OR a.g_promotion_id LIKE ' . $search . '  OR b.product_name LIKE ' . $search . ' )');
			}
		}
				
		$published = $this->getState('filter.state');
		
		if (is_numeric($published))
		{
			$query->where('a.published = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.published IN (0, 1))');
		}
		
		if(!empty($language) && $language != "*")
		{
			$query->where('a.language='.$db->quote($language));
		}
		
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'id');
		$orderDirn = $this->state->get('list.direction', 'ASC');
		if(empty($orderCol)) $orderCol = 'id';
		if(empty($orderDirn)) $orderDirn = 'ASC'; 		
		$query->order($db->escape($orderCol . ' ' . $orderDirn));
							
		return $query;
	}	
}