<?php
/*
copyright 20017 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );
jimport('joomla.filesystem.file');

class GooglebasexmlViewGooglebasexml extends JViewLegacy
{
	/**
	 * Googlebasexml view display method
	 * @return void
	 **/
	function display($tpl = null)
	{
			$config = JFactory::getConfig();
			$log_path = $config->get('log_path');
			$jinput = JFactory::getApplication()->input;
			$feedId = $jinput->get('id',0,'int');
			$logfile = $log_path .'/'.'googlebasexml.'.$feedId.'.php';
			do
			{
			  //clean all output buffers
			} while(@ob_end_clean());
			
			@header ( "Content-Type: text/plain; charset = utf-8", true);	
			
			if(JFile::exists($logfile))
			{
				$output = file_get_contents($logfile);
			}
			else
			{
				$output = JText::_('');
			}
			echo $output;
			jexit();



	}
	
	
	
	
}


?>