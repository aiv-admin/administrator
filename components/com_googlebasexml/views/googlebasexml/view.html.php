<?php
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );

/**
 * Googlebasexmls View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class GooglebasexmlViewGooglebasexml extends JViewLegacy
{
	/**
	 * Googlebasexml view display method
	 * @return void
	 **/
	function display($tpl = null)
	{
		$data = $this->get('Data');
		$form = $this->get('Form');
		$component	= $this->get('Component');
		$params = $this->get('Params');
		
		JToolBarHelper::title('GooglebaseXML - Product Feed for Google Merchants');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		JHtml::_('stylesheet','media/com_googlebasexml/tabs.css',array(), false, false, false, false);

		
		//get other html
		$lists = array();
		$states = array();
		$states[ 0 ] = ($data->published == 0) ? 'selected="selected"':'';
		$states[ 1 ] = ($data->published == 1) ? 'selected="selected"':''; 
		
		$lists['published'] = '<select name="tform[published]" id="tform_published"><option value="0" ' . $states[0] . '>unpublished</option><option value="1" ' . $states[1] .'>published</option></select>';
		if (!isset($data->ordering))
		{
           $ordering = 0;		
		}
		else
		{
           $ordering = $data->ordering;				
		}
		$lists[ 'ordering' ] = '<input type="hidden" name="tform[ordering]" id="tform_ordering" value="' . $ordering. '" />';
		
		JToolBarHelper::apply('googlebasexml.apply');
		JToolBarHelper::save('googlebasexml.save');
		JToolBarHelper::save2copy('googlebasexml.save2copy');
		JToolBarHelper::cancel('googlebasexml.cancel');
		
	    $baseurl = str_replace('/administrator','',JURI::base());	
		$lists['link'] = $baseurl . 'index.php?option=com_googlebasexml&amp;format=xml&amp;id='.$data->id;
		$lists['texttext'] = JText::_('TEXTTEXT');
		$lists['textlink'] = $baseurl . 'index.php?option=com_googlebasexml&amp;format=xml&amp;id='.$data->id.'&amp;output=text';
		$lists['downloadlink'] = $baseurl . 'index.php?option=com_googlebasexml&amp;format=xml&amp;id='.$data->id.'&amp;output=download';
		
		if(isset($params["token"]) && !empty($params["token"]))
		{
			$lists['link'] .= "&token=".htmlspecialchars(rawurlencode($params["token"]));
			$lists['textlink'] .= "&token=".htmlspecialchars(rawurlencode($params["token"]));
			$lists['downloadlink'] .= "&token=".htmlspecialchars(rawurlencode($params["token"]));
			
		}

		if(isset($params["product_language"]) && !empty($params["product_language"]))
		{
			$lists['link'] .= "&lang=".htmlspecialchars(rawurlencode($params["product_language"]));
			$lists['textlink'] .= "&lang=".htmlspecialchars(rawurlencode($params["product_language"]));
			$lists['downloadlink'] .= "&lang=".htmlspecialchars(rawurlencode($params["product_language"]));
			
		}


		
	    $this->lists = $lists;		
		$this->data = $data;
		$this->form = $form;
		$this->component = $component;
		$this->params = $params;
		
		

		$this->document->setTitle(JText::_('GooglebaseXML Parameters'));
        $jinput = JFactory::getApplication()->input;	
		$jinput->set('hidemainmenu', true);
		

		parent::display($tpl);
	}		
		
}
