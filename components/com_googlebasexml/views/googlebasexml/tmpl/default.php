<?php 
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

defined('_JEXEC') or die('Restricted access'); 

$template = JFactory::getApplication()->getTemplate();

// Load the tooltip behavior.
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if(!version_compare(JVERSION,'3','<')){
   JHtml::_('behavior.multiselect');
   //JHtml::_('formbehavior.chosen', 'select');	
}
//the main data are in array tform
//the config data are in array jform
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if(task == 'googlebasexml.cancel')
		{
			Joomla.submitform(task, document.getElementById('component-form'));
		}
		else
		{
		  var title = document.getElementById('tform_title');
		  if(title.value == '')
		  {
			alert('<?php echo JText::_('COM_GOOGLEBASEXML_TITLE_REQUIRED'); ?>');	
			title.focus();
			return;
			
		  }
		  
		  if (document.formvalidator.isValid(document.id('component-form'))) {
			  Joomla.submitform(task, document.getElementById('component-form'));
		  }
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_googlebasexml');?>" id="component-form" method="post" name="adminForm" autocomplete="off" class="form-validate">
    <fieldset>
    <h2><?php echo JText::_('GOOGLEBASEXMLPREVIEW'); ?></h2>
    <?php echo JText::_('GOOGLEBASEXMLPREVIEWINFO'); ?>
    <p><a href="<?php echo $this->lists['link']; ?>" target="_blank"><?php echo $this->lists['link']; ?></a></p>
    <?php if(isset($this->params["outputText"]) && $this->params["outputText"] == 'yes'): ?>
      <p><?php echo $this->lists['texttext']; ?></p>
      <p><a href="<?php echo $this->lists['textlink']; ?>" target="_blank"><?php echo JText::_('GOOGLEBASEXMLTEXTFEED'); ?></a></p>
    <?php endif; ?>
    <p><?php echo JText::_('GOOGLEBASEXML_DOWNLOAD_LINK_DESC'); ?></p>
    <p><a href="<?php echo $this->lists['downloadlink']; ?>" target="_blank"><?php echo JText::_('GOOGLEBASEXML_DOWNLOAD'); ?></a></p>

    </fieldset>
    <fieldset>
		<legend><?php echo JText::_( 'Details' ); ?></legend>

		<table class="admintable">
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Feed Name' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="tform[title]" id="tform_title" size="32" maxlength="250" value="<?php echo $this->escape($this->data->title);?>" />
			</td>
            <td rowspan="3">
            </td>
		</tr>
		<tr>
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'Alias' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="tform[alias]" id="tform_alias" size="32" maxlength="250" value="<?php echo $this->escape($this->data->alias);?>" />
			</td>
		</tr>  
		<tr>
		   <td class="key">
				<label for="published">
					<?php echo JText::_( 'Published' ); ?>:
				</label>
			</td>

		    <td>
			 <?php echo $this->lists[ 'published' ]; ?>

			</td>
		</tr>		
              
        </table>
        </fieldset>
	<fieldset>
		<div class="configuration" >
			<?php echo JText::_('COM_GOOGLEBASEXML_CONFIGURATION') ?>
		</div>
	</fieldset>
        
	<?php
	echo JHtml::_('tabs.start', 'config-tabs-'.$this->component->option.'_configuration', array('useCookie'=>1));
		$fieldSets = $this->form->getFieldsets();
		foreach ($fieldSets as $name => $fieldSet) :
		    if(!empty($fieldSet->hidden)){continue;}
			$label = empty($fieldSet->label) ? 'COM_CONFIG_'.$name.'_FIELDSET_LABEL' : $fieldSet->label;
			echo JHtml::_('tabs.panel', JText::_($label), 'publishing-details');
			if (isset($fieldSet->description) && !empty($fieldSet->description)) :
				echo '<p class="tab-description">'.JText::_($fieldSet->description).'</p>';
			endif;
	?>
			<?php
			foreach ($this->form->getFieldset($name) as $fieldName => $field):
			?>
				<?php if (!$field->hidden) : ?>
				<?php echo $field->label; ?>
				<?php endif; ?>
				<?php echo $field->input; ?><br /><br style="clear:both;"/>
			<?php
			endforeach;
			?>


	<div class="clr"></div>
	<?php
		endforeach;
	echo JHtml::_('tabs.end');
	?>
	<div>
		<input type="hidden" name="id" value="<?php echo $this->data->id;?>" />
		<input type="hidden" name="component" value="<?php echo $this->component->option;?>" />
		<input type="hidden" name="task" value="" />
        <?php echo $this->lists[ 'ordering' ]; ?>
        
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>



