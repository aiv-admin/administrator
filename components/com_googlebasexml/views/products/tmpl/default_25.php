<?php
/**
* @version		$Id:default_25.php 1 2015-03-13 13:44:59Z fcoulter $
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
  JFactory::getDocument()->addStyleSheet(JURI::base().'/components/com_googlebasexml/assets/lists-j25.css');
  $user		= JFactory::getUser();
  $userId		= $user->get('id');
  $listOrder	= $this->escape($this->state->get('list.ordering'));
  $listColumns	= $this->state->get('list.columns');
  foreach($listColumns as &$lc)
  {
	  $lc = $this->escape($lc);
  }
  $columnFields = $this->getColumnFields();  
  $listDirn	= $this->escape($this->state->get('list.direction'));  
  JHtml::_('behavior.tooltip');
?>
<div style="margin:4px; padding:4px; background-color:#dddddd;"><?php echo JText::_('GOOGLEBASEXML_EXPLAIN_OVERRIDES'); ?></div>

<form action="index.php?option=com_googlebasexml&amp;view=product" method="post" name="adminForm" id="adminForm">
	<table>
		<tr>
			<td align="left" width="100%">
				<div id="filter-bar" class="btn-toolbar">
					<div class="filter-search btn-group pull-left">
						<label class="element-invisible" for="filter_search"><?php echo JText::_( 'Filter' ); ?>:</label>
						<input type="text" name="search" id="filter_search" value="<?php  echo $this->escape($this->state->get('filter.search'));?>" class="text_area" onchange="document.adminForm.submit();" />
						<button class="btn" onclick="this.form.submit();"><?php if(version_compare(JVERSION,'3.0','lt')): echo JText::_( 'Go' ); else: ?><i class="icon-search"></i><?php endif; ?></button>
						<button type="button" class="btn" onclick="document.getElementById('search').value='';this.form.submit();"><?php if(version_compare(JVERSION,'3.0','lt')): echo JText::_( 'Reset' ); else: ?><i class="icon-remove"></i><?php endif; ?></button><br/><br/>
					</div>
                  <div class="btn-group pull-left">
                      <label for="columns" class="element"><?php echo JText::_('Choose Columns To Display');?></label><br/>
                      <select name="columns[]" id="columns" class="input-large" multiple="multiple" size="8">
                          <option value=""><?php echo JText::_('Select Columns');?></option>
                          <option value="all"><?php echo JText::_('All Columns');?></option>                        
                          <?php echo JHtml::_('select.options', $columnFields, 'value', 'text', $listColumns);?>
                      </select>
                      <button type="submit" class="btn hasTooltip" title="<?php echo JText::_('Fetch Columns'); ?>"><?php echo JText::_( 'Go' ); ?></button>   
 					  <button type="button" class="btn" onclick="document.getElementById('columns').value='all';this.form.submit();"><?php echo JText::_( 'Reset' );?></button>
                     
                  </div>
                    
				</div>					
			</td>
			<td nowrap="nowrap">
				<?php
 				  	echo JHTML::_('grid.state', $this->state->get('filter.state'), 'Published', 'Unpublished', 'Archived', 'Trashed');
  				?>
		
			</td>
		</tr>		
	</table>
<div id="editcell">
	<table class="adminlist table table-striped">
		<thead>
			<tr>
				<th width="5">
					<?php echo JText::_( 'NUM' ); ?>
				</th>
				<th width="20">				
					<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
				</th>
				<th class="title">
					<?php echo JHTML::_('grid.sort', JText::_('Virtuemart Product'), 'a.virtuemart_product_id', $listDirn, $listOrder ); ?>
				</th>
				<th class="title">
					<?php echo JHTML::_('grid.sort', JText::_('Product Name'), 'a.title', $listDirn, $listOrder ); ?>
				</th>
                <?php foreach($columnFields as $key=>$value): ?>
				  <?php if($this->isDisplayed($key)): ?>
                   <th class="title">
                      <?php echo JHTML::_('grid.sort', $value, $key, $listDirn, $listOrder ); ?>
                  </th>
                  <?php endif; ?>
                <?php endforeach; ?>
				<th class="title">
					<?php echo JHTML::_('grid.sort', JText::_('Published'), 'a.published', $listDirn, $listOrder ); ?>
				</th>

				<th class="title">
					<?php echo JHTML::_('grid.sort', 'Id', 'a.id', $listDirn, $listOrder ); ?>
				</th>
			</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="12">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
<?php
  $k = 0;
  if (count( $this->items ) > 0 ):
  
  for ($i=0, $n=count( $this->items ); $i < $n; $i++):
  
  	$row = $this->items[$i];

    
 	$link = JRoute::_( 'index.php?option=com_googlebasexml&view=product&task=product.edit&id='. $row->id );
 	//$row->id = $row->id;
 	
	$checked = JHTML::_('grid.id', $i, $row->id);
	$published = JHTML::_('grid.published', $row, $i, 'tick.png', 'publish_x.png', $prefix = 'product.' );

  	
 	
  ?>
	<tr class="<?php echo "row$k"; ?>">
		
		<td align="center"><?php echo $this->pagination->getRowOffset($i); ?>.</td>
        
        <td><?php echo $checked  ?></td>
		<td><?php echo $row->virtuemart_product_id; ?>. <a href="<?php echo $link; ?>"><?php  echo $this->escape($row->product_name); ?></a></td>
	<td>
						
				<a href="<?php echo $link; ?>"><?php echo $this->escape($row->title); ?></a>
								
		</td>	

					   <?php foreach($columnFields as $key=>$value): ?>
                          <?php if($this->isDisplayed($key)): ?>
                           <td>
                              <?php $prop = str_replace('a.','',$key); echo $this->escape($row->$prop); ?>
                          </td>
                          <?php endif; ?>
                        <?php endforeach; ?>
                        
            <td><?php echo $published; ?></td>        
			<td><?php echo $row->id; ?></td>		
	
	</tr>		
<?php
  $k = 1 - $k;
  endfor;
  else:
  ?>
	<tr>
		<td colspan="12">
			<?php echo JText::_( 'GOOGLEBASEXML_NO_ITEMS_PRESENT' ); ?>
		</td>
	</tr>
	<?php
  endif;
  ?>
</tbody>
</table>
</div>
<input type="hidden" name="option" value="com_googlebasexml" />
<input type="hidden" name="task" value="product" />
<input type="hidden" name="view" value="products" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>  	