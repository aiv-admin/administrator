 <?php
/**
* @version		$Id:default.php 1 2015-03-13 13:44:59Z fcoulter $
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
if(!version_compare(JVERSION,'3','<')){
	
	JHtml::_('bootstrap.tooltip');
	JHtml::_('behavior.multiselect');
	JHtml::_('dropdown.init');
	JHtml::_('formbehavior.chosen', 'select');
}

$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$listColumns	= $this->state->get('list.columns');
foreach($listColumns as &$lc)
{
	$lc = $this->escape($lc);
}
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
$params		= (isset($this->state->params)) ? $this->state->params : new JObject;
$saveOrder	= $listOrder == 'ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_googlebasexml&task=products.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
$columnFields = $this->getColumnFields();
?>

<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<div style="margin:4px; padding:4px; background-color:#dddddd;"><?php echo JText::_('GOOGLEBASEXML_EXPLAIN_OVERRIDES'); ?></div>
<form action="index.php?option=com_googlebasexml&view=product" method="post" name="adminForm" id="adminForm"><br style="clear:both;" />

	<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">				
				<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" />
			</div>
			<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="columns" class="element"><?php echo JText::_('Choose Columns To Display');?></label>
				<select name="columns[]" id="columns" class="input-medium" multiple="multiple"  onchange="">
					<option value=""><?php echo JText::_('Choose Columns');?></option>
                    <option value="all"><?php echo JText::_('All Columns');?></option>
					<?php echo JHtml::_('select.options', $columnFields, 'value', 'text', $listColumns);?>
				</select>
				<button type="submit" class="btn hasTooltip" title="<?php echo JText::_('Fetch Columns'); ?>"><i class="icon-search"></i></button>   
  			    <button type="button" class="btn" onclick="document.getElementById('columns').value='all';this.form.submit();"><i class="icon-remove"></i></button>
               
			</div>
		</div>
		<div class="clearfix"> </div>

	
<div id="editcell">
	<table class="adminlist table table-striped" id="articleList">
		<thead>
			<tr>
					
				<th width="20">
					<input type="checkbox" name="checkall-toggle" value="" title="(<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', JText::_('Virtuemart Product'), 'a.virtuemart_product_id', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', JText::_('Product Name'), 'a.title', $listDirn, $listOrder ); ?>
				</th>
                
                <?php foreach($columnFields as $key=>$value): ?>
				  <?php if($this->isDisplayed($key)): ?>
                   <th class="title">
                      <?php echo JHTML::_('grid.sort', $value, $key, $listDirn, $listOrder ); ?>
                  </th>
                  <?php endif; ?>
                <?php endforeach; ?>
                
				<th class="title">
					<?php echo JHTML::_('grid.sort', 'Published', 'a.published', $listDirn, $listOrder ); ?>
				</th>
 
				<th class="title">
					<?php echo JHTML::_('grid.sort', 'Id', 'a.id', $listDirn, $listOrder ); ?>
				</th>
							</tr> 			
		</thead>
		<tfoot>
		<tr>
			<td colspan="30">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>
<?php
  if (count($this->items)) : 
  		foreach ($this->items as $i => $item) :
																$canCreate  = $user->authorise('core.create');
				$canEdit    = $user->authorise('core.edit');				
				$canChange  = $user->authorise('core.edit.state'); 				
					
				$disableClassName = '';
				$disabledLabel	  = '';
				if (!$saveOrder) {
					$disabledLabel    = JText::_('JORDERINGDISABLED');
					$disableClassName = 'inactive tip-top';
				} 
	
 				$onclick = "";
  	
    			if (JFactory::getApplication()->input->get('function', null)) {
    				$onclick= "onclick=\"window.parent.jSelectProduct_id('".$item->id."', '".$this->escape($item->title)."', '','id')\" ";
    			}  	
    
 				$link = JRoute::_( 'index.php?option=com_googlebasexml&view=product&task=product.edit&id='. $item->id );
 	
 				
 	
 				$checked = JHTML::_('grid.id', $i, $item->id);
				
				if(empty($item->title))
				{
					$item->title = $item->product_name;
				}
 	 	
  		?>
				<tr class="row<?php echo $i % 2; ?>">
					      
        			<td><?php echo $checked;  ?></td>
						<td><?php echo $item->virtuemart_product_id; ?>. <?php  echo $this->escape($item->product_name); ?></td>
                        
									        <td class="nowrap has-context">
					<div class="pull-left">
														<?php if ($canEdit) : ?>
								<a href="<?php  echo $link; ?>">
									<?php  echo $this->escape($item->title); ?></a>
							<?php  else : ?>
								<?php  echo $this->escape($item->title); ?>
							<?php  endif; ?>
							
						</div>
						<div class="pull-left">
							<?php
								// Create dropdown items
								JHtml::_('dropdown.edit', $item->id, 'product.');
																JHtml::_('dropdown.divider');
								if ($item->published) :
									JHtml::_('dropdown.unpublish', 'cb' . $i, 'products.');
								else :
									JHtml::_('dropdown.publish', 'cb' . $i, 'products.');
								endif;									
								JHtml::_('dropdown.divider');

								if ($archived) :
									JHtml::_('dropdown.unarchive', 'cb' . $i, 'products.');
								else :
									JHtml::_('dropdown.archive', 'cb' . $i, 'products.');
								endif;
								
								if ($trashed) :
									JHtml::_('dropdown.untrash', 'cb' . $i, 'products.');
								else :
									JHtml::_('dropdown.trash', 'cb' . $i, 'products.');
								endif;								
																
								// render dropdown list
								echo JHtml::_('dropdown.render');
								?>
						</div>
						</td>
					   <?php foreach($columnFields as $key=>$value): ?>
                          <?php if($this->isDisplayed($key)): ?>
                           <td>
                              <?php $prop = str_replace('a.','',$key); echo $this->escape($item->$prop); ?>
                          </td>
                          <?php endif; ?>
                        <?php endforeach; ?>

						<td>
							<?php echo JHtml::_('jgrid.published', $item->published, $i, 'products.', $canChange, 'cb'); ?>
						</td>		
								 		
						<td><?php echo $item->id; ?></td>
						</tr>
<?php

  endforeach;
  else:
  ?>
	<tr>
		<td colspan="12">
			<?php echo JText::_( 'There are no items present' ); ?>
		</td>
	</tr>
	<?php
  endif;
  ?>
</tbody>
</table>
</div>
<input type="hidden" name="option" value="com_googlebasexml" />
<input type="hidden" name="task" value="product" />
<input type="hidden" name="view" value="products" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>  	