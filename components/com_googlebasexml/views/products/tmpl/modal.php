<?php
/**
* @version		$Id:modal.php 1 2015-03-13 13:44:59Z fcoulter $
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

defined('_JEXEC') or die;

if(!version_compare(JVERSION,'3','<')){
	JHtml::_('bootstrap.tooltip');
}

$input     = JFactory::getApplication()->input;
$function  = $input->getCmd('function', 'jSelectProduct');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
?>
<form action="<?php echo JRoute::_('index.php?option=com_googlebasexml&view=products&layout=modal&tmpl=component&function='.$function);?>" method="post" name="adminForm" id="adminForm" class="form-inline">
	<fieldset class="filter clearfix">
		<div class="btn-toolbar">
			<div class="btn-group pull-left">
				<label for="filter_search">
					<?php echo JText::_('JSEARCH_FILTER_LABEL'); ?>
				</label>
				<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" size="30" />
			</div>
			<div class="btn-group pull-left">
				<button type="submit" class="btn hasTooltip" data-placement="bottom" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
					<i class="icon-search"></i></button>
				<button type="button" class="btn hasTooltip" data-placement="bottom" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();">
					<i class="icon-remove"></i></button>
			</div>
			<input onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>('0', '<?php echo $this->escape(addslashes(JText::_('SELECT_AN_ITEM'))); ?>', null, null);" class="btn" type="button" value="" />
			<div class="clearfix"></div>
		</div>
		<hr class="hr-condensed" />

	</fieldset>

	<table class="table table-striped table-condensed">
		<thead>
			<tr>				
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'Title', 'a.title', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_google_category', 'a.g_google_category', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_condition', 'a.g_condition', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_brand', 'a.g_brand', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_gtin', 'a.g_gtin', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_mpn', 'a.g_mpn', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_shipping_label', 'a.g_shipping_label', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_is_bundle', 'a.g_is_bundle', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_adult', 'a.g_adult', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_adwords_redirect', 'a.g_adwords_redirect', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_gender', 'a.g_gender', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_age_group', 'a.g_age_group', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_color', 'a.g_color', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_size', 'a.g_size', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_size_type', 'a.g_size_type', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_size_system', 'a.g_size_system', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_material', 'a.g_material', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_pattern', 'a.g_pattern', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_custom_label_0', 'a.g_custom_label_0', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_custom_label_1', 'a.g_custom_label_1', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_custom_label_2', 'a.g_custom_label_2', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_custom_label_3', 'a.g_custom_label_3', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_custom_label_4', 'a.g_custom_label_4', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_excluded_destination', 'a.g_excluded_destination', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_energy_efficiency_class', 'a.g_energy_efficiency_class', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'G_promotion_id', 'a.g_promotion_id', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'Published', 'a.published', $listDirn, $listOrder ); ?>
				</th>
								<th class="title">
					<?php echo JHTML::_('grid.sort', 'Id', 'a.id', $listDirn, $listOrder ); ?>
				</th>
							</tr>
		</thead>
		<tfoot>
		<tr>
			<td colspan="28">
				<?php echo $this->pagination->getListFooter(); ?>
			</td>
		</tr>
	</tfoot>
	<tbody>

		<?php foreach ($this->items as $i => $item) : ?>
			<tr class="row<?php  echo $i % 2; ?>">
											<td>
					<a class="pointer" onclick="if (window.parent) window.parent.<?php echo $this->escape($function);?>('<?php echo $item->id; ?>', '<?php echo $this->escape(addslashes($item->name)); ?>');">
						<?php echo $this->escape($item->title); ?></a>
				</td>
											 		
				<td><?php echo $item->g_google_category; ?></td>
											 		
				<td><?php echo $item->g_condition; ?></td>
											 		
				<td><?php echo $item->g_brand; ?></td>
											 		
				<td><?php echo $item->g_gtin; ?></td>
											 		
				<td><?php echo $item->g_mpn; ?></td>
											 		
				<td><?php echo $item->g_shipping_label; ?></td>
											 		
				<td><?php echo $item->g_is_bundle; ?></td>
											 		
				<td><?php echo $item->g_adult; ?></td>
											 		
				<td><?php echo $item->g_adwords_redirect; ?></td>
											 		
				<td><?php echo $item->g_gender; ?></td>
											 		
				<td><?php echo $item->g_age_group; ?></td>
											 		
				<td><?php echo $item->g_color; ?></td>
											 		
				<td><?php echo $item->g_size; ?></td>
											 		
				<td><?php echo $item->g_size_type; ?></td>
											 		
				<td><?php echo $item->g_size_system; ?></td>
											 		
				<td><?php echo $item->g_material; ?></td>
											 		
				<td><?php echo $item->g_pattern; ?></td>
											 		
				<td><?php echo $item->g_custom_label_0; ?></td>
											 		
				<td><?php echo $item->g_custom_label_1; ?></td>
											 		
				<td><?php echo $item->g_custom_label_2; ?></td>
											 		
				<td><?php echo $item->g_custom_label_3; ?></td>
											 		
				<td><?php echo $item->g_custom_label_4; ?></td>
											 		
				<td><?php echo $item->g_excluded_destination; ?></td>
											 		
				<td><?php echo $item->g_energy_efficiency_class; ?></td>
											 		
				<td><?php echo $item->g_promotion_id; ?></td>
											 		
				<td><?php echo $item->published; ?></td>
											 		
				<td><?php echo $item->id; ?></td>
										</tr>
			<?php endforeach; ?>
		</tbody>
	</table>

	<input type="hidden" name="task" value="" />
	<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
	<?php echo JHtml::_('form.token'); ?>
</form>