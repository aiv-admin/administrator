<?php
/**
* @version		$Id:product.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Views
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license #http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

 
class GooglebasexmlViewproducts  extends JViewLegacy {


	protected $items;

	protected $pagination;

	protected $state;
	
	protected $columns = array();
	
	
	/**
	 *  Displays the list view
 	 * @param string $tpl   
     */
	public function display($tpl = null)
	{
		
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		
		$this->columns = $this->state->get('list.columns');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		GooglebasexmlHelper::addSubmenu('products');

		$this->addToolbar();
		if(!version_compare(JVERSION,'3','<')){
			$this->sidebar = JHtmlSidebar::render();
		}
		
		if(version_compare(JVERSION,'3','<')){
			$tpl = "25";
		}
		parent::display($tpl);
	}
	
	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 */
	protected function addToolbar()
	{
		
		$canDo = GooglebasexmlHelper::getActions();
		$user = JFactory::getUser();
		JToolBarHelper::title( JText::_( 'Product' ), 'generic.png' );
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('product.add');
		}	
		
		if (($canDo->get('core.edit')))
		{
			JToolBarHelper::editList('product.edit');
		}
		
				
		if ($this->state->get('filter.state') != 2)
		{
			JToolbarHelper::publish('products.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('products.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}
				
		if ($canDo->get('core.edit.state'))
		{
			if ($this->state->get('filter.state') != -1)
			{
				if ($this->state->get('filter.state') != 2)
				{
					JToolbarHelper::archiveList('products.archive');
				}
				elseif ($this->state->get('filter.state') == 2)
				{
					JToolbarHelper::unarchiveList('products.publish');
				}
			}
			
		}
				
				

		if ($this->state->get('filter.state') == -2 && $canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'products.delete', 'JTOOLBAR_EMPTY_TRASH');
		}
		elseif ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::trash('products.trash');
		}		
				
		
		//JToolBarHelper::preferences('com_googlebasexml', '550');  
		if(!version_compare(JVERSION,'3','<')){		
			JHtmlSidebar::setAction('index.php?option=com_googlebasexml&view=products');
		}
		if(!version_compare(JVERSION,'3','<')){
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_state',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
			);
		}
				
		if(!version_compare(JVERSION,'3','<')){
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_LANGUAGE'),
				'filter_language',
				JHtml::_('select.options', $this->getLanguages(true), 'value', 'text', $this->state->get('filter.language'), true)
			);
		}
					
	}	
	

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
		 	          'a.title' => JText::_('Product Name'),
					  'a.virtuemart_product_id' => JText::_('Product ID'),
	     	          'a.g_item_group_id' => JText::_('g:item_group_id'),					  
	     	          'a.g_google_category' => JText::_('g:google_category'),
	     	          'a.g_condition' => JText::_('g:condition'),
	     	          'a.g_brand' => JText::_('g:brand'),
	     	          'a.g_gtin' => JText::_('g:gtin'),
	     	          'a.g_mpn' => JText::_('g:mpn'),
	     	          'a.g_shipping_label' => JText::_('g:shipping_label'),
	     	          'a.g_is_bundle' => JText::_('g:is_bundle'),
	     	          'a.g_adult' => JText::_('g:adult'),
	     	          'a.g_adwords_redirect' => JText::_('g:adwords_redirect'),
	     	          'a.g_gender' => JText::_('g:gender'),
	     	          'a.g_age_group' => JText::_('g:age_group'),
	     	          'a.g_color' => JText::_('g:color'),
	     	          'a.g_size' => JText::_('g:size'),
	     	          'a.g_size_type' => JText::_('g:size_type'),
	     	          'a.g_size_system' => JText::_('g:size_system'),
	     	          'a.g_material' => JText::_('g:material'),
	     	          'a.g_pattern' => JText::_('g:pattern'),
	     	          'a.g_custom_label_0' => JText::_('g:custom_label_0'),
	     	          'a.g_custom_label_1' => JText::_('g:custom_label_1'),
	     	          'a.g_custom_label_2' => JText::_('g:custom_label_2'),
	     	          'a.g_custom_label_3' => JText::_('g:custom_label_3'),
	     	          'a.g_custom_label_4' => JText::_('g:custom_label_4'),
	     	          'a.g_excluded_destination' => JText::_('g:excluded_destination'),
	     	          'a.g_energy_efficiency_class' => JText::_('g:energy_efficiency_class'),
	     	          'a.g_promotion_id' => JText::_('g:promotion_id'),
	     	          'a.published' => JText::_('JSTATUS'),
	     	          'a.id' => JText::_('JGRID_HEADING_ID'),
					  'b.product_name' => JText::_('Product Name'),
					  'a.language' => JText::_('language')
					  
	     		);
	}	
	
	
	protected function getColumnFields()
	{
		return array(
	     	          'a.g_google_category' => JText::_('g:google_category'),
	     	          'a.g_item_group_id' => JText::_('g:g_item_group_id'),					  					  
	     	          'a.g_condition' => JText::_('g:condition'),
	     	          'a.g_brand' => JText::_('g:brand'),
	     	          'a.g_gtin' => JText::_('g:gtin'),
	     	          'a.g_mpn' => JText::_('g:mpn'),
	     	          'a.g_shipping_label' => JText::_('g:shipping_label'),
	     	          'a.g_is_bundle' => JText::_('g:is_bundle'),
	     	          'a.g_adult' => JText::_('g:adult'),
	     	          'a.g_adwords_redirect' => JText::_('g:adwords_redirect'),
	     	          'a.g_gender' => JText::_('g:gender'),
	     	          'a.g_age_group' => JText::_('g:age_group'),
	     	          'a.g_color' => JText::_('g:color'),
	     	          'a.g_size' => JText::_('g:size'),
	     	          'a.g_size_type' => JText::_('g:size_type'),
	     	          'a.g_size_system' => JText::_('g:size_system'),
	     	          'a.g_material' => JText::_('g:material'),
	     	          'a.g_pattern' => JText::_('g:pattern'),
	     	          'a.g_custom_label_0' => JText::_('g:custom_label_0'),
	     	          'a.g_custom_label_1' => JText::_('g:custom_label_1'),
	     	          'a.g_custom_label_2' => JText::_('g:custom_label_2'),
	     	          'a.g_custom_label_3' => JText::_('g:custom_label_3'),
	     	          'a.g_custom_label_4' => JText::_('g:custom_label_4'),
	     	          'a.g_excluded_destination' => JText::_('g:excluded_destination'),
	     	          'a.g_energy_efficiency_class' => JText::_('g:energy_efficiency_class'),
	     	          'a.g_promotion_id' => JText::_('g:promotion_id'),
					  'a.language' => JText::_('language')
	     		);
	}		
	
	protected function isDisplayed($field)
	{
		if(empty($this->columns)){ return true; }
		if(in_array($field,$this->columns))
		{
			return true;
		}
		else
		{
		    return false;	
		}
	}
	
   protected function getLanguages($all = false, $translate = false)
   {
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);

		// Build the query.
		$query->select('a.lang_code AS value, a.title AS text, a.title_native');
		$query->from('#__languages AS a');
		$query->where('a.published >= 0');
		$query->order('a.title');

		// Set the query and load the options.
		$db->setQuery($query);
		$items = $db->loadObjectList();
		if ($all)
		{
			array_unshift($items, new JObject(array('value' => '*', 'text' => $translate ? JText::alt('JALL', 'language') : 'JALL_LANGUAGE')));
		}
		
		foreach($items as &$item)
		{
			$item->value = str_replace("-","_",strtolower($item->value));
		}

		// Detect errors
		if ($db->getErrorNum())
		{
			JError::raiseWarning(500, $db->getErrorMsg());
		}
		
		return $items;
	}		
}
?>
