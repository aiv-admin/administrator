<?php
/**
* @version		$Id:edit.php 1 2015-03-13 13:44:59Z fcoulter $
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');

// Set toolbar items for the page
$edit		= JFactory::getApplication()->input->get('edit', true);
$text = !$edit ? JText::_( 'New' ) : JText::_( 'Edit' );
JToolBarHelper::title(   JText::_( 'Cat' ).': <small><small>[ ' . $text.' ]</small></small>' );
JToolBarHelper::apply('cat.apply');
JToolBarHelper::save('cat.save');
if (!$edit) {
	JToolBarHelper::cancel('cat.cancel');
} else {
	// for existing items the button is renamed `close`
	JToolBarHelper::cancel( 'cat.cancel', 'Close' );
}
?>

<script language="javascript" type="text/javascript">


Joomla.submitbutton = function(task)
{
	if (task == 'cat.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {
		Joomla.submitform(task, document.getElementById('adminForm'));
	}
}

</script>

	 	<form method="post" action="<?php echo JRoute::_('index.php?option=com_googlebasexml&layout=edit&id='.(int) $this->item->id);  ?>" id="adminForm" name="adminForm">
	 	<div class="col <?php if(version_compare(JVERSION,'3.0','lt')):  ?>width-60  <?php endif; ?>span8 form-horizontal fltlft">
		  <fieldset class="adminform">
			<legend><?php echo JText::_( 'Details' ); ?></legend>
		
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('virtuemart_category_id'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('virtuemart_category_id');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('google_category'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('google_category');  ?>
					</div>
				</div>		
					
					
			
						
          </fieldset>                      
        </div>
        <div class="col <?php if(version_compare(JVERSION,'3.0','lt')):  ?>width-30  <?php endif; ?>span2 fltrgt">
		        
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Parameters' ); ?></legend>
		
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('language'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('language');  ?>
					</div>
				</div>		
								
			</fieldset>
			        

        </div>                   
		<input type="hidden" name="option" value="com_googlebasexml" />
	    <input type="hidden" name="cid[]" value="<?php echo $this->item->id ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="view" value="cat" />
		<?php echo JHTML::_( 'form.token' ); ?>
	</form>