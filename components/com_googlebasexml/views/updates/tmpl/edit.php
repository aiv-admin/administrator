<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Googlebasexml
 * @author     Fiona Coulter <forwarder@spiralscripts.co.uk>
 * @copyright  2016 Fiona Coulter
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
if(!version_compare(JVERSION,'3','<')){

JHtml::_('formbehavior.chosen', 'select');
}
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
//$document->addStyleSheet(JUri::root() . 'media/com_googlebasexml/css/form.css');
?>
<script type="text/javascript">

	Joomla.submitbutton = function (task) {
		if (task == 'updates.cancel') {
			Joomla.submitform(task, document.getElementById('updates-form'));
		}
		else {
			
			if (task != 'updates.cancel' && document.formvalidator.isValid(document.id('updates-form'))) {
				
				Joomla.submitform(task, document.getElementById('updates-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<div style="margin:10px; padding:10px; background-color:#dddddd;">
  <?php echo JText::_('COM_GOOGLEBASEXML_UPDATES_EXPLAIN'); ?>
</div>

<form
	action="<?php echo JRoute::_('index.php?option=com_googlebasexml&layout=edit&id=1'); ?>"
	method="post"  name="adminForm" id="updates-form" class="form-validate">

	<div class="form-horizontal">
		<?php //echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php //echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_GOOGLEBASEXML_TITLE_UPDATES', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('download_url'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('download_url'); ?></div>
					</div>
                    <?php if(!empty($this->item->download_url)): ?>
					<div class="control-group">
                      <a href="<?php echo htmlspecialchars($this->item->download_url); ?>" class="btn"><?php echo JText::_('GOOGLEBASEXML_DOWNLOAD_UPDATE'); ?></a>
                    </div>   
                    <?php endif; ?>                 
				</fieldset>
			</div>
		</div>
		<?php //echo JHtml::_('bootstrap.endTab'); ?>

		

		<?php //echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
