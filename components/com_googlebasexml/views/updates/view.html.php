<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Googlebasexml
 * @author     Fiona Coulter <forwarder@spiralscripts.co.uk>
 * @copyright  2016 Fiona Coulter
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class GooglebasexmlViewUpdates extends JViewLegacy
{

	protected $form;
	protected $option = "com_googlebasexml";
	

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
        if (!JFactory::getUser()->authorise('core.admin', $this->option))
		{
			JFactory::getApplication()->redirect('index.php', JText::_('JERROR_ALERTNOAUTHOR'));
			return;
		}		
		
		//$this->state = $this->get('State');
		$this->item  = $this->get('Item');
		$this->form  = $this->get('Form');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		$this->addToolbar();
		$this->setLayout('edit');
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		
		$checkedOut = false;
		$canDo = GooglebasexmlHelper::getActions();

		JToolBarHelper::title(JText::_('COM_GOOGLEBASEXML_TITLE_UPDATES'), 'updates.png');

		// If not checked out, can save the item.
		if (!$checkedOut && ($canDo->get('core.admin')))
		{
			JToolBarHelper::apply('updates.apply', 'JTOOLBAR_APPLY');
			//JToolBarHelper::save('updates.save', 'JTOOLBAR_SAVE');
		}



		JToolBarHelper::cancel('updates.cancel', 'JTOOLBAR_CLOSE');

	}
}
