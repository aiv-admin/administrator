<?php
/**
* @version		$Id:product.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Views
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license #http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

 
class GooglebasexmlViewVmproducts  extends JViewLegacy {


	protected $items;

	protected $pagination;

	protected $state;
	
	
	/**
	 *  Displays the list view
 	 * @param string $tpl   
     */
	public function display($tpl = null)
	{
		
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		if(version_compare(JVERSION,'3','<')){
			$tpl = "25";
		}
		

		parent::display($tpl);
	}
	

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
		 	          'b.product_name' => JText::_('Product Name'),
					  'a.virtuemart_product_id' => JText::_('Product ID'),
	     	          'c.product_category_id' => JText::_('Product Category')
	     		);
	}	
}
?>
