<?php
/**
* @version		$Id:edit.php 1 2015-03-13 13:44:59Z fcoulter $
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license 		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/
// no direct access
defined('_JEXEC') or die('Restricted access');
if(!version_compare(JVERSION,'3','<')){
}
	JHtml::_('behavior.tooltip');

	JHtml::_('behavior.formvalidation');


// Set toolbar items for the page
$edit		= JFactory::getApplication()->input->get('edit', true);
$text = !$edit ? JText::_( 'New' ) : JText::_( 'Edit' );
JToolBarHelper::title(   JText::_( 'Product' ).': <small><small>[ ' . $text.' ]</small></small>' );
JToolBarHelper::apply('product.apply');
JToolBarHelper::save('product.save');
JToolBarHelper::save2copy('product.save2copy');

if (!$edit) {
	JToolBarHelper::cancel('product.cancel');
} else {
	// for existing items the button is renamed `close`
	JToolBarHelper::cancel( 'product.cancel', 'Close' );
}
?>

<script language="javascript" type="text/javascript">


Joomla.submitbutton = function(task)
{
	if (task == 'product.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {
		Joomla.submitform(task, document.getElementById('adminForm'));
	}
}

</script>

	 	<form method="post" action="<?php echo JRoute::_('index.php?option=com_googlebasexml&layout=edit&id='.(int) $this->item->id);  ?>" id="adminForm" name="adminForm">
	 	<div class="col <?php if(version_compare(JVERSION,'3.0','lt')):  ?>width-60  <?php endif; ?>span8 form-horizontal fltlft">
		  <fieldset class="adminform">
			<legend><?php echo JText::_( 'Details' ); ?></legend>
		
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('title'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('title');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('virtuemart_product_id'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('virtuemart_product_id');  ?>
					</div>
				</div>	
 
 				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_item_group_id'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_item_group_id');  ?>
					</div>
				</div>	

                
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('description'); ?>
					</div>
				<?php if(version_compare(JVERSION,'3.0','lt')): ?>
				<div class="clr"></div>
				<?php  endif; ?>						
					
					<div class="controls">	
						<?php echo $this->form->getInput('description');  ?>
					</div>
				</div>		
                	

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_google_category'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_google_category');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_condition'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_condition');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_brand'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_brand');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_gtin'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_gtin');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_mpn'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_mpn');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_shipping_label'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_shipping_label');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_multipack'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_multipack');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_is_bundle'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_is_bundle');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_adult'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_adult');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_adwords_redirect'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_adwords_redirect');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_gender'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_gender');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_age_group'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_age_group');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_color'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_color');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_size'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_size');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_size_type'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_size_type');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_size_system'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_size_system');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_material'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_material');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_pattern'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_pattern');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_custom_label_0'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_custom_label_0');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_custom_label_1'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_custom_label_1');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_custom_label_2'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_custom_label_2');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_custom_label_3'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_custom_label_3');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_custom_label_4'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_custom_label_4');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_excluded_destination'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_excluded_destination');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_energy_efficiency_class'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_energy_efficiency_class');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('g_promotion_id'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('g_promotion_id');  ?>
					</div>
				</div>		
					
		
					
		
			
						
          </fieldset>                      
        </div>
        <div class="col <?php if(version_compare(JVERSION,'3.0','lt')):  ?>width-30  <?php endif; ?>span2 fltrgt">
		        
			<fieldset class="adminform">
				<legend><?php echo JText::_( 'Parameters' ); ?></legend>
                
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('published'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('published');  ?>
					</div>
				</div>		
		
				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('publish_down'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('publish_down');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('publish_up'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('publish_up');  ?>
					</div>
				</div>		

				<div class="control-group">
					<div class="control-label">					
						<?php echo $this->form->getLabel('language'); ?>
					</div>
					
					<div class="controls">	
						<?php echo $this->form->getInput('language');  ?>
					</div>
				</div>		
								
			</fieldset>
			        

        </div>                   
		<input type="hidden" name="option" value="com_googlebasexml" />
	    <input type="hidden" name="cid[]" value="<?php echo $this->item->id ?>" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="view" value="product" />
		<?php echo JHTML::_( 'form.token' ); ?>
	</form>