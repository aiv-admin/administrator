<?php
/**
* @version		$Id:cat.php 1 2015-03-13 13:44:59Z fcoulter $
* @package		Googlebasexml
* @subpackage 	Views
* @copyright	Copyright (C) 2015, Fiona Coulter. All rights reserved.
* @license #http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

 
class GooglebasexmlViewcats  extends JViewLegacy {


	protected $items;

	protected $pagination;

	protected $state;
	
	
	/**
	 *  Displays the list view
 	 * @param string $tpl   
     */
	public function display($tpl = null)
	{
		
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		
		$model = $this->getModel();		
		$model->addCategoryPaths($this->items);

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		GooglebasexmlHelper::addSubmenu('cats');

		$this->addToolbar();
		if(!version_compare(JVERSION,'3','<')){
			$this->sidebar = JHtmlSidebar::render();
		}
		
		if(version_compare(JVERSION,'3','<')){
			$tpl = "25";
		}
		parent::display($tpl);
	}
	
	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 */
	protected function addToolbar()
	{
		
		$canDo = GooglebasexmlHelper::getActions();
		$user = JFactory::getUser();
		JToolBarHelper::title( JText::_('GOOGLEBASEXML_CATEGORIES_TITLE'), 'generic.png' );
		if ($canDo->get('core.create')) {
			JToolBarHelper::addNew('cat.add');
		}	
		
		if (($canDo->get('core.edit')))
		{
			JToolBarHelper::editList('cat.edit');
		}
		
				
				

		if ($canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'cats.delete', 'JTOOLBAR_DELETE');
		}
				
		
		//JToolBarHelper::preferences('com_googlebasexml', '550');  
		if(!version_compare(JVERSION,'3','<')){		
			JHtmlSidebar::setAction('index.php?option=com_googlebasexml&view=cats');
		}
		
		if(!version_compare(JVERSION,'3','<')){
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_LANGUAGE'),
				'filter_language',
				JHtml::_('select.options', $this->getLanguages(true), 'value', 'text', $this->state->get('filter.language'), true)
			);
		}
		
				
					
	}	
	

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 */
	protected function getSortFields()
	{
		return array(
		 	          'a.virtuemart_category_id' => JText::_('Virtuemart category'),					 
		 	          'a.google_category' => JText::_('Google category'),
	     	          'a.id' => JText::_('JGRID_HEADING_ID'),
					  'b.category_name' => JText::_('Category Name'),
					  'c.category_parent_id'=>JText::_('Parent Category'),
					  'a.language' => JText::_('Language')
	     		);
	}	
	
   protected function getLanguages($all = false, $translate = false)
   {
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);

		// Build the query.
		$query->select('a.lang_code AS value, a.title AS text, a.title_native');
		$query->from('#__languages AS a');
		$query->where('a.published >= 0');
		$query->order('a.title');

		// Set the query and load the options.
		$db->setQuery($query);
		$items = $db->loadObjectList();
		if ($all)
		{
			array_unshift($items, new JObject(array('value' => '*', 'text' => $translate ? JText::alt('JALL', 'language') : 'JALL_LANGUAGE')));
		}
		
		foreach($items as &$item)
		{
			$item->value = str_replace("-","_",strtolower($item->value));
		}

		// Detect errors
		if ($db->getErrorNum())
		{
			JError::raiseWarning(500, $db->getErrorMsg());
		}
		
		return $items;
	}		
	
}
?>