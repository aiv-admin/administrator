<?php 
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

defined('_JEXEC') or die('Restricted access'); 

?>

	<?php if (!empty( $this->sidebar)) : ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<div id="adminFormHeader" style="width:100%">
    <div style="float:right">
       <?php echo $this->lists['state']; ?>
    </div>	
    <div style="float:right">
	  <?php echo JText::_('Filter'); ?>
	  <input type="text" name="filter_search" id="search" value="<?php echo htmlspecialchars($this->lists['search'], ENT_QUOTES); ?>" class="text_area" onchange="document.adminForm.submit();" />
	  <button onclick="this.form.submit();"><?php echo JText::_('Go'); ?></button>
	  <button onclick="document.adminForm.filter_search.value=''; this.form.submit();"><?php echo JText::_('Reset'); ?></button>
    </div>	
</div>
<br style="clear:both;" />
<div id="editcell">
	<table class="adminlist">
	<thead>
		<tr>
			<th width="5">
				<?php //echo JText::_( 'ID' ); ?>
				<?php echo JHTML::_('grid.sort', JText::_( 'ID' ), 'id', htmlspecialchars($this->lists['order_Dir'],ENT_QUOTES), htmlspecialchars($this->lists['order'],ENT_QUOTES)); ?>
			</th>
			<th width="20" align="left" style="text-align:left">
				<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
			</th>	
            <th align="left" style="text-align:left"><?php echo JHTML::_('grid.sort', JText::_( 'GXML_TITLE' ), 'title', htmlspecialchars($this->lists['order_Dir'], ENT_QUOTES), htmlspecialchars($this->lists['order'], ENT_QUOTES)); ?>								
            </th>
            <th align="left" style="text-align:left">
              <?php echo JText::_('COM_GOOGLEBASEXML_FEED_URL'); ?>
            </th>		
            <th align="left" style="text-align:left">
              <?php echo JText::_('GOOGLEBASEXML_DOWNLOAD_LINK'); ?>
            </th>		
            <th align="left" style="text-align:left">
              &nbsp;
            </th>		            
			<th align="left" style="text-align:left">
                <?php echo JHTML::_('grid.sort', JText::_( 'GXML_PUBLISHED' ), 'published', htmlspecialchars($this->lists['order_Dir'], ENT_QUOTES), htmlspecialchars($this->lists['order'], ENT_QUOTES)); ?>								
			</th>
 		</tr>
	</thead>
	<?php
	$k = 0;

	$n=count( $this->items );
	$baseurl = str_replace('/administrator','',JURI::base());	


	for ($i=0; $i < $n; $i++)	{
		$row = $this->items[$i];
		$params = json_decode($row->params);
		$paramsToken = "";
		if(isset($params->token))
		{
			$paramsToken = trim($params->token);
		}
		if(!empty($paramsToken))
		{
			$token="&token=".htmlspecialchars(rawurlencode($paramsToken));
		}
		else
		{
			$token = "";
		}
		//we need to put lang in feed url too
		
		$paramsLang = "";
		$lang = "";
		if(isset($params->product_language))
		{
			$paramsLang = trim($params->product_language);
		}
		if(!empty($paramsLang))
		{
			$lang = "&lang=".htmlspecialchars(rawurlencode($paramsLang));
		}
		
		$link = JRoute::_( 'index.php?option=com_googlebasexml&task=googlebasexml.edit&cid[]='. $row->id );
		$feedLink = $baseurl.'index.php?option=com_googlebasexml&format=xml&id='.$row->id.$token.$lang;
		$downloadLink = $baseurl.'index.php?option=com_googlebasexml&format=xml&output=download&id='.$row->id.$token.$lang;
		$logLink = JRoute::_("index.php?option=com_googlebasexml&view=googlebasexml&format=log&id=". $row->id) ;
		
		//$row->published = $row->state;
		$publish = JHTML::_('grid.published', $row, $i,'tick.png', 'publish_x.png', $prefix = 'googlebasexml.' );
		?>
		<tr class="<?php echo "row$k"; ?>" align="center">
			<td>
				<?php echo $row->id; ?>
			</td>
            <td>
				<?php echo JHtml::_('grid.id', $i, $row->id); ?>            
            </td>
			<td>
				<a href="<?php echo $link; ?>"><?php echo $this->escape($row->title); ?></a>
			</td>
            <td>
                <a href="<?php echo $feedLink; ?>" target="_blank"><?php echo $feedLink; ?></a>
            </td>
            <td>
                <a href="<?php echo $downloadLink; ?>" target="_blank"><?php echo JText::_('GOOGLEBASEXML_DOWNLOAD'); ?></a>
            </td>
            <td>
                <a href="<?php echo $logLink; ?>" target="_blank"><?php echo JText::_('GOOGLEBASEXML_VIEW_LOG'); ?></a>
            </td>            
			<td>
				<?php echo $publish; ?>			
			</td>
		</tr>
		<?php
		$k = 1 - $k;
	}
	?>
    <tfoot>
      <tr>
       <th colspan="10" align="center">
	       <?php echo $this->page->getListFooter(); ?>
       </th>
      </tr>
    </tfoot>
	</table>
</div>

<input type="hidden" name="option" id="option" value="com_googlebasexml" />
<input type="hidden" name="task" id="task" value="" />
<input type="hidden" name="boxchecked" id="boxchecked" value="0" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</div>



