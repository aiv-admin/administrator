<?php
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.application.component.view' );
//jimport('joomla.html.toolbar');

/**
 * Googlebasexmls View
 *
 * @package    Joomla.Tutorials
 * @subpackage Components
 */
class GooglebasexmlViewGooglebasexmls extends JViewLegacy
{
	/**
	 * Googlebasexmls view display method
	 * @return void
	 **/
	function display($tpl = null)
	{
		
		$app = JFactory::getApplication();
		$option = 'com_googlebasexml';
		$canDo = GooglebasexmlHelper::getActions();
		
		
		JToolBarHelper::title(   JText::_('GOOGLEBASEXML_FEEDS_TITLE'), 'generic.png' );
		if ($canDo->get('core.admin')) {
			JToolBarHelper::preferences('com_googlebasexml', 400, 570);
		}
		if ($canDo->get('core.create')) {		
			JToolBarHelper::addNew('googlebasexml.add');
		}
		
		if ($canDo->get('core.edit'))
		{		
			JToolBarHelper::editList('googlebasexml.edit');
		}
		
		if ($canDo->get('core.edit.state'))
		{

			JToolBarHelper::publishList('googlebasexml.publish');
			JToolBarHelper::unpublishList('googlebasexml.unpublish');
		}
		
		if ($canDo->get('core.delete'))
		{
			JToolBarHelper::deleteList('','googlebasexml.remove');
		}
		
		$lists = new stdClass();	
	
		
		
		// Get data from the model
		$items		= $this->get( 'Data');
		$page = $this->get('Pagination');
		
		$lists = array();
		
		/*ordering*/
		$lists['order'] = $app->getUserStateFromRequest($option.'.filter_order', 'filter_order', 'id','','cmd');
		$lists['order_Dir'] = $app->getUserStateFromRequest($option.'.filter_order_Dir', 'filter_order_dir', 'ASC','','cmd');
		
		/*state*/
		$filter_state = $app->getUserStateFromRequest( $option.'.filter_state','filter_state','','cmd');
		$lists['state'] = JHTML::_('grid.state', $filter_state);
		/*search*/
		$filter_search = $app->getUserStateFromRequest( $option.'.filter_search','filter_search','','string');
		$lists['search'] = $filter_search;
		
		GooglebasexmlHelper::addSubmenu('googlebasexmls');

		if(!version_compare(JVERSION,'3','<')){
			$this->sidebar = JHtmlSidebar::render();
		}

		

		$this->items = $items;
		$this->page = $page;
		$this->lists = $lists;
		


		parent::display($tpl);
	}
}