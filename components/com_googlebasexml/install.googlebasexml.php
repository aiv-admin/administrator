<?php
/*
copyright 2009 Fiona Coulter http://www.iswebdesign.co.uk

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


// no direct access
defined('_JEXEC') or die('Restricted access');

class Com_GooglebasexmlInstallerScript{
	
	
	public function preflight($type, $parent){
		
	}
	 
  public function postflight($type, $parent){
	  
  }
  
  public function update($parent)
  {
		  $this->alterTable("#__googlebasexml_products", array("g_item_group_id" => "`g_item_group_id` varchar(100) NOT NULL DEFAULT '' AFTER `description`"));  
	  
  }
  
	/**
	 * Runs on uninstallation
	 *
	 * @param JInstaller $parent
	 */
	  public function uninstall($parent){
		  
	  }
	

		private function alterTable($tablename,$fields,$command='ADD'){

			$ok = false;
			$db = JFactory::getDBO();

			$query = 'SHOW COLUMNS FROM `'.$tablename.'` ';
			$db->setQuery($query);
			$columns = $db->loadColumn(0);

			foreach($fields as $fieldname => $alterCommand){
			   if($command == "ADD")
			   {
				  if(!in_array($fieldname,$columns)){
					  $query = 'ALTER TABLE `'.$tablename.'` '.$command.' COLUMN '.$alterCommand;
  
					  $db->setQuery($query);
					  if(!$db->execute()){
						  $app = JFactory::getApplication();
						  $app->enqueueMessage('Error: Install alterTable '.$db->getErrorMsg() );
						  $ok = false;
					  }
				  }
			   }
			   else
			   {
				  if(in_array($fieldname,$columns)){
					  $query = 'ALTER TABLE `'.$tablename.'` '.$command.' COLUMN '.$alterCommand;
  
					  $db->setQuery($query);
					  if(!$db->execute()){
						  $app = JFactory::getApplication();
						  $app->enqueueMessage('Error: Install alterTable '.$db->getErrorMsg() );
						  $ok = false;
					  }
				  }
				   
			   }
			}

			return $ok;
		}

}

?>


