<?php
/**
 * @version		$Id: #component#.php 170 2013-11-12 22:44:37Z michel $
 * @package		Joomla.Framework
 * @subpackage		HTML
 * @copyright	Copyright (C) 2005 - 2009 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

class GooglebasexmlHelper
{
	
	/*
	 * Submenu for Joomla 3.x
	 */
	public static function addSubmenu($vName = 'cats')
	{
        	if(version_compare(JVERSION,'3','<')){
		JSubMenuHelper::addEntry(
			JText::_('COM_GOOGLEBASEXML_FEEDS'),
			'index.php?option=com_googlebasexml',
			($vName == 'googlebasexmls')
		);	
	} else {
		JHtmlSidebar::addEntry(
			JText::_('COM_GOOGLEBASEXML_FEEDS'),
			'index.php?option=com_googlebasexml',
			($vName == 'googlebasexmls')
		);	
	}
		
        	if(version_compare(JVERSION,'3','<')){
		JSubMenuHelper::addEntry(
			JText::_('COM_GOOGLEBASEXML_CATEGORY_MAP'),
			'index.php?option=com_googlebasexml&view=cats',
			($vName == 'cats')
		);	
	} else {
		JHtmlSidebar::addEntry(
			JText::_('COM_GOOGLEBASEXML_CATEGORY_MAP'),
			'index.php?option=com_googlebasexml&view=cats',
			($vName == 'cats')
		);	
	}
	if(version_compare(JVERSION,'3','<')){
		JSubMenuHelper::addEntry(
			JText::_('COM_GOOGLEBASEXML_PRODUCT_OVERRIDES'),
			'index.php?option=com_googlebasexml&view=products',
			($vName == 'products')
		);	
	} else {
		JHtmlSidebar::addEntry(
			JText::_('COM_GOOGLEBASEXML_PRODUCT_OVERRIDES'),
			'index.php?option=com_googlebasexml&view=products',
			($vName == 'products')
		);	
	}
	
	if(version_compare(JVERSION,'3','<')){
		JSubMenuHelper::addEntry(
			JText::_('COM_GOOGLEBASEXML_UPDATES'),
			'index.php?option=com_googlebasexml&view=updates',
			($vName == 'updates')
		);	
	} else {
		JHtmlSidebar::addEntry(
			JText::_('COM_GOOGLEBASEXML_UPDATES'),
			'index.php?option=com_googlebasexml&view=updates',
			($vName == 'updates')
		);	
	}
	

  }
	
	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 *
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;
	
		if (empty($categoryId))
		{
			$assetName = 'com_googlebasexml';
			$level = 'component';
		}
		else
		{
			$assetName = 'com_googlebasexml.category.'.(int) $categoryId;
			$level = 'category';
		}
	
		$actions = JAccess::getActions('com_googlebasexml', $level);
	
		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, $assetName));
		}
	
		return $result;
	}	
	/**
	 * 
	 * Get the Extensions for Categories
	 */
	public static function getExtensions() 
	{
						
		static $extensions;
		
		if(!empty($extensions )) return $extensions;
		
		jimport('joomla.utilities.xmlelement');
		
		$xml = simplexml_load_file(JPATH_ADMINISTRATOR.'/components/com_googlebasexml/elements/extensions.xml', 'JXMLElement');		        
		$elements = $xml->xpath('extensions');
		$extensions = $xml->extensions->xpath('descendant-or-self::extension');
		
		return $extensions;
	} 
	
	public static function getVMLang()
	{
		static $vmlang;
		
		if(!empty($vmlang)) return $vmlang;
		$lang = JFactory::getLanguage();
		//$lang_code = $lang->get('lang_code','en-GB');
		$lang_code = $lang->getTag();

		//$vmlang = strtolower(strtr($lang->lang_code,'-','_'));
		if (!class_exists( 'VmConfig' )) {
			require(JPATH_ADMINISTRATOR . '/components/com_virtuemart/helpers/config.php');
		}
 	    $vmconfig = VmConfig::loadConfig(true,false);
		$active_langs = self::getActiveLangs();
		if(in_array($lang_code,$active_langs))
		{
			$vmlang = strtolower(strtr($lang_code,'-','_'));
		}
		else
		{
			//$vmlang = $active_langs[0];
			$vmlang = strtolower(strtr($active_langs[0],'-','_'));
			
		}
		
		return $vmlang;		
	}

    public static function getActiveLangs()
	{
		static $active_langs;
		if(isset($active_langs))
		{
			return $active_langs;
		}
		
		if (!class_exists( 'VmConfig' )) {
			require(JPATH_ADMINISTRATOR . '/components/com_virtuemart/helpers/config.php');
		}
 	    $vmconfig = VmConfig::loadConfig(true,false);
		$active_langs = (array)$vmconfig->get('active_languages',array('en-GB'));
		return $active_langs;
		
	}
	
	/**
	 *
	 * Returns views that associated with categories
	 */
	public static function getCategoryViews()
	{
	
		$extensions = self::getExtensions();
		$views = array();
		foreach($extensions as $extension ) {
			$views[$extension->listview->__toString()] = 'com_googlebasexml.'.$extension->name->__toString();
		}
		return $views;
	}	
}

/**
 * Utility class for categories
 *
 * @static
 * @package 	Joomla.Framework
 * @subpackage	HTML
 * @since		1.5
 */
abstract class JHtmlGooglebasexml
{
	/**
	 * @var	array	Cached array of the category items.
	 */
	protected static $items = array();
	
	/**
	 * Returns the options for extensions list
	 * 
	 * @param string $ext - the extension
	 */
	public static function extensions($ext) 
	{
		$extensions = GooglebasexmlHelper::getExtensions();
		$options = array();
		
		foreach ($extensions as $extension) {   
		
			$option = new stdClass();
			$option->text = JText::_(ucfirst($extension->name));
			$option->value = 'com_googlebasexml.'.$extension->name;
			$options[] = $option;			
		}		
		return JHtml::_('select.options', $options, 'value', 'text', $ext, true);
	}
	
	/**
	 * Returns an array of categories for the given extension.
	 *
	 * @param	string	The extension option.
	 * @param	array	An array of configuration options. By default, only published and unpulbished categories are returned.
	 *
	 * @return	array
	 */
	public static function categories($extension, $cat_id,$name="categories",$title="Select Category", $config = array('attributes'=>'class="inputbox"','filter.published' => array(0,1)))
	{

			$config	= (array) $config;
			$db		= JFactory::getDbo();

			$query = $db->getQuery(true);

			$query->select('a.id, a.title, a.level');
			$query->from('#__googlebasexml_categories AS a');
			$query->where('a.parent_id > 0');

			// Filter on extension.
			if($extension)
			    $query->where('extension = '.$db->quote($extension));
			
			$attributes = "";
			
			if (isset($config['attributes'])) {
				$attributes = $config['attributes'];
			}
			
			// Filter on the published state
			if (isset($config['filter.published'])) {
				
				if (is_numeric($config['filter.published'])) {
					
					$query->where('a.published = '.(int) $config['filter.published']);
					
				} else if (is_array($config['filter.published'])) {
					
					JArrayHelper::toInteger($config['filter.published']);
					$query->where('a.published IN ('.implode(',', $config['filter.published']).')');
					
				}
			}

			$query->order('a.lft');

			$db->setQuery($query);
			$items = $db->loadObjectList();
			
			// Assemble the list options.
			self::$items = array();
			self::$items[] = JHtml::_('select.option', '', JText::_($title));
			foreach ($items as &$item) {
								
				$item->title = str_repeat('- ', $item->level - 1).$item->title;
				self::$items[] = JHtml::_('select.option', $item->id, $item->title);
			}

		return  JHtml::_('select.genericlist', self::$items, $name, $attributes, 'value', 'text', $cat_id, $name);
		//return self::$items;
	}
	
}