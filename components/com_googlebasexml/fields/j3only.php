<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfieldtext');

class JFormFieldJ3only extends JFormFieldText {
	
	protected $type = 'j3only';
	
	protected function getInput(){
		
			$version = new JVersion();
			if($version->RELEASE == '2.5'){
				$html = JText::_('COM_GOOGLEBASEXML_NOT_SUPPORTED_25');
				return $html;
				
			}
			
			return parent::getInput();
			
	}
	
	
}