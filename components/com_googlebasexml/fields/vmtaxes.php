<?php
defined('JPATH_PLATFORM') or die;
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */


defined('DS') or define('DS', DIRECTORY_SEPARATOR);
if (!class_exists( 'VmConfig' )) require(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_virtuemart'.DS.'helpers'.DS.'config.php');

if (!class_exists('ShopFunctions'))
	require(VMPATH_ADMIN . DS . 'helpers' . DS . 'shopfunctions.php');


class JFormFieldVmTaxes extends JFormField {

	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	var $type = 'vmtaxes';

	protected function getInput() {

	VmConfig::loadConfig(true,false);
	
	$option = "com_virtuemart";
	$lang = JFactory::getLanguage();
	$lang->load($option, JPATH_BASE, null, false, false)
	||	$lang->load($option, JPATH_BASE . "/components/$option", null, false, false)
	||	$lang->load($option, JPATH_BASE, $lang->getDefault(), false, false)
	||	$lang->load($option, JPATH_BASE . "/components/$option", $lang->getDefault(), false, false);
	
	return ShopFunctions::renderTaxList($this->value, $this->name, '');

	}

}