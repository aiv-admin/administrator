<?php
/**
 * @version		1.0.0 amazonshoppingcart $
 * @package		amazonshoppingcart
 * @copyright	Copyright � Fiona Coulter http://www.spiralscripts.co.uk 2011 - All rights reserved.
 * @license		GNU/GPL
 * @author		Fiona Coulter
 * @author mail	joomla - at - iswebdesign.co.uk
 * @website		http://www.spiralscripts.co.uk
 *
 **/

// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

/**
 */
class TableGooglebasexml extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $id = null;
	
	/**
	 * @var int
	 */
	var $ordering = null;

	/**
	 * @var string
	 */
	var $title = null;
	
		/**
	 * @var string
	 */
	var $alias = null;
		
	
	/**
	 * @var int
	 */
	var $published = null;			 
	
	/**
	* @var datetime
	*/
	var $date = null;
	
	
	/**
	* @var datetime
	*/
	var $publish_up = null;	
	
	
	/**
	* @var datetime
	*/
	var $publish_down = null;	
	
	/**
	 * @var string
	 */
    var $params = null;	
 



	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 */
	function TableGooglebasexml(& $db) {
		parent::__construct('#__googlebasexml', 'id', $db);
	}
	
	
	/**
	* validation
	*
	* @return boolean true if buffer is valid
	*/
	function check()
	{
	  return true;
	}
	
}
?>