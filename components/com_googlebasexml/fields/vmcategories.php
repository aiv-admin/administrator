<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
defined('JPATH_PLATFORM') or die;

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();
	

if (!class_exists('VmElementVmCategories'))
    require(JPATH_VM_ADMINISTRATOR . '/elements/vmcategories.php');
