<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
defined('JPATH_PLATFORM') or die;

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();

if (!class_exists('ShopFunctions'))
    require(JPATH_VM_ADMINISTRATOR . '/helpers/shopfunctions.php');
if (!class_exists('TableCategories'))
    require(JPATH_VM_ADMINISTRATOR . '/tables/categories.php');

jimport('joomla.form.formfield');
	
class JFormFieldVMultipleCategories extends JFormField {

    var $type = 'vmultiplecategories';
	
    function getInput() {
        //$key = ($this->element['key_field'] ? $this->element['key_field'] : 'value');
        //$val = ($this->element['value_field'] ? $this->element['value_field'] : $this->name);
        //JPlugin::loadLanguage('com_virtuemart', JPATH_ADMINISTRATOR);
		$value =  (array)$this->value;
        $categorylist = ShopFunctions::categoryListTree($value);
        $class = '';
        $html = '<select class="inputbox"  multiple="multiple" name="' . $this->name . '" size="10">';
        $html .= '<option value="0">' . JText::_('COM_GOOGLEBASEXML_CATEGORY_FORM_TOP_LEVEL') . '</option>';
        $html .= $categorylist;
        $html .="</select>";
        return $html;
    }


}
	
	
	
	
