<?php
/**
 * @license     GNU General Public License version 2 or later; see LICENSE
 */


defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');


class JFormFieldVMLanguages extends JFormFieldList
{

	protected $type = 'vmlanguages';

	/**
	 * Method to get the field options for virtuemart languages.
	 *
	 * @return  array  The field option objects.
	 *
	 */
	protected function getOptions()
	{
		// Merge any additional options in the XML definition.
		return array_merge(parent::getOptions(), $this->getLanguages(false));
	}
	
	
    protected function getLanguages($all = false, $translate = false)
	{
		if (empty(self::$items))
		{
			// Get the database object and a new query object.
			$db		= JFactory::getDBO();
			$query	= $db->getQuery(true);

			// Build the query.
			$query->select('a.lang_code AS value, a.title AS text, a.title_native');
			$query->from('#__languages AS a');
			$query->where('a.published >= 0');
			$query->order('a.title');

			// Set the query and load the options.
			$db->setQuery($query);
			$items = $db->loadObjectList();
			if ($all)
			{
				array_unshift($items, new JObject(array('value' => '*', 'text' => $translate ? JText::alt('JALL', 'language') : 'JALL_LANGUAGE')));
			}
			
			array_unshift($items, new JObject(array('value' => '', 'text' => JText::_('JOPTION_SELECT_LANGUAGE'))));
			
			
			foreach($items as &$item)
			{
				$item->value = str_replace("-","_",strtolower($item->value));
			}

			// Detect errors
			if ($db->getErrorNum())
			{
				JError::raiseWarning(500, $db->getErrorMsg());
			}
		}
		return $items;
	}	
}
