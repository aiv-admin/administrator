<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

if (!class_exists('VmConfig'))
   { require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');}
VmConfig::loadConfig();


class JFormFieldAvailability extends JFormField {
	
	protected $type = 'availability';
	
	protected function getInput(){
		
		    if(! file_exists(VMPATH_SITE.'/helpers/vmtemplate.php'))
			{
				//this is older version of VM
				$html = '<input type="text" name="'.$this->name.'" value="'.$this->value'"/>';
				return $html;
				
			}
			
			if(!class_exists('VmTemplate')) require(VMPATH_SITE.'/helpers/vmtemplate.php');
			$vmtemplate = VmTemplate::loadVmTemplateStyle();
			if(!class_exists('shopFunctions')) require(VMPATH_ADMIN.'/helpers/vmtemplate.php');
			
			$imagePath = shopFunctions::getAvailabilityIconUrl($vmtemplate);
		    $unique = uniqid();
		
			$html = '<span><input type="text" class="inputbox box'.$unique.'" id="product_availability" name="'.$this->name.'" value="'. $this->value.'" />';
			$html .= '<span class="icon-nofloat vmicon vmicon-16-info tooltip" title="'. JText::_('COM_GOOGLEBASEXML_AVAILABILITY').'"><br/ ></span>';
			
			//$function = "function(){}";
			$function = 'var newimage=jQuery(this).val();jQuery(this).parent().find(\'.box'.$unique.'\').val(newimage);';

			$html .= '<br style="clear:both" />'.JHtml::_('list.images', 'image'.$unique, $this->value, "onchange=\"".$function."\"", $imagePath).'</span>';
			//$html .= '<br style="clear:both" />'.JHtml::_('list.images', 'image'.$unique, $this->value, "", $imagePath);
			
            
		   /* $html .= '<script type="text/javascript">
			jQuery(document).ready(function(){
              jQuery(\'#image'.$unique.'\').change( function() {
                  var $newimage = jQuery(this).val();
                  jQuery(\'.box'.$unique.'\').val($newimage);
				  window.alert($newimage);
                  });
			});
          </script>';*/
		  
		  
		  
		  return $html;
            
		
	}
	
}
