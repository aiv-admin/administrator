<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

class JFormFieldSubheading extends JFormField {
	
	protected $type = 'subheading';
	
	protected function getInput(){
		JHtml::_('behavior.tooltip');
        $output = JHtml::tooltip(JText::_($this->description), '', '', JText::_($this->value));
		// Output
		return '
		<div style="font-weight:bold;font-size:12px;color:#333;padding:4px;margin:0; float:left; width:90%; text-align:left;">
			'.$output. '
		</div><br style="clear:both;" />
		';	}
	
}
