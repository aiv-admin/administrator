<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

class JFormFieldLinebreak extends JFormField {
	
	protected $type = 'linebreak';
	
	protected function getInput(){
		return '<br style="clear:both;" />';
	}
	
}
