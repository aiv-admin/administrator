<?php


defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');
if (!class_exists('VmConfig'))
{
	require(JPATH_ROOT . DS . 'administrator' . DS . 'components' . DS . 'com_virtuemart' . DS . 'helpers' . DS . 'config.php');
}
VmConfig::loadConfig();


if (!class_exists( 'VirtueMartModelManufacturer' ))
JLoader::import( 'manufacturer', JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_virtuemart' . DS . 'models' );

/**
 * Supports a modal product picker.
 *
 *
 */
class JFormFieldMultipleManufacturers extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @author      Valerie Cartan Isaksen
	 * @var		string
	 *
	 */
	protected $type = 'multiplemanufacturers';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */


	function getInput() {

		$model = VmModel::getModel('Manufacturer');
		$value = (array)$this->value;
		$manufacturers = $model->getManufacturers(true, true, false);
		$default = new stdClass();
		$default->virtuemart_manufacturer_id = 0;
		$default->mf_name = JText::_("-- All Manufacturers --");
		array_unshift($manufacturers, $default);
		return JHTML::_('select.genericlist', $manufacturers, $this->name, 'class="inputbox"  multiple="multiple" size="10"', 'virtuemart_manufacturer_id', 'mf_name', $value, $this->id);
	}


}