<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

class JFormFieldGavail extends JFormFieldList {
	
	protected $type = 'gavail';
	
	
	protected function getOptions(){
		
		$options = array();
		
		
		$version = new JVersion();
		if($version->RELEASE == '2.5'){
		  $options[] = JHTML::_('select.option', JText::_('COM_GOOGLEBASEXML_NOT_SUPPORTED_25'), JText::_('COM_GOOGLEBASEXML_NOT_SUPPORTED_25'));
			
		}
		else
		{		
		  $options[] = JHTML::_('select.option', JText::_('COM_GOOGELBASEXML_AVAIL_INSTOCK'), JText::_('COM_GOOGELBASEXML_AVAIL_INSTOCK'));
		  $options[] = JHTML::_('select.option', JText::_('COM_GOOGELBASEXML_AVAIL_OUTOFSTOCK'), JText::_('COM_GOOGELBASEXML_AVAIL_OUTOFSTOCK'));
		  $options[] = JHTML::_('select.option', JText::_('COM_GOOGELBASEXML_AVAIL_PREORDER'), JText::_('COM_GOOGELBASEXML_AVAIL_PREORDER'));
		}
		return array_merge(parent::getOptions(), $options);		
		
	}
	
}
