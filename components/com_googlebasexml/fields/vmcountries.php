<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();


jimport('joomla.form.formfield');

class JFormFieldVMCountries extends JFormField
{
	protected $type = 'vmcountries';
	
	function getInput()
	{
        $db =  JFactory::getDBO();
		$query	= $db->getQuery(true);
        $query->select('country_2_code AS value, country_name AS text');
		$query->from('#__virtuemart_countries');
        $query->where('published = 1');
		$query->order('country_name ASC');

        $db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$mitem = new stdClass();
		$mitem->value = '';
		$mitem->text = '--use default country--';
		
		array_unshift($mitems, $mitem);
	
		$output = '';
		if( ! $mitems || (count($mitems) <= 1))
		{
			$output = '';
		}
		else
		{
		  $output= JHTML::_('select.genericlist',  $mitems, $this->name, 'class="inputbox" ', 'value', 'text', $this->value );
		}
		
		return $output;		
		
	}
	
}
	
