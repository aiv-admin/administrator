<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');


// Category element
class JFormFieldSiteurl extends JFormField {

	protected $type = 'siteurl';
	
	protected function getInput(){
        
		$returnValue = '';
		$baseurl = JURI::root();
		if ($this->value == '')
		{
		  $returnValue = str_replace('/administrator','',$baseurl);	
		}
		else
		{
		  $returnValue = $this->value;	
		}
		// Output
		return '<input class="inputbox" type="text" name="'.$this->name.'" id="'.$this->id.'" value="'.$returnValue.'" size="70"/>';
	}
	
} 
