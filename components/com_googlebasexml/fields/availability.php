<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

if (!class_exists('VmConfig'))
   { require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');}
VmConfig::loadConfig();


class JFormFieldAvailability extends JFormField {
	
	protected $type = 'availability';
	
	protected function getInput(){
		
			$version = new JVersion();
			if($version->RELEASE == '2.5'){
				$html = JText::_('COM_GOOGLEBASEXML_NOT_SUPPORTED_25');
				return $html;
				
			}
		
		    if(!defined("VMPATH_SITE"))
			{
				define("VMPATH_SITE", JPATH_SITE .'/components/com_virtuemart');
			}
		
		    if(! file_exists(VMPATH_SITE.'/helpers/vmtemplate.php'))
			{
			    //$html = '<input type="text" class="inputbox" id="product_availability" name="'.$this->name.'" value="'. $this->value.'" />';
				$html = "";				
				return $html;
				
			}
			
			if(!class_exists('VmTemplate')) require(VMPATH_SITE.'/helpers/vmtemplate.php');
			$vmtemplate = VmTemplate::loadVmTemplateStyle();
			if(!class_exists('shopFunctions')) require(VMPATH_ADMIN.'/helpers/vmtemplate.php');
			
			$imagePath = shopFunctions::getAvailabilityIconUrl($vmtemplate);
		    $unique = uniqid();
		
			$html = '<span><input type="text" class="inputbox box'.$unique.'" id="product_availability" name="'.$this->name.'" value="'. $this->value.'" />';
			$html .= '<span class="icon-nofloat vmicon vmicon-16-info tooltip" title="'. JText::_('COM_GOOGLEBASEXML_AVAILABILITY').'"><br/ ></span>';
			
			//$function = "function(){}";
			$function = 'var newimage=jQuery(this).val();jQuery(this).parent().find(\'.box'.$unique.'\').val(newimage);';

			$html .= '<br style="clear:both" />'.JHtml::_('list.images', 'image'.$unique, $this->value, "onchange=\"".$function."\"", $imagePath).'</span>';
		    return $html;
			
            
		
	}
	
}
