<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();

class JFormFieldVMCurrencies extends JFormField
{

	protected $type = 'vmcurrencies';

	function getInput(){
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$publishedOnly = true;
		
    	$query->select('virtuemart_currency_id as value, currency_name as text');
		$query->from('#__virtuemart_currencies');
		if ($publishedOnly) {$query->where('published=1');}
		$query->order('currency_name');

        $db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$mitem = new stdClass();
		$mitem->value = '0';
		$mitem->text = '--use default shop currency--';
		
		array_unshift($mitems, $mitem);


	
		$output = '';
		if( ! $mitems || (count($mitems) <= 1))
		{
			$output = '';
		}
		else
		{
		  $output= JHTML::_('select.genericlist',  $mitems, $this->name, 'class="inputbox" ', 'value', 'text', $this->value );
		}
		
		return $output;
	}
}
