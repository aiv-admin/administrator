<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();

class JFormFieldVMTypefield extends JFormField
{

	protected $type = 'vmtypefield';

	function getInput(){
		
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(true);
		$query->select(array('virtuemart_custom_id as value','custom_title as text'));
		$query->from('#__virtuemart_customs');
		$query->where('field_type <> "R" AND field_type <> "Z" AND field_type <> "P"');

        $db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$mitem = new stdClass();
		$mitem->value = '';
		$mitem->text = '--select value--';
		
		array_unshift($mitems, $mitem);

        $multiple = ($this->element['multiple'] == "multiple")? 'multiple="multiple" size="10"':'';
	
		$output = '';
		if( ! $mitems || (count($mitems) <= 1))
		{
			$output = '';
		}
		else
		{
		  $output= JHTML::_('select.genericlist',  $mitems, $this->name, 'class="inputbox" '.$multiple, 'value', 'text', $this->value );
		}
		
		return $output;
	}
}
