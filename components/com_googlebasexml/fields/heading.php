<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.formfield');

class JFormFieldHeading extends JFormField {
	
	protected $type = 'heading';
	
	protected function getInput(){
		JHtml::_('behavior.tooltip');
        $output = JHtml::tooltip(JText::_($this->description), '', '', JText::_($this->value));
		// Output
		return '
		<div style="font-weight:bold;font-size:12px;color:#333;padding:4px;margin:0;background-color:#cccccc; float:left; width:90%; text-align:center;">
			'.$output. '
		</div><br style="clear:both;" />
		';
	}
	
}
