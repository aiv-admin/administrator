<?php
/**
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

defined('JPATH_PLATFORM') or die;

if (!class_exists('VmConfig'))
    require(JPATH_ROOT . '/administrator/components/com_virtuemart/helpers/config.php');
VmConfig::loadConfig();


jimport('joomla.form.formfield');

class JFormFieldVMShipping extends JFormField
{
	protected $type = 'vmshipping';
	
	
	protected function getInput(){
		
		$db = JFactory::getDBO();
		$query	= $db->getQuery(true);
		$concat = $query->concatenate(array('b.shipment_name','\' - \'','a.virtuemart_shipmentmethod_id'));
		$query->select('a.virtuemart_shipmentmethod_id as value, '.$concat.' as text');
		$query->from('#__virtuemart_shipmentmethods as a');
		$query->leftJoin('#__virtuemart_shipmentmethods_'.VMLANG.' as b ON a.virtuemart_shipmentmethod_id=b.virtuemart_shipmentmethod_id');
		$query->where('a.shipment_element=\'weight_countries\' AND published=1');
		$query->order('a.virtuemart_shipmentmethod_id');
		
        $db->setQuery( $query );
		$mitems = $db->loadObjectList();
		
		$mitem = new stdClass();
		$mitem->value = '0';
		$mitem->text = '--include all weight-countries shipping methods--';
		
		$value =  (array)$this->value;
		
		array_unshift($mitems, $mitem);
	
		$output = '';
		if( ! $mitems || (count($mitems) <= 1))
		{
			$output = '';
		}
		else
		{
		  $output= JHTML::_('select.genericlist',  $mitems, $this->name."[]", 'class="inputbox" multiple="multiple" size=10', 'value', 'text', $this->value );
		}
		
		return $output;		
		
	}
	
}