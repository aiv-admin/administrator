<?php
/**
 * @package     Responsive Scroll Triggered Box
 * @subpackage  com_rstbox
 *
 * @copyright   Copyright (C) 2014 Tassos Marinos - http://www.tassos.gr
 * @license     GNU General Public License version 2 or later; see http://www.tassos.gr/license
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import the Joomla modellist library
jimport('joomla.application.component.modellist');

class RstboxModelItems extends JModelList
{
    /**
     * Method to build an SQL query to load the list data.
     *
     * @return      string  An SQL query
     */
    protected function getListQuery()
    {
        // Create a new query object.           
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);
        // Select some fields from the item table
        $query
            ->select('b.*')
            ->from('#__rstbox b');
        
        // Filter State
        $filter = $this->getState('filter.state');
        if (is_numeric($filter))
        {
            $query->where('b.published = ' . ( int ) $filter);
        }
        else if ($filter === '')
        {
            $query->where('( b.published IN ( 0,1,2 ) )');
        }

        // Filter Box Type
        $filter = $this->getState('filter.boxtype');
        if ($filter != '')
        {
            $query->where('b.boxtype = ' . $db->q($filter));
        }

        // Filter the list over the search string if set.
        $search = $this->getState('filter.search');
        if (!empty($search))
        {
            if (stripos($search, 'id:') === 0)
            {
                $query->where('b.id = ' . ( int ) substr($search, 3));
            }
            else
            {
                $search = $db->quote('%' . $db->escape($search, true) . '%');
                $query->where(
                    '( `name` LIKE ' . $search . ' )'
                );
            }
        }

        // Filter Assigned User Groups
        $filter = $this->getState('filter.usergroups');
        if ($filter != '')
        {
            $query->where('b.params LIKE ' . $db->q('%"%usergroups%":["%' . $filter . '%"]%'));
        }  

        // Filter Assigned Devices
        $filter = $this->getState('filter.devices');
        if ($filter != '')
        {
            $query->where('b.params LIKE ' . $db->q('%"%devices%":["%' . $filter . '%"]%'));
        }  

        // Add the list ordering clause.
        $ordering = $this->getState('list.ordering', 'b.id');
        $query->order($db->escape($ordering) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));

        return $query;
    }

    public function getItems()
    {
        $items = parent::getItems();

        foreach ($items as $item) {

            $item->params = json_decode($item->params);

            // Prepare Cookie Type
            $cookieType = (isset($item->params->cookietype)) ? $item->params->cookietype : "days";
            $item->params->cookietype = $cookieType;

            // Prepare usergroups
            $usergroups = array();
            if ((isset($item->params->assign_usergroups)) && ($item->params->assign_usergroups)) {
                $usergroups = implode(",",$item->params->assign_usergroups_list);
                $usergroupsNames = RSTBoxHelper::fetch("usergroups", "*", "id in ($usergroups)");
                $item->params->assign_usergroupsNames = $usergroupsNames;
            }
        }

        return $items;
    }

    /**
     * Copy Method
     * Copy all items specified by array cid
     * and set Redirection to the list of items
     */
    function copy($ids, $model)
    {
        foreach ($ids as $id)
        {
            $model->copy($id);
        }

        $msg = JText::sprintf('Items copied', count($ids));
        JFactory::getApplication()->redirect('index.php?option=com_rstbox&view=items', $msg);
    }

}