<?php
/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */

defined('JPATH_PLATFORM') or die;

jimport('joomla.form.helper');

class JFormFieldAssignmentSelection extends JFormField
{
	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	public $type = 'AssignmentSelection';

	protected function getLabel()
	{
		return '';
	}

	protected function getInput()
	{	

		$assetsDir = JURI::root(true)."/administrator/components/com_rstbox/assets/";
		$doc = JFactory::getDocument();
        $doc->addScript($assetsDir.'js/assignmentselection.js');
        $doc->addStyleSheet($assetsDir.'css/assignmentselection.css');

		$label = JText::_($this->element["label"]);
		$this->value = (int) $this->value;
		$html = array();

		// Start the radio field output.

		$html[] = '<div class="assignmentselection">';

		$html[] = '<div class="control-group">';

		$html[] = '<div class="control-label"><label><strong>' . $label . '</strong></label></div>';

		$html[] = '<div class="controls">';
		$html[] = '<fieldset id="' . $this->id . '"  class="radio btn-group">';

		$html[] = '<input type="radio" id="' . $this->id . '0" name="' . $this->name . '" value="0"' . ((!$this->value) ? ' checked="checked"' : '').'/>';
		$html[] = '<label class="btn_ignore" for="' . $this->id . '0">' . JText::_('COM_RSTBOX_IGNORE') . '</label>';

		$html[] = '<input type="radio" id="' . $this->id . '1" name="' . $this->name . '" value="1"' . (($this->value === 1) ? ' checked="checked"' : '').'/>';
		$html[] = '<label class="btn_include" for="' . $this->id . '1">' . JText::_('COM_RSTBOX_INCLUDE') . '</label>';

		$html[] = '<input type="radio" id="' . $this->id . '2" name="' . $this->name . '" value="2"' . (($this->value === 2) ? ' checked="checked"' : '').'/>';
		$html[] = '<label class="btn_exclude" for="' . $this->id . '2">' . JText::_('COM_RSTBOX_EXCLUDE') . '</label>';

		$html[] = '</fieldset>';
		$html[] = '</div></div></div>';


		return implode($html);
	}
}
