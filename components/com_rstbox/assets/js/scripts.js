/**
 * @package         Responsive Scroll Triggered Box
 * @author          Tassos Marinos <info@tassos.gr>
 * @link            http://www.tassos.gr
 * @copyright       Copyright © 2015 TM Extensions All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */

var jqRSTBox = jQuery.noConflict();

(function($) {
    $(document).ready(function() {

        /* Joomla <= 3.3.6 - Added change trigger when check the radio input */
        $('.btn-group label:not(.active)').click(function() {
            var input = $('#' + $(this).attr('for'));
            if (!input.prop('checked')) {
                input.trigger('change');
            }
        });

        /* Trigger Method */
        $("#jform_triggermethod").change(function() {
            val = $(this).val();
            switch (val) {
                case "pageheight":
                    disableObjects = ["triggerelement"];
                    break;
                case 'pageready':
                case "pageload":
                    disableObjects = ["triggerelement", "triggerpercentage","autohide", "hashtag"];
                    break;
                case "hashtag":
                case "userleave":
                case "ondemand":
                    disableObjects = ["triggerelement", "triggerpercentage","autohide"];
                    break;
                case "element":
                    disableObjects = ["triggerpercentage"];
                    break;
                case "elementHover":
                    disableObjects = ["triggerpercentage","autohide"];
                    break;
            }

            form = $(this).closest("form#adminForm");

            form.find(".control-group").removeClass("hide");
            disableObjects.each(function(x) {
                form.find(".control-group."+x).addClass("hide");
            })
        }).trigger('change');

        /* Show/Hide Fieldsets */
        showOnFields = [
            {
                "trigger": $('input[name="jform[autoclose]"]'),
                "triggerValues": ["1"],
                "showField" : $(".autoclosevalue")
            },
            {
                "trigger": $('select#jform_boxtype'),
                "triggerValues": ["module"],
                "showField" : $("#module")
            },
            {
                "trigger": $('select#jform_boxtype'),
                "triggerValues": ["custom"],
                "showField" : $("#custom")
            },
            {
                "trigger": $('select#jform_boxtype'),
                "triggerValues": ["emailform"],
                "showField" : $("#emailform")
            },
            {
                "trigger": $('input[name="jform[overlay]"]'),
                "triggerValues" : ["1"],
                "showField": $(".overlaycolor, .overlaypercent, .overlayclick")
            },
            {
                "trigger": $('select[name="jform[cookietype]"]'),
                "triggerValues": ["seconds","minutes", "days", "hours"],
                "showField": $(".cookie")
            }
        ]

        $.each(showOnFields, function(key, value) {
            trigger = $(value.trigger);
            trigger.on("change", function() {

                valid = false;

                if (value.triggerValues == ":checked") {
                    if ($(this).is(":checked")) {
                        valid = true;
                    }
                }

                if ($.isArray(value.triggerValues)) {
                    if ($.inArray($(this).val(), value.triggerValues) > -1) {
                        valid = true;
                    }   
                }  

                if (valid) {
                    value.showField.slideDown("fast");
                } else {
                    value.showField.slideUp("fast");
                }
            });

            if (trigger.is("select")) {
                trigger.trigger("change");
            }

            if (trigger.is("input:radio")) {
                trigger.filter(":checked").trigger("change"); 
            }

        });
    });
}(jqRSTBox));
