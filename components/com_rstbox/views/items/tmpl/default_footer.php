<?php 
	$ver = RstboxHelper::getVer();
?>

<div id="rstboxfooter" class="center">
    <?php echo JText::sprintf('COM_RSTBOX_DESC', $ver) ?><br>
    <div class="nn_footer_review"><?php echo JText::_("COM_RSTBOX_LIKE_THIS_EXTENSION") ?>
        <a href="http://www.tassos.gr/joomla-extensions/responsive-scroll-triggered-box-for-joomla/review" target="_blank"><?php echo JText::_("COM_RSTBOX_LEAVE_A_REVIEW") ?></a> 
        <a href="http://www.tassos.gr/joomla-extensions/responsive-scroll-triggered-box-for-joomla/review" target="_blank" class="stars"><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span><span class="icon-star"></span></a>
    </div>

    <Br>&copy; <?php echo JText::sprintf('COM_RSTBOX_COPYRIGHT', date("Y")) ?><br>
    <?php echo JText::_("COM_RSTBOX_NEED_SUPPORT") ?> 

	<a href="http://www.tassos.gr/joomla-extensions/responsive-scroll-triggered-box-for-joomla/docs" target="_blank"><?php echo JText::_("COM_RSTBOX_READ_DOCUMENTATION") ?></a> or
    <a href="http://www.tassos.gr/contact?s=BackEndSupport-<?php echo $ver ?>" target="_blank"><?php echo JText::_("COM_RSTBOX_DROP_EMAIL") ?></a>
</div>