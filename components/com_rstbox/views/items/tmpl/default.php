<?php
/**
 * @package     Responsive Scroll Triggered Box
 * @subpackage  com_rstbox
 *
 * @copyright   Copyright (C) 2014 Tassos Marinos - http://www.tassos.gr
 * @license     GNU General Public License version 2 or later; see http://www.tassos.gr/license
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
// load tooltip behavior
JHtml::_('behavior.tooltip');
JHtml::_('behavior.multiselect');

if (RstboxHelper::jVer() == "3") {
    JHtml::_('formbehavior.chosen', 'select');
}

function assignmentSelectionNames($item, $param) {

    if (!isset($item->params->{'assign_' . $param . "Names"})) {
        return '<span class="label label-badge">'.JText::_("COM_RSTBOX_IGNORE").'</span>';
    }

    $label = ($item->params->{'assign_' . $param} == 1) ? "success" : "important";

    $html = array();

    foreach ($item->params->{'assign_' . $param . "Names"} as $value) {
        $html[] = '<span title="'. (($label == "success") ? JText::_("COM_RSTBOX_INCLUDE") : JText::_("COM_RSTBOX_EXCLUDE")) ." ". $value->title.'" class="label label-'.$label.' hasTooltip">'.$value->title.'</span>';
    }

    return implode("\n",$html);
}

?>

<div class="rstbox rstbox-items">
    <form action="<?php echo JRoute::_('index.php?option=com_rstbox'); ?>" method="post" name="adminForm" id="adminForm">
        <?php
            // Search tools bar only for Joomla 3
            if (RstboxHelper::jVer() == "3") {
                echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
            }
        ?>

        <table class="adminlist table table-striped">
            <thead>
                <tr>
                    <th class="center" width="2%">
                        <?php if (RstboxHelper::jVer() == "3") { ?>
                            <?php echo JHtml::_('grid.checkall'); ?>
                        <?php } else { ?>
                            <input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
                        <?php } ?>
                    </th>
                    <th class="center" width="7%"><a href="#"><?php echo JText::_('COM_RSTBOX_ITEM_PUBLISHED'); ?></a></th>
                    <th class="title"><a href="#"><?php echo JText::_('COM_RSTBOX_ITEM_TITLE'); ?></a></th>
                    <th width="14%"><a href="#"><?php echo JText::_('COM_RSTBOX_ITEM_FIELD_TYPE'); ?></a></th>
                    <th width="13%"><?php echo JText::_('COM_RSTBOX_ACCESSLEVEL'); ?></th>
                    <th width="13%"><?php echo JText::_('COM_RSTBOX_ITEM_BOX_POSITION'); ?></th>
                    <th width="13%"><?php echo JText::_('COM_RSTBOX_ITEM_TRIGGER'); ?></th>
                    <th width="13%"><?php echo JText::_('COM_RSTBOX_ITEM_COOKIE'); ?></th>
                    <th width="4%"><a href="#"><?php echo JText::_('COM_RSTBOX_ID'); ?></a></th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($this->items)) { ?>
                    <?php foreach($this->items as $i => $item): ?>
                        <tr class="row<?php echo $i % 2; ?>">
                            <td class="center"><?php echo JHtml::_('grid.id', $i, $item->id); ?></td>
                            <td class="center"><?php echo JHtml::_('jgrid.published', $item->published, $i, 'items.', true); ?></td>
                            <td>
                                <a href="<?php echo JRoute::_('index.php?option=com_rstbox&task=item.edit&id='.$item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>"><?php echo RstboxHelper::pretty($this->escape($item->name)); ?></a>
                                <?php if (RstboxHelper::boxHasCookie($item->id)) { ?>
                                    <span class="label label-important hasTooltip" title="<?php echo JText::_("COM_RSTBOX_HIDDEN_BY_COOKIE_DESC") ?>">
                                        <?php echo JText::_("COM_RSTBOX_HIDDEN_BY_COOKIE") ?>
                                    </span>                
                                <?php } ?>

                                <?php if ($item->testmode) { ?>
                                    <span class="label hasTooltip" title="<?php echo JTEXT::_("COM_RSTBOX_ITEM_TESTMODE_DESC") ?>">
                                        <?php echo JText::_("COM_RSTBOX_ITEM_TESTMODE") ?>
                                    </span>
                                <?php } ?>

                                <div class="small"><?php echo (isset($item->params->note)) ? $item->params->note : "" ?></div>
                            </td>
                            <td>
                                <span class="label label-info"><?php echo ucfirst($item->boxtype) ?></span>
                            </td>
                            <td><?php echo assignmentSelectionNames($item, "usergroups") ?></td>
                            <td><?php echo RstboxHelper::pretty($item->position) ?></td>
                            <td><?php echo RstboxHelper::pretty($item->triggermethod) ?></td>
                            <td>
                                <?php 
                                    switch ($item->params->cookietype) {
                                        case "session":
                                        case "never":
                                            echo RstboxHelper::pretty($item->params->cookietype);
                                            break;
                                        case "ever":
                                            echo JText::_("COM_RSTBOX_FOREVER");
                                            break;
                                        default:
                                            echo $item->cookie . " " . RstboxHelper::pretty($item->params->cookietype);
                                            break;
                                    }
                                ?>
                            </td>
                            <td><?php echo $item->id ?></td>
                        </tr>
                    <?php endforeach; ?>  
                <?php } else { ?>
                    <tr>
                        <td align="center" colspan="9">
                            <div align="center"><?php echo JText::_('COM_RSTBOX_ERROR_NO_BOXES') ?></div>
                        </td>
                    </tr>
                <?php } ?>        
            </tbody>
            <tfoot>
    			<tr><td colspan="9"><?php echo $this->pagination->getListFooter(); ?></td></tr>
            </tfoot>
        </table>
        <div>
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="boxchecked" value="0" />
            <?php echo JHtml::_('form.token'); ?>
        </div>
    </form>
    <?php echo $this->loadTemplate('footer'); ?>
</div>