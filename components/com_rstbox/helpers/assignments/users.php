<?php

defined('_JEXEC') or die;

class tmFrameworkAssignmentsUsers extends tmFrameworkAssignmentsHelper 
{

	private $session;
	private $selection;

	function __construct($assignment) {
    	$this->selection = $assignment->selection;
    	$this->session = JFactory::getSession();
   	}

	function passGroupLevels()
	{
		$user = JFactory::getUser();

		if (!empty($user->groups))
		{
			$groups = array_values($user->groups);
		}
		else
		{
			$groups = $user->getAuthorisedGroups();
		}

    	return $this->passSimple($groups, $this->selection); 
	}

	function passTimeOnSite()
	{
		$pass = false;

		$sessionStartTime = strtotime(RstboxHelper::SessionStartTime());

		if (!$sessionStartTime) {
			return $pass;
		}

		$dateTimeNow = strtotime(RstboxHelper::dateTimeNow());
		$diffInSeconds = $dateTimeNow - $sessionStartTime;

		if (intval($this->selection) <= $diffInSeconds) {
			$pass = true;
		}

		return $pass;
	}
}
