<?php

defined('_JEXEC') or die;

class tmFrameworkAssignmentsDevices extends tmFrameworkAssignmentsHelper
{

	private $selection;

	function __construct($assignment) {
    	$this->selection = $assignment->selection;

        if (!class_exists('Mobile_Detect')) {
            require_once(JPATH_ADMINISTRATOR."/components/com_rstbox/helpers/vendors/Mobile_Detect.php");
        }
   	}

	function passDevices()
	{
        if (class_exists('Mobile_Detect')) {
            $detect = new Mobile_Detect;
            $detectDeviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'mobile') : 'desktop');

    		return $this->passSimple($detectDeviceType, $this->selection); 
        }
	}
}
