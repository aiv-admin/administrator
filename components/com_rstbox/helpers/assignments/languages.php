<?php

defined('_JEXEC') or die;

class tmFrameworkAssignmentsLanguages extends tmFrameworkAssignmentsHelper
{

	private $selection;

	function __construct($assignment) {
    	$this->selection = $assignment->selection;
   	}

	function passLanguages()
	{
		return $this->passSimple(JFactory::getLanguage()->getTag(), $this->selection); 
	}
}
