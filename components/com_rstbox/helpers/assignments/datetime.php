<?php


defined('_JEXEC') or die;

class tmFrameworkAssignmentsDateTime extends tmFrameworkAssignmentsHelper
{

	private $assignment;
	private $params;
	private $tz;
	private $date;

	function __construct($assignment) {
    	$this->assignment = $assignment;
    	$this->params = $assignment->params;
    	$this->tz = new DateTimeZone(JFactory::getApplication()->getCfg('offset'));
    	$this->date = JFactory::getDate()->setTimeZone($this->tz);
   	}

	function passDate()
	{

		$this->params->publish_up = $this->params->assign_datetime_param_publish_up;
		$this->params->publish_down = $this->params->assign_datetime_param_publish_down;

		if (!$this->params->publish_up && !$this->params->publish_down)
		{
			// no date range set
			return ($this->assignment->assignment == 'include');
		}

		RSTBoxHelper::fixDate($this->params->publish_up);
		RSTBoxHelper::fixDate($this->params->publish_down);

		$now = strtotime($this->date->format('Y-m-d H:i:s', true));
		$up = JFactory::getDate($this->params->publish_up)->setTimeZone($this->tz);
		$down = JFactory::getDate($this->params->publish_down)->setTimeZone($this->tz);

		if (
			(
				(int) $this->params->publish_up
				&& strtotime($up->format('Y-m-d H:i:s', true)) > $now
			)
			|| (
				(int) $this->params->publish_down
				&& strtotime($down->format('Y-m-d H:i:s', true)) < $now
			)
		)
		{
			// outside date range
			return $this->pass(false);
		}

		// pass
		return ($this->assignment->assignment == 'include');
	}
}
