<?php

defined('_JEXEC') or die;

class tmFrameworkAssignmentsMenu extends tmFrameworkAssignmentsHelper 
{

	private $selection;
	private $includeNoItemID;

	function __construct($assignment) {
    	$this->selection = $assignment->selection;
    	$this->includeNoItemID = isset($assignment->params->assign_menu_param_noitem) ? $assignment->params->assign_menu_param_noitem : false;
   	}

	function passMenu()
	{
        $menuActive = JFactory::getApplication()->getMenu()->getActive();

        if (!$menuActive || empty($this->selection)) {
        	return $this->pass($this->includeNoItemID);
        }

		return $this->passSimple($menuActive->id, $this->selection); 
	}
}
