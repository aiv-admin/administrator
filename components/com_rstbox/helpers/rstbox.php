<?php
/**
 * @package     Responsive Scroll Triggered Box
 * @subpackage  com_rstbox
 *
 * @copyright   Copyright (C) 2014 Tassos Marinos - http://www.tassos.gr
 * @license     GNU General Public License version 2 or later; see http://www.tassos.gr/license
 */
 
// No direct access to this file
defined('_JEXEC') or die;
 
abstract class RstboxHelper {
    public static $assdir = "components/com_rstbox/assets/";

    public static function getActions()
    {
        $user = JFactory::getUser();
        $result = new JObject;
        $assetName = 'com_rstbox';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action)
        {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


    /* General Functions */
    public static function getVer() {
        $xmlFile = JPATH_ADMINISTRATOR .'/components/com_rstbox/rstbox.xml';

        if (self::jVer() == "3") {
            $xml = JFactory::getXML($xmlFile);
            $version = (string)$xml->version;

            return $version;
        } else {
            $parser = JFactory::getXMLParser('Simple');
            $parser->loadFile($xmlFile);
            $doc = $parser->document;
            $element = $doc->getElementByPath('version');
            $version = $element->data();

            return $version;        
        }
    }

    public static function dateTimeNow() {
        return JFactory::getDate()->format("Y-m-d H:i:s");
    }

    public static function SessionStartTime() {
        $session = JFactory::getSession();
        
        $var = 'starttime';
        $sessionStartTime = $session->get($var);

        if (!$sessionStartTime) {
            $date = self::dateTimeNow();
            $session->set($var, $date);
        }

        return $session->get($var);
    }

    public static function clean($string) {
       $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
       return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function renderField($field, $blank = false) {

        if (!$field) {
            return;
        }

        $fieldName = self::clean(str_replace("jform","",$field->name));

        if ($blank) {
            return $field->label.$field->input;
        } else {
            return '<div class="control-group '.$fieldName.' clearfix "><div class="control-label">'.$field->label.'</div><div class="controls">'.$field->input.'</div></div>';
        }
    }

    public static function renderFormFields($fieldset) {
        $html = "";

        foreach ($fieldset as $field) {
            $html .= self::renderField($field);
        }

        return $html;
    }

    public static function renderLayout($file, $displayData) {
        /* Checking Joomla version */
        if (self::jVer() == "3") {
            /* If is 3 then render layout with JLayoutFile class help */
            $layout = new JLayoutFile($file, null, array('debug' => false, 'client' => 1, 'component' => 'com_rstbox'));
            $layoutOutput = $layout->render($displayData);
            return $layoutOutput;
        } else {
            /* Check if there is an override layout in templates folder */
            $mainframe = JFactory::getApplication();
            $templatePath = JPATH_THEMES."/".$mainframe->getTemplate()."/html/layouts/com_rstbox/";
            $componentPath = JPATH_ADMINISTRATOR."/components/com_rstbox/layouts/";
            $usePath = (JFile::exists($templatePath.$file.'.php')) ? $templatePath : $componentPath;

            /* If Joomla version is below 3 then render layout with custom render functions */
            ob_start();
            include $usePath.$file.".php";
            $layoutOutput = ob_get_contents();
            ob_end_clean();
            return $layoutOutput;
        }
    }

    public static function boxHasCookie($id) {
        if (!$id) {
            return;
        }

        $cookie = "rstbox_" . md5(JPATH_SITE) . "_" . $id;

        if (isset($_COOKIE[$cookie])) {
            return true;
        }

        return false;
    }

    public static function jVer() {
        if (version_compare(JVERSION, '3.0', '>=')) { return "3"; }
        if (version_compare(JVERSION, '3.0', '<')) { return "2.5"; }
    }

    public static function enableRenderPlugin() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);
         
        $fields = array(
            $db->quoteName('enabled') . '=1'
        );
         
        $conditions = array(
            $db->quoteName('type') . '="plugin"', 
            $db->quoteName('element') . '="rstbox"',
            $db->quoteName('folder') . '="system"'
        );
         
        $query->update($db->quoteName('#__extensions'))->set($fields)->where($conditions);
        $db->setQuery($query);
         
        return $db->query();
    }

    public static function getModuleTitle($id) {
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query->select('title');
        $query->from('#__modules');
        $query->where('id='.$db->q($id));        
        $rows = $db->setQuery($query);              
        return $db->loadResult();
    }

    public static function loadModule($id) {
        $db = JFactory::getDBO();
        $document = JFactory::getDocument();

        $renderer = $document->loadRenderer('module');
        
        $module_style = "none";

        $params = array('style'=>$module_style);
        
        $contents = '';
        $module = 0;
        
        //get module as an object       
        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__modules');
        $query->where('id='.$db->q($id));        
        $rows = $db->setQuery($query);              
        $rows = $db->loadObjectList();

        if ($rows) {
            foreach($rows as $row){     
                $params = array('style'=>$module_style);            
                $contents = $renderer->render($row, $params);
            }           
        }
        
        return $contents;
    }

    public static function boxRender($box) {
        switch ($box->boxtype) {
            case 'module':
                $layout = self::loadModule(intval($box->params->moduleid));
                return $layout;
                break;
            case 'emailform':
                $layout = self::renderLayout("emailform", $box);
                return $layout;
                break;
            default:
                return $box->customhtml;
                break;
        }

        return $box->customhtml;
    }

    public static function checkPublishingAssignments($boxes) {
        
        if (!$boxes) {
            return;
        }

        $passBoxes = array();

        // Check publishing assignments
        require_once(__DIR__."/assignments/assignments.php");
        $assignments = new tmFrameworkAssignmentsHelper();

        foreach ($boxes as $box) {
            $pass = $assignments->passAll($box);

            if ($pass) {
                $passBoxes[] = $box;
            }
        }   

        return $passBoxes;
    }

    public static function getBoxes() {
        $user = JFactory::getUser();
        $isRoot = ($user->get('isRoot')) ? 1 : 0;

        $query = "select b.* from #__rstbox b ";
        $query .= "where b.published = 1 ";
        if (!$isRoot) { $query .= " AND b.testmode=0"; }

        $boxes = self::runquery($query);
        $boxes = self::checkPublishingAssignments($boxes);

        if (is_array($boxes)) {
            foreach ($boxes as $box) {

                $box->params = json_decode($box->params);

                unset($box->_params);

                $settings_ = new stdClass();

                /* Box Class Suffix */
                $classes = array(
                    "rstbox_".$box->position,
                    "rstbox_".$box->boxtype,
                    (isset($box->params->classsuffix)) ? strip_tags($box->params->classsuffix) : "",
                    ((isset($box->params->verticalalign)) && ($box->params->verticalalign)) ? "rstboxVa" : ""
                );

                $box->classes = $classes;

                /* Box Style Attribute */
                $width = ($box->params->width) ? "max-width:".$box->params->width." ;" : "";
                $height = ($box->params->height) ? "height:".$box->params->height." ;" : "";
                $background = ($box->params->backgroundcolor) ? "background-color:".$box->params->backgroundcolor.";" : "";
                $color = ($box->params->textcolor) ? "color:".$box->params->textcolor." ;" : "";
                $border = ($box->params->bordercolor) ? "border:solid ".$box->params->borderwidth." ".$box->params->bordercolor.";" : "";
                $padding = "padding:".self::getParam($box->params, "padding", "20px").";";
                $shadow = (self::vld($box->params->boxshadow)) ? "box-shadow:".$box->params->boxshadow.";" : "";

                if (isset($box->params->zindex)) {
                    $zindex = "z-index:".$box->params->zindex.";";
                } else {
                    $zindex = "";
                }

                $customstyles = self::getParam($box->params, "customstyles", "");

                /* Overlay Prepare */
                if ((isset($box->params->overlay)) && ($box->params->overlay)) {
                    $settings_->overlay = $box->params->overlay_percent.":".$box->params->overlay_color.":".$box->params->overlayclick;
                }

                /* Other Settings */
                $settings_->delay = $box->params->triggerdelay;
                $settings_->transitionin = (isset($box->params->animationin)) ? $box->params->animationin : "rstbox.slideUpIn";
                $settings_->transitionout = (isset($box->params->animationout)) ? $box->params->animationout : "rstbox.slideUpOut";
                $settings_->duration = (isset($box->params->duration)) ? $box->params->duration : "400";
                $settings_->autohide = $box->params->autohide;
                $settings_->testmode = $box->testmode;

                // Prepare auto close option
                if ((bool) self::getParam($box->params, "autoclose", false)) {
                    if (intval($box->params->autoclosevalue) > 0) {
                        $settings_->autoclose = $box->params->autoclosevalue; 
                    }
                }

                $box->style = $width.$height.$background.$border.$padding.$color.$shadow.$zindex.$customstyles;

                /* Box Trigger Attribute */
                $trigger = $box->triggermethod;
                if ($box->triggermethod == "pageheight") { $trigger .= ":".$box->params->triggerpercentage; }
                if ($box->triggermethod == "element") { $trigger .= ":".$box->params->triggerelement; }
                if ($box->triggermethod == "elementHover") { 
                    $trigger .= ":".$box->params->triggerelement.":".$box->params->triggerdelay; 
                    $box->params->triggerdelay = 0;
                }

                /* Trigger by Location Hash */
                if (!in_array($box->triggermethod, array("pageready", "pageload"))) {
                    if ((isset($box->params->hashtag)) && (!empty($box->params->hashtag))) {
                        $settings_->triggerbyhash = $box->params->hashtag;
                    }
                }

                // Close Button Classes
                $closeButtonClasses = array(
                    "rstbox-close",
                    "rstbox_clbtn_".self::getParam($box->params, "closebutton_style", "default")
                );

                $box->closebuttonclasses = implode(" ", $closeButtonClasses);
                $box->settings_ = json_encode($settings_);
                $box->trigger_prepared = $trigger;
            }
        }

        return $boxes;
    }

    public static function getParam($obj, $param, $default = null) {
        if (!isset($obj->$param)) {
            return $default;
        }
        
        return $obj->$param;
    }

    public static function vld($prm) {
        if ((isset($prm)) && (!is_null($prm)) && (!empty($prm))) {
            return true;
        }
    }

    public static function searchFields($data, $find) {
        $arr = new stdClass;
        foreach ($data as $key => $value) {
            $pos = strpos($key, $find);
            if ($pos !== false) {
                $key_new = str_replace($find, "", $key);
                $arr->$key_new = $value;
            }
        }

        return $arr;
    }

    public static function msg($str) {
        JError::raiseNotice(403, $str);
    }

    public static function pretty($str) {
        $a = $str;
        $a = ucfirst(str_replace("_", " ", $a));
        return $a;
    }

    public static function loadassets() {
        $params = JComponentHelper::getParams('com_rstbox');
        $document = JFactory::getDocument();
        $back_dir = JURI::root(true)."/administrator/".self::$assdir;

        $document->addStyleSheet($back_dir.'css/styles.css?v='.self::getVer());

        if (self::jVer() == "2.5") {
            $document->addStyleSheet($back_dir.'css/styles.j25.css?v='.self::getVer());

            if ($params->get('jqueryback', 1)) {
                $document->addScript('//code.jquery.com/jquery-latest.min.js');
            }
        }

        $document->addScript($back_dir.'js/scripts.js?v='.self::getVer());
    }

    public static function loadassets_front() {
        if (self::jVer() == "3") {
            JHtml::_('jquery.framework');
        }

        $params = JComponentHelper::getParams('com_rstbox');
        $document = JFactory::getDocument();
        $front_dir = JURI::root(true)."/".self::$assdir;

        $document->addStyleSheet($front_dir.'css/rstbox.css?v='.self::getVer());

        if (self::jVer() == "2.5") {
            if ($params->get('jquery', 1)) { $document->addScript('//code.jquery.com/jquery-latest.min.js'); }
        }

        $document->addScript($front_dir.'js/rstbox.js?v='.self::getVer());
    }

    public static function runquery($q) {
        if (isset($q)) {
            $db = JFactory::getDBO();
            $db->setQuery($q);
            return $db->loadObjectList();
        }
    }

    public static function fetch($table, $columns = "*", $where = null, $singlerow = false) {
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $query
            ->select($columns)
            ->from("#__$table");
        
        if (isset($where)) {
            $query->where("$where");
        }
        
        $db->setQuery($query);
 
        return ($singlerow) ? $db->loadObject() : $db->loadObjectList();
    }

    public static function del($table, $where) {
        if ((isset($table)) && (isset($where))) {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);

            $query
                ->delete($db->quoteName("#__$table"))
                ->where($where);
            $db->setQuery($query);

            $result = $db->query();
            return  $result;
        }
    }


    public static function fixDate(&$date)
    {
        if (!$date)
        {
            $date = null;

            return;
        }

        $date = trim($date);

        // Check if date has correct syntax: 00-00-00 00:00:00
        if (preg_match('#^[0-9]+-[0-9]+-[0-9]+( [0-9][0-9]:[0-9][0-9]:[0-9][0-9])?$#', $date))
        {
            return;
        }

        // Check if date has syntax: 00-00-00 00:00
        // If so, add :00 (seconds)
        if (preg_match('#^[0-9]+-[0-9]+-[0-9]+ [0-9][0-9]:[0-9][0-9]$#', $date))
        {
            $date .= ':00';

            return;
        }

        // Check if date has a prepending date syntax: 00-00-00 ...
        // If so, add :00 (seconds)
        if (preg_match('#^([0-9]+-[0-9]+-[0-9]+)#', $date, $match))
        {
            $date = $match['1'] . ' 00:00:00';

            return;
        }

        // Date format is not correct, so return null
        $date = null;
    }

    public static function fixDateOffset(&$date)
    {
        if ($date <= 0)
        {
            $date = 0;

            return;
        }

        $date = JFactory::getDate($date, JFactory::getUser()->getParam('timezone', JFactory::getConfig()->get('offset')));
        $date->setTimezone(new DateTimeZone('UTC'));

        $date = $date->format('Y-m-d H:i:s', true, false);
    }
}