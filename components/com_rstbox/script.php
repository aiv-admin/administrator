<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
define('JV', (version_compare(JVERSION, '3', 'l')) ? 'j2' : 'j3');
 
/**
 * Script file of Responsive Scroll Triggered Box component
 */
class com_RstboxInstallerScript
{

    private $app;
    private $installedManifest;
    private $db;

    function preflight($type, $parent) 
    {

        $this->init();

        /* Run Schema Fix only on Joomla 2.5 */
        if (JV != 'j3') {
            if ($type == "update") {
                $com = $this->getComponent();
                $comManifest = json_decode($com["1"]);
                $extensionId = $com["0"];
                $extensionVersion = $comManifest->version;

                $this->schemaFix($extensionId, $extensionVersion);
            }
        }

        if ($type == "update") {
            if (version_compare($this->installedManifest->version, '2.2', 'l')) {
                $this->fixBoxes();
            }
        }

        $this->app->enqueueMessage("Please clear your browser's cache", 'notice');
        return true;
    }

    function postflight($type, $parent) {
        if ($type == "update") {
            if (version_compare($this->installedManifest->version, '2.1', 'l')) {
                $this->app->enqueueMessage("This new version comes with a critical bug fix concerning the assigned usergroups per box. We urge you to re-check your assigned usergroup settings in your RSTBoxes and verify that everything is in order.", 'notice');
            }

            if (version_compare($this->installedManifest->version, '2.2', 'l')) {

                // Remove unwanted columns
                $this->dropUnwantedColumns(array(
                    "animation",
                    "accesslevel",
                    "settings"
                ));

                // Remove unwanted tables
                $this->dropUnwantedTables(array(
                    "rstbox_menu"
                ));
            }
        }
    }

    function dropUnwantedTables($tables) {
        $db = $this->db;

        if (!$tables) {
            return;
        }

        foreach ($tables as $table) {
            $query = "DROP TABLE IF EXISTS #__".$db->escape($table);
            $db->setQuery($query);
            $db->execute();
        }
    }

    function dropUnwantedColumns($columns) {

        if (!$columns) {
            return;
        }

        $db = $this->db;

        // Check if columns exists in database
        function qt($n) {
            return(JFactory::getDBO()->quote($n));
        }
        
        $query = 'SHOW COLUMNS FROM #__rstbox WHERE Field IN ('.implode(",", array_map("qt", $columns)).')';
        $db->setQuery($query);
        $rows = $db->loadColumn(0);

        // Abort if we don't have any row
        if (!$rows) {
            return;
        }

        // Let's remove the columns
        $q = "";
        foreach ($rows as $key => $column) {
            $comma = (($key+1) < count($rows)) ? "," : "";
            $q .= "drop ".$this->db->escape($column).$comma;
        }

        $query = "alter table #__rstbox $q";

        $db->setQuery($query);
        $db->execute();
    }

    function init() {
        $this->db = JFactory::getDBO();

        $com = $this->getComponent();
        
        $this->installedManifest = json_decode($com["1"]);
        $this->app = JFactory::getApplication();
    }

    function fixBoxes() {
        $data = $this->fetch("rstbox");

        if (!$data) {
            return;
        }

        foreach ($data as $key => $box) {
            // Prepare data
            $object = new stdClass();
            $object->id = $box->id;
            $params = json_decode($box->params);

            if (version_compare($this->installedManifest->version, '2.0', 'l')) {
                // Fix animation field values on RSTBox < 2.0
                if (($box->animation == "fade") || ($box->animation == "slide")) {
                    $hasTopPosition = strpos($box->position, "top");

                    $transitionIn = ($box->animation == "fade") ? "transition.fadeIn" : (($hasTopPosition === false) ? "rstbox.slideUpIn" : "rstbox.slideDownIn");
                    $transitionOut = ($box->animation == "fade") ? "transition.fadeOut" : (($hasTopPosition === false) ? "rstbox.slideUpOut" : "rstbox.slideDownOut");

                    $object->animation = $transitionIn;
                    $params->animationout = $transitionOut;
                }

                // Fix animation duration
                if (isset($params->duration)) {
                    if ($params->duration == "slow") { $params->duration = "200"; }
                    if ($params->duration == "normal") { $params->duration = "400"; }
                    if ($params->duration == "fast") { $params->duration = "600"; }  
                }
            }

            if (version_compare($this->installedManifest->version, '2.1', 'l')) {

                // Fix User groups
                if ((isset($box->accesslevel)) && ((int)$box->accesslevel > 1)) {
                    $params->assign_usergroups = 1;
                    $params->assign_usergroups_list = array($box->accesslevel);
                }

                // Transfer animation to field to param objects
                if (isset($box->animation)) {
                    $params->animationin = $box->animation;
                }
            
                // Transform menu items relations
                $query = "SELECT menuid FROM #__rstbox_menu where boxid = " . $box->id;
                $this->db->setQuery($query);
                $items = array_values($this->db->loadColumn());

                if (!in_array("-1",$items)) {
                    $params->assign_menu = 1;
                    $params->assign_menu_list = $items;
                } else {
                    $params->assign_menu = 0;
                }
            }

            if (version_compare($this->installedManifest->version, '2.2', 'l')) {
                if (isset($box->settings)) {
                    $settings = json_decode($box->settings);
                    $params = (object) array_merge((array) $settings, (array) $params);
                }
            }

            $object->params = json_encode($params);
             
            // Update database
            $this->db->updateObject('#__rstbox', $object, 'id');
        }
    }

    function fetch($table, $columns = "*", $where = null, $singlerow = false) {
        if (!$table) {
            return;
        }

        $db = $this->db;
        $query = $db->getQuery(true);

        $query
            ->select($columns)
            ->from("#__$table");
        
        if (isset($where)) {
            $query->where("$where");
        }
        
        $db->setQuery($query);
 
        return ($singlerow) ? $db->loadObject() : $db->loadObjectList();
    }

    // deprecate
    function schemaFix($extension_id, $version_id) {
        $schemaCheck = $this->schemaIsOK($extensionId, $extensionVersion);

        if (!$schemaCheck) {

            $this->schemaDelete($extension_id);

            $profile = new stdClass();
            $profile->extension_id=$extension_id;
            $profile->version_id=$version_id;
            $this->db->insertObject('#__schemas', $profile);  
        }
    }

    // deprecate
    function schemaDelete($extension_id) {
        $db = $this->db;
        $query = $db->getQuery(true);
        $conditions = array($db->quoteName('extension_id') .'='. $extension_id);
        $query->delete($db->quoteName('#__schemas'));
        $query->where($conditions);
        $db->setQuery($query);
        $result = $db->execute();
    }

    // deprecate
    function schemaIsOK($extension_id, $version_id) {
        $db = $this->db;
        $query = $db->getQuery(true);
        $query
            ->select('COUNT(*)')
            ->from($db->quoteName('#__schemas'))
            ->where($db->quoteName('extension_id') . ' = '. $extension_id)
            ->where($db->quoteName('version_id') . ' = '. $db->quote($version_id));
        
        $db->setQuery($query);
        return $db->loadResult();       
    }

    function getComponent() {
        $db = $this->db;
        $query = $db->getQuery(true);
        $query
            ->select($db->quoteName(array("extension_id","manifest_cache")))
            ->from($db->quoteName('#__extensions'))
            ->where($db->quoteName('element') . ' = '. $db->quote('com_rstbox'));
        
        $db->setQuery($query);
        return $db->loadRow();
    }
}

?>
