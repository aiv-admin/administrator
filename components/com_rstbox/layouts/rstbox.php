<?php
/**
 * @package     Responsive Scroll Triggered Box
 * @subpackage  com_rstbox
 * @copyright   Copyright (C) 2014 Tassos Marinos - http://www.tassos.gr
 * @license     GNU General Public License version 2 or later; see http://www.tassos.gr/license
 */

defined('JPATH_BASE') or die;
$boxes = $displayData;
$p = JComponentHelper::getParams('com_rstbox');
$forceLoadMedia = $p->get("forceloadmedia");
$debug = $p->get("debug", 0);

$googleAnalyticsID = ($p->get("gaTrack", 0) && ($p->get("gaID") != "")) ? $p->get("gaID") : false;
$trackingAttr = ($googleAnalyticsID) ? $googleAnalyticsID.":".$p->get("gaLibrary", 2).":".$p->get("gaCategory") : false;

?>

<div class="rstboxes" data-site="<?php echo md5(JPATH_SITE) ?>" data-debug="<?php echo $debug ?>" <?php if ($trackingAttr) { ?> data-tracking="<?php echo $trackingAttr ?>" <?php } ?>>
	<?php if ($forceLoadMedia) { ?>
		<link rel="stylesheet" href="<?php echo JURI::root(true) ?>/components/com_rstbox/assets/css/rstbox.css?v=<?php echo RstboxHelper::getVer() ?>" type="text/css" />

        <?php if ($p->get('jquery', 1)) { ?>
        	<script src="//code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
        <?php } ?>

		<script src="<?php echo JURI::root(true) ?>/components/com_rstbox/assets/js/rstbox.js?v=<?php echo RstboxHelper::getVer() ?>" type="text/javascript"></script>
	<?php } ?>

	<?php foreach ($boxes as $box) { ?>
	
	<?php 
		$cookieType = (isset($box->params->cookietype)) ? $box->params->cookietype : "days";
		$customCSS = (isset($box->params->customcss)) ? $box->params->customcss : "";
		$showCloseButton = ((isset($box->params->hideclosebutton)) && ($box->params->hideclosebutton)) ? false : true;
	?>

	<div id="rstbox_<?php echo $box->id ?>" class="rstbox <?php echo implode(" ",$box->classes) ?>" data-settings='<?php echo $box->settings_; ?>' data-trigger="<?php echo $box->trigger_prepared ?>" data-cookietype="<?php echo $cookieType ?>" data-cookie="<?php echo $box->cookie ?>" data-title="<?php echo $box->name ?>" style="<?php echo $box->style ?>">
		
		<?php if ($customCSS) { echo "<style>$customCSS</style>"; }	?>
		<?php if ($showCloseButton) { ?><a aria-label="Close Box" class="<?php echo $box->closebuttonclasses ?>" href="#">&times;</a><?php } ?>

		<div class="rstbox-container">
			<?php if (in_array("rstboxVa", $box->classes)) { ?>
				<div class="rstbox-wrap-va-container">
					<div class="rstbox-wrap-va">
			<?php } ?>

			<?php if ($box->params->showtitle) { ?>
				<div class="rstbox-header">
					<div class="rstbox-heading"><?php echo $box->name ?></div>
				</div>
				<?php } ?>
			<div class="rstbox-content">
				<?php echo RstboxHelper::boxRender($box) ?>
			</div>

			<?php if (in_array("rstboxVa", $box->classes)) { ?>
				</div></div>
			<?php } ?>
		</div>

		<?php echo $box->params->customcode; ?>

	</div>	
	<?php } ?>
</div>
