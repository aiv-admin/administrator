<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Abstract LOGman Plugin
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
abstract class ComLogmanPluginAbstract extends PlgKoowaAbstract implements ComLogmanPluginInterface
{
    /**
     * LOGman component settings
     *
     * @var JRegistry
     */
    static private $_logman_params = null;

    /**
     * Constructor.
     *
     * @param   object             $dispatcher Event dispatcher
     * @param array|\KObjectConfig $config Configuration options
     */
    public function __construct(&$dispatcher, $config = array())
    {
        // Make params available as a JRegistry object to _initialize.
        if (isset($config['params']))
        {
            $params = new JRegistry;
            $params->loadString($config['params']);
            $config['params'] = $params;
        }

        parent::__construct($dispatcher, $config);

        // Load plugin translations.
        $this->getObject('translator')->load($this->getIdentifier());

        if (is_null(self::$_logman_params))
        {
            $params = JComponentHelper::getParams('com_logman');

            self::$_logman_params = array();

            foreach ($params->toArray() as $name => $value) {
                self::$_logman_params['logman_'.$name] = $value; // Namespace LOGman parameters.
            }

            self::$_logman_params = new JRegistry(self::$_logman_params);
        }

        // Merge plugin and LOGman parameters.
        $this->params->merge(self::$_logman_params);
    }

    /**
     * Adds/logs an activity row.
     *
     * @param array $data The activity data.
     * @throws Exception
     * @return object The activity row.
     */
    final public function log($data = array())
    {
        $result = false;
        $data   = new KObjectConfig($data);

        if ($this->_beforeLog($data) !== false)
        {
            try
            {
                $data   = KObjectConfig::unbox($data->activity);
                $result = $this->getObject('com://admin/logman.controller.activity')->add($data);
            }
            catch (Exception $e)
            {
                if (JDEBUG) {
                    throw $e;
                }
            }
        }

        return $result;
    }

    /**
     * Get a plugin parameter
     *
     * @param string $name      The parameter name
     * @param null   $default   The default value if the parameter doesn't exist.
     * @return mixed
     */
    public function getParameter($name, $default = null)
    {
        return $this->params->get($name, $default);
    }

    /**
     * Before log event handler.
     *
     * Set the activity data based on the passed config object, and set default based on the context we are logging in.
     *
     * @param KObjectConfig $config The configuration object.
     * @return bool Return false for preventing an activity from being logged.
     */
    protected function _beforeLog(KObjectConfig $config)
    {
        $config->append(array(
            'extension'     => 'com',
            'application' => JFactory::getApplication()->isAdmin() ? 'admin' : 'site'));

        $activity = array();

        $activity['type']     = $config->extension;
        $activity['package']  = $config->object->package;
        $activity['name']     = $config->object->type;
        $activity['row']      = $config->object->id;
        $activity['title']    = $config->object->name;
        $activity['metadata'] = $config->object->metadata;
        $activity['status']   = $config->result;
        $activity['action']   = $config->verb;

        if ($actor = $config->actor) {
            $activity['created_by'] = $actor;
        }

        $activity['application'] = $config->application;

        $config->activity = $activity;

        return true;
    }
}