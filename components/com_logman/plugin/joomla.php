<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Abstract Joomla LOGman Plugin
 *
 * Provides support for content and extension core events.
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
abstract class ComLogmanPluginJoomla extends ComLogmanPluginAbstract
{
    /**
     * A list of contexts for which activities will be logged.
     *
     * Events from contexts not listed in this array will not be logged.
     *
     * @var array
     */
    protected $_contexts;

    /**
     * The name of the component (with com_ prefix) this plugin handles events for.
     *
     * @var string
     */
    protected $_component;

    /**
     * Constructor.
     *
     * @param   KObjectConfig $config Configuration options
     */
    public function __construct(&$subject, $config = array())
    {
        parent::__construct($subject, $config);

        // Enforce component name based on the plugin name.
        $this->_component = 'com_' . strtolower($this->_name);

        $contexts = $this->getConfig()->contexts;

        if (!$contexts)
        {
            $contexts = array();

            foreach ($this->getConfig()->resources as $resource) {
                $contexts[] = $this->_component . '.' . $resource;
            }
        }

        $this->_contexts = $contexts;
    }

    /**
     * Initializes the options for the object
     *
     * Called from {@link __construct()} as a first step of object instantiation.
     *
     * @param   KObjectConfig $config Configuration options.
     * @return  void
     */
    protected function _initialize(KObjectConfig $config)
    {
        $config->append(array(
            'resources' => array()
        ));

        parent::_initialize($config);
    }

    /**
     * Before log event handler.
     *
     * Set the activity data based on the passed config object, and set default based on the context we are logging in.
     *
     * @param KObjectConfig $config The configuration object.
     * @return bool Return false for preventing an activity from being logged.
     */
    final protected function _beforeLog(KObjectConfig $config)
    {
        if ($context = $config->context)
        {
            $result = false;

            if (in_array($context, $this->_contexts))
            {
                $parts = explode('.', $context);

                list($type, $package) = explode('_', $parts[0]);
                $method = '_get' . ucfirst($parts[1]) . 'ObjectData';

                if (method_exists($this, $method)) {
                    $object_data = call_user_func_array(array($this, $method), array($config->data, $config->event));
                } else {
                    $object_data = array('id' => $config->data->id, 'name' => $config->data->title);
                }

                $object_data['type']    = $parts[1];
                $object_data['package'] = $package;

                $config->extension = $type;
                $config->object     = $object_data;

                $result = parent::_beforeLog($config);
            }
        }
        else $result = parent::_beforeLog($config);

        return $result;
    }

    /**
     * After content save event handler.
     *
     * @param string $context The event context.
     * @param mixed  $content The event content, aka data.
     * @param int    $isNew   Whether or not the content is new.
     */
    public function onContentAfterSave($context, $content, $isNew)
    {
        $this->log(array(
            'context' => $context,
            'data'    => $content,
            'verb'    => $isNew ? 'add' : 'edit',
            'event'   => 'onContentAfterSave'
        ));
    }

    /**
     * After content delete event handler.
     *
     * @param string $context The event context.
     * @param mixed  $content The event content, aka data.
     */
    public function onContentAfterDelete($context, $content)
    {
        $this->log(array(
            'context' => $context,
            'data'    => $content,
            'verb'    => 'delete',
            'event'   => 'onContentAfterDelete'
        ));
    }

    /**
     * After extension save event handler.
     *
     * @param $context
     * @param $data
     * @param $isNew
     */
    public function onExtensionAfterSave($context, $data, $isNew)
    {
        $this->log(array(
            'context' => $context,
            'data'    => $data,
            'verb'    => $isNew ? 'add' : 'edit',
            'event'   => 'onExtensionAfterSave'
        ));
    }

    /**
     * After extension delete event handler.
     *
     * @param $context
     * @param $data
     */
    public function onExtensionAfterDelete($context, $data)
    {
        $this->log(array(
            'context' => $context,
            'data'    => $data,
            'verb'    => 'delete',
            'event'   => 'onExtensionAfterDelete'
        ));
    }

    /**
     * Content change event handler.
     *
     * @param string $context The event context.
     * @param array  $pks     A list of the primary keys to change
     * @param int    $state   The state that was set.
     */
    public function onContentChangeState($context, $pks, $state)
    {
        if (in_array($context, $this->_contexts))
        {
            $config = new KObjectConfig(array('context' => $context));

            $parts = explode('.', $context);

            if (is_array($parts) && count($parts) === 2) {
                $config->append(array('type' => $parts[1]));
            }

            $items = $this->_getItems($pks, $config);

            foreach ($items as $item)
            {
                switch ($state)
                {
                    case -2:
                        $verb   = 'trash';
                        $result = 'trashed';
                        break;
                    case 0:
                        $verb   = 'unpublish';
                        $result = 'unpublished';
                        break;
                    case 1:
                        $verb   = 'publish';
                        $result = 'published';
                        break;
                    case 2:
                        $verb   = 'archive';
                        $result = 'archived';
                        break;
                    default: // Unknown state. Ignore event.
                        return;
                        break;
                }

                $this->log(
                    array(
                        'context' => $context,
                        'result'  => $result,
                        'data'    => $item,
                        'verb'    => $verb,
                        'event'   => 'onContentChangeState'
                    ));
            }
        }
    }

    /**
     * Items getter.
     *
     * @param int           $ids    The identifier of the items to get.
     * @param KObjectConfig $config The configuration object.
     *
     * @return array An array of items.
     */
    protected function _getItems($ids, KObjectConfig $config)
    {
        $items = array();
        $ids   = (array) $ids;

        $config->append(
            array(
                'prefix' => 'JTable',
                'config' => array(),
                'path'   => JPATH_ADMINISTRATOR . '/components/' . $this->_component . '/tables',
                'type'   => KStringInflector::singularize($this->_name)
            )
        );

        foreach ($ids as $id)
        {
            JTable::addIncludePath($config->path);

            if ($table = JTable::getInstance($config->type, $config->prefix, $config->config))
            {
               if ($table->load($id)) {
                   $items[] = $table;
               }
            }
        }

        return $items;
    }
}