<?
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('_JEXEC') or die; ?>

<?= helper('behavior.jquery') ?>

<script>
kQuery(function($) {
    var end_date = $('#end_date');

    if (!end_date.val()) {
    	end_date.val(<?= json_encode(helper('date.format', array('format' => translate('DATE_FORMAT_LC4')))); ?>);
    }

    $('#activities-filter').on('reset', function(e) {
        e.preventDefault();

        $(this).find('input').each(function(i, el) {
           if ($.inArray($(el).attr('name'), ['day_range','end_date', 'user']) !== -1) {
               $(el).val('');
           }
        });

        $('select[name="usergroup[]"]').select2("val", null, true);

        $(this).append('<input type="hidden" name="usergroup" value="" />');

        $(this).submit();
    });

    $('#activities-filter').on('submit', function(e) {
        if (!$('select[name="usergroup[]"]').select2("val").length) {
            $(this).append('<input type="hidden" name="usergroup" value="" />');
        }
    });
});
</script>
<div id="logman-sidebar">
	<h3><?=translate( 'Components' )?></h3>
	<ul class="logman-components">
		<li class="<?= empty(parameters()->package) ? 'active' : ''; ?>">
			<a href="<?= route('package=') ?>">
		    <?= translate('All components')?>
			</a>
		</li>
	    <?php foreach ($packages as $package): ?>
		    <?php if ($package == parameters()->package): ?>
				<li class="active">
		    <?php else: ?> <li> <?php endif ?>
				<a href="<?=route('package='.$package)?>"><?= translate(ucfirst($package))?></a>
			</li>
	    <?php endforeach ?>
	</ul>

	<div class="activities-filter">
		<form action="" method="get" id="activities-filter" class="form-vertical">
            <h3><?=translate( 'Filter by users' )?></h3>
            <div class="sidebar-panel">
                <div class="controls logman-activities-filters__by-user">
                    <?=helper('listbox.users', array(
                        'sort' => 'name',
                        'attribs' => array(
                            'id' => 'user',
                            'class' => 'input-block-level', 'placeholder' => translate('Enter a name&hellip;'),
                            'onchange' => 'this.form.submit()'
                        )
                    ))?>
                </div>
                <div class="controls activities-filter-by-groups">
                    <?= helper('listbox.usergroups', array('selected' => parameters()->usergroup)) ?>
                </div>
            </div>

            <h3><?=translate( 'Filter by date' )?></h3>
            <div class="sidebar-panel">
                <div class="controls" style="border-bottom: none;">
                    <div class="logman-activities-filters__calendar">
                        <label for="end_date"><?=translate( 'Show events until' )?></label>
                        <?= helper('behavior.calendar',
                                array(
                                    'attribs' => array('class' => 'input-small'),
                                    'value' => parameters()->end_date,
                                    'name' => 'end_date',
                                    'format' => '%Y-%m-%d'
                                )); ?>
                    </div>
                    <div class="logman-activities-filters__days-back input-prepend input-append">
                        <span class="add-on"><?= translate( 'Going back' )?></span><input type="text" size="3" id="day_range" name="day_range" class="span1" value="<?=parameters()->day_range?>" placeholder="&nbsp;&nbsp;&infin;" /><span class="add-on"><?= translate('days') ?></span>
                    </div>
                    <div class="logman-activities-filters__buttons">
                        <input type="reset" name="cancelfilter" class="btn" value="<?=translate('Reset')?>" />
                        <input type="submit" name="submitfilter" class="btn btn-primary" value="<?=translate('Filter')?>" />
                    </div>
                </div>
            </div>
		</form>
	</div>
</div>