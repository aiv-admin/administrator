<?
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('_JEXEC') or die; ?>

<?= helper('bootstrap.load', array('package' => 'logman')) ?>
<?= helper('behavior.koowa') ?>
<?= helper('behavior.jquery') ?>
<?= helper('behavior.modal') ?>
<?= helper('bootstrap.load', array('javascript' => true)) ?>

<? // RSS feed ?>
<link href="<?=route('format=rss');?>" rel="alternate" type="application/rss+xml" title="RSS 2.0" />

<ktml:module position="toolbar">
    <ktml:toolbar type="actionbar" title="LOGman">
</ktml:module>

<div class="logman_container">

    <?= import('default_sidebar.html')?>

    <div class="logman_admin_list_grid">
        <form action="" method="get" class="-koowa-grid">
            <div class="logman_table_layout logman_table_layout--default">
                <table class="table table-striped koowa_table koowa_table--activities">
                    <thead>
                        <tr class="logman-activities-list__header">
                            <th width="10" align="center">
                                <?= helper('grid.checkall') ?>
                            </th>
                            <th><?=translate('Message')?></th>
                            <th><?=translate('Time')?></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <td colspan="3">
                                <?= helper('paginator.pagination') ?>
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                    <? $date = $old_date = '';   ?>
                    <? foreach ($activities as $activity) : ?>
                        <? $date = helper('date.format', array('date' => $activity->created_on, 'format' => translate('DATE_FORMAT_LC3')))?>
                        <? if ($date != $old_date): ?>
                        <? $old_date = $date; ?>
                        <tr class="logman_table_layout__item--header">
                            <th colspan="3">
                                <?= $date; ?>
                            </th>
                        </tr>
                        <? endif; ?>
                        <tr class="logman_table_layout__item <?=($grey_self && (object('user')->getId() == $activity->created_by))?'greyed':''?>">
                            <td>
                                <a href="#" style="color:inherit"><?= helper('grid.checkbox',array('entity' => $activity)); ?></a>
                            </td>
                            <td class="logman_table_layout__message">
                                <div class="koowa_wrapped_content">
                                    <div class="whitespace_preserver">
                                        <i class="<?=$activity->image?>"></i>
                                        <?= helper('activity.activity', array('entity' => $activity, 'scripts' => true))?>
                                    </div>
                                </div>
                            </td>
                            <td align="left" class="logman_table_layout__time nowrap">
                                <?= helper('activity.when', array('entity' => $activity))?>
                            </td>
                        </tr>
                    <? endforeach; ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>
</div>
