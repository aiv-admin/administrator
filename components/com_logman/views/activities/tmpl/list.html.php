<?
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */
defined('_JEXEC') or die; ?>

<? if(count($activities)) : ?>
    <?= helper('bootstrap.load') ?>
    <?= helper('behavior.jquery') ?>
    <?= helper('behavior.modal')?>
    <?= helper('bootstrap.load', array('javascript' => true)) ?>

    <div class="logman_container">
        <table class="table table-striped simple logman_table_layout logman_table_layout--list">
            <tfoot>
            <? if ($view_all): ?>
                <tr class="logman_table_layout__viewall">
                    <td colspan="2">
                        <a href="<?= JRoute::_('index.php?option=com_logman&view=activities'); ?>" class="btn"><?=translate('View all')?></a>
                    </td>
                </tr>
            <? endif; ?>
            </tfoot>
            <tbody>
            <? foreach ($activities as $activity) : ?>
                <tr class="logman_table_layout__item">
                    <td class="logman_table_layout__message">
                        <div class="koowa_wrapped_content">
                            <div class="whitespace_preserver">
                                <i class="<?=$activity->image?>"></i>
                                <?= helper('activity.activity', array('entity' => $activity, 'scripts' => true))?>
                            </div>
                        </div>
                    </td>
                    <td align="right" class="logman_table_layout__time">
                        <?= helper('activity.when', array('entity' => $activity, 'humanize' => true))?>
                    </td>
                </tr>
            <? endforeach; ?>
            </tbody>
        </table>
    </div>
<? endif ?>