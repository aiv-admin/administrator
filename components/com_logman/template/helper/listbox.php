<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * Listbox Template Helper
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class ComLogmanTemplateHelperListbox extends ComKoowaTemplateHelperListbox
{
    public function usergroups($config = array())
    {
        $config = new KObjectConfigJson($config);

        $adapter = $this->getObject('lib:database.adapter.mysqli');
        $query = $this->getObject('lib:database.query.select')->table('usergroups')->columns(array('id','title'));

        $groups = $adapter->select($query, KDatabase::FETCH_OBJECT_LIST);

        $options = array();

        foreach($groups as $group) {
            $options[] = $this->option(array('value' => $group->id, 'label' => $group->title));
        }

        $translator = $this->getObject('translator');

        $config->append(array(
            'name'     => 'usergroup',
            'select2'  => true,
            'attribs'  => array('multiple' => true),
            'deselect' => true,
            'options'  => $options,
            'prompt' => $translator->translate('Select user groups')
        ));

        return $this->optionlist($config);
    }

    public function packages($config = array())
    {
        $config = new KObjectConfig($config);

        $config->append(array('select2' => true, 'installed' => true));

        $iterator = new DirectoryIterator(JPATH_PLUGINS . '/logman');

        $options = array();

        $translator = $this->getObject('translator');

        foreach ($iterator as $directory)
        {
            $name = $directory->getBasename();

            $manifest = sprintf('%s/%s.xml', $directory->getPathname(), $name);

            if (file_exists($manifest))
            {
                $manifest = simplexml_load_file($manifest);

                // Assume logger by default.
                $type = 'logger';

                if (isset($manifest->plugin['type'])) {
                    $type = (string) $manifest->plugin['type'];
                }

                if ($type == 'logger')
                {
                    $packages = '';

                    if (isset($manifest->plugin['packages'])) {
                        $packages = str_replace(' ', '', (string) $manifest->plugin['packages']);
                    }

                    if (empty($packages)) {
                        $packages = $name; // Use the plugin name.
                    }

                    $packages = explode(',', $packages);

                    foreach ($packages as $package)
                    {
                        if (!empty($package) && !isset($options[$package]))
                        {
                            $component = 'com_' . $package;

                            // Check if only installed components should be added to the list.
                            if (!$config->installed || $this->_componentInstalled($component))
                            {
                                // Load component translations.
                                $lang      = JFactory::getLanguage();
                                $lang->load($component . '.sys', JPATH_BASE, null, false, false)
                                || $lang->load($component . '.sys', JPATH_ADMINISTRATOR . '/components/' . $component, null, false, false)
                                || $lang->load($component . '.sys', JPATH_BASE, $lang->getDefault(), false, false)
                                || $lang->load($component . '.sys', JPATH_ADMINISTRATOR . '/components/' . $component, $lang->getDefault(),
                                    false, false);

                                $options[$package] = $this->option(array(
                                        'label' => $translator->translate('com_' . $package),
                                        'value' => $package
                                    )
                                );
                            }


                        }
                    }
                }
            }
        }

        $config->options = array_values($options);

        return parent::optionlist($config);
    }

    /**
     * Checks if a component is installed.
     *
     * @param string $component The component name.
     *
     * @result bool True if installed, false otherwise.
     */
    protected function _componentInstalled($component)
    {
        $query = $this->getObject('lib:database.query.select')
                      ->table('extensions')
                      ->columns('COUNT(*)')
                      ->where('element = :element')
                      ->bind(array('element' => $component));


        return (bool) $this->getObject('lib:database.adapter.mysqli')->select($query, KDatabase::FETCH_FIELD);
    }
}