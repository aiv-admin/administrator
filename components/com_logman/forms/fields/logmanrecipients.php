<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

jimport('joomla.form.formfield');
JFormHelper::loadFieldClass('usergroup');

/**
 * Recipients Field
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class JFormFieldLogmanrecipients extends JFormField
{
    public function getInput()
    {

        if (!class_exists('Koowa')) {
            return '';
        }

        $manager = KObjectManager::getInstance();

        //$manager->getObject('translator')->load('com://admin/logman');

        if (isset($this->element['id'])) {
            $id = (string) $this->element['id'];
        } else {
            if (version_compare(JVERSION, 3.3, '<')) {
                $id = 'jform'.$this->element['name'];
            } else {
                $id = 'jform_'.$this->element['name'];
            }
        }

        $value = $this->value;
        $name = $this->name;

        $template = $manager->getObject('com://admin/logman.template.default')
                            ->addFilter('style')
                            ->addFilter('asset')
                            ->addFilter('script');

        $string = "
            <?= helper('bootstrap.load') ?>
            <?= helper('behavior.select2') ?>

            <input type='hidden' id='<?= \$id ?>' name='<?= \$name ?>' value='<?= \$value ?>'/>

            <script>
                kQuery(function($) {
                    $('#<?= \$id ?>').select2({
                        tags: [],
                      tokenSeparators: [',', ' ']});
                });
            </script>
        ";

        return $template->loadString($string, 'php')->render(array('value' => $value, 'id' => $id, 'name' => $name));
    }
}