<?php
/**
 * @package     LOGman
 * @copyright   Copyright (C) 2011 - 2015 Timble CVBA. (http://www.timble.net)
 * @license     GNU GPLv3 <http://www.gnu.org/licenses/gpl.html>
 * @link        http://www.joomlatools.com
 */

/**
 * LOGman installer
 *
 * @author  Arunas Mazeika <https://github.com/amazeika>
 * @package Joomlatools\Component\LOGman
 */
class com_logmanInstallerScript
{
	/**
	 * Name of the component
	 */
	public $component;

    /**
     * @var string The current installed EXTman version.
     */
    protected $_extman_ver = null;

    /**
     * @var string The current installed LOGman version.
     */
    protected $_current_ver = null;

	public function __construct($installer)
	{
		preg_match('#^com_([a-z0-9_]+)#', get_class($this), $matches);
		$this->component = $matches[1];
     }

	public function preflight($type, $installer)
	{
	    $return = true;
        $errors = array();

		if (!class_exists('Koowa') || !class_exists('ComExtmanControllerExtension'))
		{
			if (file_exists(JPATH_ADMINISTRATOR.'/components/com_extman/extman.php') && !JPluginHelper::isEnabled('system', 'koowa')) {
				$errors[] = sprintf(JText::_('This component requires System - Nooku Framework plugin to be installed and enabled. Please go to <a href=%s>Plugin Manager</a>, enable <strong>System - Nooku Framework</strong> and try again'), JRoute::_('index.php?option=com_plugins&view=plugins&filter_folder=system'));
			}
			else $errors[] = JText::_('This component requires EXTman to be installed on your site. Please download this component from <a href=http://joomlatools.com target=_blank>joomlatools.com</a> and install it');

		    $return = false;
		}

        // Check EXTman version.
        if ($return === true)
        {
            if (version_compare($this->getExtmanVersion(), '2.1.2', '<') || !class_exists('ComExtmanModelEntityExtension'))
            {
                $errors[] = JText::_('This component requires a newer EXTman version. Please download EXTman from <a href=http://joomlatools.com target=_blank>joomlatools.com</a> and upgrade it first.');
                $return   = false;
            }
        }

        if ($return == true && $type == 'update')
        {
            if ($current = $this->getCurrentVersion())
            {
                require_once JPATH_ADMINISTRATOR . '/components/com_extman/resources/install/helper.php';
                $helper = new ComExtmanInstallerHelper();

                if ($failed = $helper->checkDatabasePrivileges(array('ALTER')))
                {
                    $errors[] = JText::sprintf('The following MySQL privileges are missing: %s. Please make them available to your MySQL user and try again.',
                        htmlspecialchars(implode(', ', $failed), ENT_QUOTES));
                    $return   = false;
                }
                elseif($failed = $this->_dbUpdate($installer, array('from_ver' => $current)))
                {
                    $errors[] = JText::_('DB schema update failed while processing queries:');
                    foreach ($failed as $query) {
                        $errors[] = htmlspecialchars($query, ENT_QUOTES);
                    }
                    $return = false;
                }

                // Old system plugin needs to be un-installed.
                if (version_compare($current, '1.0.0RC4', '<'))
                {
                    $extension = KObjectManager::getInstance()->getObject('com://admin/extman.model.extension')->identifier('plg:system.logman')->fetch();
                    if (!$extension->isNew()) {
                        $extension->delete();
                    }
                }

            }
            else
            {
                $errors[] = JText::_('Update failed. Unable to determine previous installed version of LOGman.');
                $return   = false;
            }
        }

        if ($return == false && $errors)
        {
            $error = implode('<br />', $errors);
            $installer->getParent()->abort($error);
        }

        if ($return === true)
        {
            if (version_compare($this->getCurrentVersion(), '2.0', '<') && version_compare($this->getExtmanVersion(), '2.0', '>')) {
                $this->_upgrade_to_v2 = true;
            }
        }

		return $return;
	}

	public function postflight($type, $installer)
	{
        $extension_id = ComExtmanModelEntityExtension::getExtensionId(array(
			'type' 	  => 'component',
			'element' => 'com_'.$this->component
		));

        $controller = KObjectManager::getInstance()->getObject('com://admin/extman.controller.extension')
            ->view('extension')
            ->layout('success')
            ->event($type === 'update' ? 'update' : 'install');

        $controller->add(array(
            'source'              => $installer->getParent()->getPath('source'),
            'manifest'            => $installer->getParent()->getPath('manifest'),
            'joomla_extension_id' => $extension_id,
            'install_method'        => $type,
            'event'               => $type === 'update' ? 'update' : 'install'
        ));

        echo $controller->render();

        if (!empty($this->_upgrade_to_v2))
        {
            // Remove old plugin
            $query = "DELETE FROM #__extensions WHERE type='plugin' AND folder='logman' AND element='extension'";
            JFactory::getDBO()->setQuery($query)->query();

            $this->_enableComponent();

            jimport('joomla.filesystem.folder');
            $path = JPATH_PLUGINS.'/logman/extension';
            if (JFolder::exists($path)) {
                JFolder::delete($path);
            }
        }

        // Check if the activities table got successfully renamed and disable LOGman if it didn't
        if ($type == 'update' && version_compare($this->getCurrentVersion(), '2.0.2', '<'))
        {
            $prefix = JFactory::getApplication()->getCfg('dbprefix');

            $query = sprintf("SHOW TABLES LIKE '%slogman_activities'", $prefix);

            if (!JFactory::getDBO()->setQuery($query)->loadResult()) {
                $this->_enableComponent(false);
                JLog::add('The installer failed to rename the activities database during the upgrade', JLog::WARNING, 'jerror');
            }
        }
	}

    protected function _enableComponent($state = true)
    {
        $db = JFactory::getDBO();

        $state = (int) $state;

        // Enable logging
        $query = "UPDATE #__extensions SET enabled = {$state} WHERE type='plugin' AND folder='koowa' AND element='logman'";
        $db->setQuery($query)->query();

        // Enable control panel module
        $query = "UPDATE #__modules SET published = {$state} WHERE module='mod_logman'";
        $db->setQuery($query)->query();
    }

    protected function _dbUpdate($installer, $config = array())
    {
        $installer_manifest = simplexml_load_file($installer->getParent()->getPath('manifest'));

        $failed      = array();
        $current_ver = (string) $installer_manifest->version;
        $queries     = array();

        $db  = JFactory::getDBO();
        $app = JFactory::getApplication();

        $adapter = KObjectManager::getInstance()->getObject('lib:database.adapter.mysqli');

        $dbname   = $app->get('db');
        $dbprefix = $app->get('dbprefix');

        $query = KObjectManager::getInstance()->getObject('lib:database.query.show')->show('tables')->from($dbname);

        $tables = $adapter->select($query, KDatabase::FETCH_FIELD_LIST);

        // Only update if a newer version is being installed.
        if (in_array($dbprefix . '_activities_activities', $tables) && version_compare($config['from_ver'], $current_ver, '<'))
        {
            // Check that schema isn't already up to date (downgrade and re-install).
            // TODO: Remove when downgrades get disallowed on installers.
            $schema = $adapter->getTableSchema('activities_activities');

            if (!isset($schema->columns['metadata']))
            {
                // Row can now contain non-integer values.
                $queries[] = 'ALTER TABLE `#__activities_activities` MODIFY `row`  VARCHAR(2048) NOT NULL DEFAULT \'\'';

                // Adding indexes.
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD INDEX `package` (`package`)';
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD INDEX `name` (`name`)';
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD INDEX `row` (`row`)';

                // Added ip column.
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD COLUMN `ip` varchar(45) NOT NULL DEFAULT \'\'';
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD INDEX `ip` (`ip`)';

                // Add context row.
                $queries[] = 'ALTER TABLE `#__activities_activities` ADD COLUMN `metadata` text NOT NULL';
            }

            // Fix com_config activities.
            $queries[] = 'UPDATE `#__activities_activities` AS `activities` SET `name`=\'settings\' WHERE `package`=\'config\' AND `name` <> \'settings\'';

            foreach ($queries as $query)
            {
                $db->setQuery($query);
                if ($db->query() === false)
                {
                    $failed[] = $query;
                    // Do not continue with the upgrade. This should facilitate a manual intervention in case
                    // anything goes wrong.
                    break;
                }
            }
        }

        return $failed;
    }

    /**
     * Returns the current version (if any) of LOGman.
     *
     * @return string|null The LOGman version if present, null otherwise.
     */
    public function getCurrentVersion()
    {
        if (!$this->_current_ver) {
            $this->_current_ver = $this->_getExtensionVersion('com_logman');
        }
        return $this->_current_ver;
    }

    /**
     * Returns the current installed version (if any) of EXTman.
     *
     * @return null|string The EXTman version if present, null otherwise.
     */
    public function getExtmanVersion()
    {
        if (!$this->_extman_ver) {
            $this->_extman_ver = $this->_getExtensionVersion('com_extman');
        }
        return $this->_extman_ver;
    }

    /**
     * Extension version getter.
     *
     * @param string $element The element name, e.g. com_extman, com_logman, etc.
     * @return mixed|null|string The extension version, null if couldn't be determined.
     */
    protected function _getExtensionVersion($element)
    {
        $version = null;

        $query    = "SELECT manifest_cache FROM #__extensions WHERE element = '{$element}'";
        if ($result = JFactory::getDBO()->setQuery($query)->loadResult()) {
            $manifest = new JRegistry($result);
            $version  = $manifest->get('version', null);
        }

        return $version;
    }
}
